﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pelican7.UIGraph
{
    public static class SceneExtensions
    {
        public static List<T> FindAllComponentsInScene<T>(Scene scene)
            where T : Component
        {
            List<T> components = new List<T>();
            foreach (GameObject rootGameObject in scene.GetRootGameObjects())
            {
                components.AddRange(rootGameObject.GetComponentsInChildren<T>());
            }

            return components;
        }
    }
}