﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public partial class GraphableFieldPortData
    {
        public string fieldName;
        public bool allowsMultipleConnections;
        public List<FieldEdgeData> edges = new List<FieldEdgeData>();

        public GraphableFieldPortData(GraphableField field)
        {
            fieldName = field.FieldName;
            allowsMultipleConnections = field.AllowsMultiple;
        }
    }

    public partial class GraphableFieldPortData
    {
#if UNITY_EDITOR
        public void AddEdge(FieldEdgeData edge)
        {
            edges.Add(edge);
        }

        public void RemoveEdge(FieldEdgeData edge)
        {
            edges.Remove(edge);
        }

        public bool HasEdgeWithDestination(NodeData destination)
        {
            bool hasEdgeWithDestination = false;
            foreach (FieldEdgeData edge in edges)
            {
                if (edge.destinationNode.Equals(destination))
                {
                    hasEdgeWithDestination = true;
                    break;
                }
            }

            return hasEdgeWithDestination;
        }
#endif
    }
}