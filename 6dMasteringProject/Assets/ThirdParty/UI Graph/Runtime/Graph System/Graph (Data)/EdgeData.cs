﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    public abstract partial class EdgeData : ScriptableObject
    {
        public string guid;
        public NodeData sourceNode;
        public NodeData destinationNode;
        public NodeData targetNode;
        public EdgeType type;
        
        [System.Flags]
        public enum EdgeType
        {
            Presentation = 1,
            Dismissal = 2,
            Replacement = 4,
            Embed = 8,

            AllTransitionTypes = Presentation | Dismissal | Replacement,
            AllDownstreamTypes = Presentation | Replacement | Embed,
            AllTypes = Presentation | Dismissal | Replacement | Embed
        }
    }

    public abstract partial class EdgeData : ScriptableObject
    {
#if UNITY_EDITOR
        public GraphResource graph;

        public abstract void AddToSourceNode();
        public abstract void AssignDestinationNodeParentIfNecessary();
        public abstract void RemoveFromSourceNode();
        public abstract void NullifyDestinationNodeParentIfNecessary();
#endif
    }
}