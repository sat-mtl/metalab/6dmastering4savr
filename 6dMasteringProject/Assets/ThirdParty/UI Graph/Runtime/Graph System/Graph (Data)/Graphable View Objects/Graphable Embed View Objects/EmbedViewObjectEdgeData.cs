﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public partial class EmbedViewObjectEdgeData : EdgeData
    {
#if UNITY_EDITOR
        public string embedViewObjectGuid;

        public ViewControllerNodeData SourceNode
        {
            get
            {
                return sourceNode as ViewControllerNodeData;
            }
        }

        public override void AddToSourceNode()
        {
            SourceNode.AddEmbedViewObjectEdgeToEmbedViewObjectPortWithEmbedViewObjectGuid(this, embedViewObjectGuid);
        }

        public override void AssignDestinationNodeParentIfNecessary()
        {
            // Embed view object edge's always assign their destination parent.
            destinationNode.parent = targetNode;
        }

        public override void RemoveFromSourceNode()
        {
            SourceNode.RemoveEmbedViewObjectEdgeFromCurrentPort(this);
        }

        public override void NullifyDestinationNodeParentIfNecessary()
        {
            // Embed view object edge's always nullify their destination parent.
            destinationNode.parent = null;
        }
#endif
    }
}