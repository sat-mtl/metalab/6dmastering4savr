﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public partial class GraphableTransitionViewObjectPortData
    {
        public string transitionViewObjectGuid;
        public string transitionViewObjectDisplayName;
        public List<TransitionViewObjectEdgeData> edges = new List<TransitionViewObjectEdgeData>();
    }

    public partial class GraphableTransitionViewObjectPortData
    {
#if UNITY_EDITOR
        // At runtime, edges are stored directly on the GraphableViewObjectPortData only. In the editor, we store an additional copy of the edge in the trigger data so we can use property drawers.
        public List<GraphableTransitionViewObjectTriggerData> triggers;

        public GraphableTransitionViewObjectPortData(IGraphableTransitionViewObject viewObject)
        {
            transitionViewObjectGuid = viewObject.Guid;
            transitionViewObjectDisplayName = viewObject.DisplayName;

            triggers = new List<GraphableTransitionViewObjectTriggerData>();
            foreach (GraphableTransitionViewObjectTrigger trigger in viewObject.Triggers)
            {
                triggers.Add(new GraphableTransitionViewObjectTriggerData(trigger));
            }
        }

        public void AddEdge(TransitionViewObjectEdgeData edge)
        {
            edges.Add(edge);

            GraphableTransitionViewObjectTriggerData trigger = TriggerWithGuid(edge.triggerGuid);
            trigger.edge = edge;
        }

        public void RemoveEdge(TransitionViewObjectEdgeData edge)
        {
            edges.Remove(edge);

            // Trigger can be null if a user resets and existing graphable (i.e. it receives a new guid), which has an existing edge. In this case, the port will be removed anyway.
            GraphableTransitionViewObjectTriggerData trigger = TriggerWithGuid(edge.triggerGuid);
            if (trigger != null)
            {
                trigger.edge = null;
            }
        }

        private GraphableTransitionViewObjectTriggerData TriggerWithGuid(string triggerGuid)
        {
            GraphableTransitionViewObjectTriggerData trigger = null;
            foreach (GraphableTransitionViewObjectTriggerData t in triggers)
            {
                if (t.trigger.Guid.Equals(triggerGuid))
                {
                    trigger = t;
                    break;
                }
            }

            return trigger;
        }
#endif
    }
}