﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public partial class TransitionViewObjectEdgeData : TransitionEdgeData
    {
        public string triggerGuid;
    }

    public partial class TransitionViewObjectEdgeData : TransitionEdgeData
    {
#if UNITY_EDITOR
        public string transitionViewObjectGuid;

        public ViewControllerNodeData SourceNode
        {
            get
            {
                return sourceNode as ViewControllerNodeData;
            }
        }

        public void Initialize(GraphResource graph, NodeData sourceNode, NodeData destinationNode, NodeData targetNode, ViewControllerTransitionIdentifier transitionIdentifier, string viewObjectGuid, string triggerGuid)
        {
            Initialize(graph, sourceNode, destinationNode, targetNode, transitionIdentifier);

            this.transitionViewObjectGuid = viewObjectGuid;
            this.triggerGuid = triggerGuid;

            name = string.Format("[ViewObjectEdge] {0} --{1}--> {2} ({3})", sourceNode.resource.Name, transitionGuid, destinationNode.resource.Name, guid);
        }

        public override void AddToSourceNode()
        {
            SourceNode.AddTransitionViewObjectEdgeToTransitionViewObjectPortWithTransitionViewObjectGuid(this, transitionViewObjectGuid);
        }

        public override void RemoveFromSourceNode()
        {
            SourceNode.RemoveTransitionViewObjectEdgeFromCurrentPort(this);
        }
#endif
    }
}