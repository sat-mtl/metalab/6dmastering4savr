﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class GraphableTransitionViewObjectTriggerData
    {
        public GraphableTransitionViewObjectTrigger trigger;
        public TransitionViewObjectEdgeData edge;

        public GraphableTransitionViewObjectTriggerData(GraphableTransitionViewObjectTrigger trigger)
        {
            this.trigger = trigger;
        }

        public string TriggerGuid
        {
            get
            {
                return trigger.Guid;
            }
        }
    }
}