﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// When a graph performs a transition between two of its nodes, it creates a GraphTransition object to represent it. This object is passed to <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.PrepareForGraphTransition(GraphTransition{TViewController})"/> to provide information about the transition, such as the view controllers involved.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GraphTransition<T>
        where T : class, IGraphable
    {
        public GraphTransition(GraphableTransition graphableTransition)
        {
            TransitionIdentifier = graphableTransition.TransitionIdentifier;
            FromViewController = graphableTransition.SourceGraphable as T;
            ToViewController = graphableTransition.DestinationGraphable as T;
            Animated = graphableTransition.Animated;
            Interactive = graphableTransition.Interactive;
            UserIdentifier = graphableTransition.UserIdentifier;
            Invoker = graphableTransition.Invoker;
        }

        /// <summary>
        /// The transition's identifier.
        /// </summary>
        public ViewControllerTransitionIdentifier TransitionIdentifier { get; }
        /// <summary>
        /// The view controller being transition from.
        /// </summary>
        public T FromViewController { get; private set; }
        /// <summary>
        /// The view controller being transition to.
        /// </summary>
        public T ToViewController { get; }
        /// <summary>
        /// Is the transition animated?
        /// </summary>
        public bool Animated { get; }
        /// <summary>
        /// Is the transition interactive?
        /// </summary>
        public bool Interactive { get; }
        /// <summary>
        /// The transition's user identifier, as specified in the graph editor.
        /// </summary>
        public string UserIdentifier { get; }
        /// <summary>
        /// The object whom invoked this transition.
        /// </summary>
        public object Invoker { get; }
    }
}