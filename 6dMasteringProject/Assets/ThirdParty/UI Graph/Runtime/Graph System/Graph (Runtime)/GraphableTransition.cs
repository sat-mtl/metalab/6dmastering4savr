﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    public class GraphableTransition
    {
        public GraphableTransition(ViewControllerTransitionIdentifier transitionIdentifier, IGraphable sourceGraphable, IGraphable destinationGraphable, bool animated, bool interactive, string userIdentifier, object invoker)
        {
            TransitionIdentifier = transitionIdentifier;
            SourceGraphable = sourceGraphable;
            DestinationGraphable = destinationGraphable;
            Animated = animated;
            Interactive = interactive;
            UserIdentifier = userIdentifier;
            Invoker = invoker;
        }

        public ViewControllerTransitionIdentifier TransitionIdentifier { get; }
        public IGraphable SourceGraphable { get; }
        public IGraphable DestinationGraphable { get; }
        public bool Animated { get; }
        public bool Interactive { get; }
        public string UserIdentifier { get; }
        public object Invoker { get; }
    }
}