﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class GraphableFieldPort
    {
        public string fieldName;
        public List<FieldEdge> edges;

        public GraphableFieldPort(GraphableFieldPortData data, Dictionary<string, Node> nodes)
        {
            fieldName = data.fieldName;

            edges = new List<FieldEdge>(data.edges.Count);
            foreach (FieldEdgeData fieldEdgeData in data.edges)
            {
                FieldEdge fieldEdge = new FieldEdge(fieldEdgeData, nodes);
                edges.Add(fieldEdge);
            }
        }

        public IGraphable[] Activate(object invoker)
        {
            var graphables = new IGraphable[edges.Count];
            for (int i = 0; i < edges.Count; i++)
            {
                FieldEdge edge = edges[i];
                edge.Invoke(invoker);

                graphables[i] = edge.DestinationNode.Graphable;
            }

            return graphables;
        }
    }
}