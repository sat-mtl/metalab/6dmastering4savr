﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A <see cref="GraphableTransitionViewComponent{T}"/> defines one or more GraphableTransitionViewObjectTrigger objects to expose them to the graph editor. The trigger's <see cref="displayName"/> will be displayed in the editor.
    /// </summary>
    [System.Serializable]
    public class GraphableTransitionViewObjectTrigger
    {
#pragma warning disable 0649
        [SerializeField] private string guid;
        [SerializeField] private string displayName;
#pragma warning restore 0649

        public GraphableTransitionViewObjectTrigger(string displayName)
        {
            guid = System.Guid.NewGuid().ToString();
            this.displayName = displayName;
        }

        public string Guid
        {
            get
            {
                return guid;
            }
        }

        public string DisplayName
        {
            get
            {
                return displayName;
            }
        }
    }
}