﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    [System.Serializable]
    public class GraphableEmbedViewObjectPort
    {
        public string embedViewObjectGuid;
        public EmbedViewObjectEdge edge;

        public GraphableEmbedViewObjectPort(GraphableEmbedViewObjectPortData data, Dictionary<string, Node> nodes)
        {
            embedViewObjectGuid = data.embedViewObjectGuid;
            if (data.edge != null)
            {
                edge = new EmbedViewObjectEdge(data.edge, nodes);
            }
        }

        public IGraphable Activate(object invoker)
        {
            edge?.Invoke(invoker);
            return edge?.DestinationNode.Graphable;
        }
    }
}