﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The GraphableButtonComponent exposes UnityEngine.UI.Button components to the graph editor. It defines a single trigger, "On Click", that is activated upon the button being clicked.
    /// </summary>
    public class GraphableButtonComponent : GraphableTransitionViewComponent<UnityEngine.UI.Button>
    {
        protected override void BindToTarget()
        {
            // Bind port activation to the button's onClick callback.
            target.onClick.AddListener(() =>
            {
                Activate(triggers[0]);
            });
        }

        protected override void Reset()
        {
            base.Reset();

            // Provide a single trigger for when the button is clicked.
            triggers = new GraphableTransitionViewObjectTrigger[]
            {
                new GraphableTransitionViewObjectTrigger("On Click")
            };
        }
    }
}