﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    // [Developer Note] Currently for internal use only.
    //[CreateAssetMenu(fileName = "New Slide Animation", menuName = "UI Graph/UIElements/Transition Sequence Animator/Slide Animation", order = MenuItemPriority.Group1)]
    public class SlideElementsControllerTransitionAnimation : ElementsControllerTransitionSequenceAnimation
    {
        // The slide animation requires an off-screen position to be slid to/from. This off-screen position is determined by an angular direction and a scalar. This allows us to animate a slide in any direction, as well as over varying lengths. The angular direction value specifies the direction from the center moving clockwise through 360°. For example, 0° points to the top, 90° points right, 180° points down and so on... The scalar value determines how far along this direction the off-screen position is. A value of 1 specifies that the off-screen position will be placed exactly off-screen (i.e. the rects will be touching). Values less than one interpolate between the center and this extreme.
        [Range(0f, 359.9f)] public float offScreenAngularDirection;
        [Range(0f, 1f)] public float offScreenScalar = 1f;

        private Vector2 startPosition;
        private Vector2 endPosition;

        public override void InitializeAnimationWithContext(ElementsControllerTransitionContext context)
        {
            base.InitializeAnimationWithContext(context);
            startPosition = StartPositionForViewController(TargetViewController);
            endPosition = EndPositionForViewController(TargetViewController);
        }

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);

            var targetTransform = TargetViewController.View.Transform;
            targetTransform.Position = Vector3.LerpUnclamped(startPosition, endPosition, EasedProgress01);
        }

        private Vector2 StartPositionForViewController(ElementsController viewController)
        {
            if (transitionDirection == Direction.TransitionOffScreen)
            {
                return Vector2.zero;
            }

            return OffScreenPositionForViewController(viewController);
        }

        private Vector2 EndPositionForViewController(ElementsController viewController)
        {
            if (transitionDirection == Direction.TransitionOnScreen)
            {
                return Vector2.zero;
            }

            return OffScreenPositionForViewController(viewController);
        }

        private Vector2 OffScreenPositionForViewController(ElementsController viewController)
        {
            var radians = offScreenAngularDirection * Mathf.Deg2Rad;
            float sinTheta = Mathf.Sin(radians);
            float x01 = Mathf.InverseLerp(-0.5f, 0.5f, sinTheta);
            float xScalar = Mathf.Lerp(-1f, 1f, x01);

            float cosTheta = Mathf.Cos(radians);
            float y01 = Mathf.InverseLerp(-0.5f, 0.5f, cosTheta);
            float yScalar = Mathf.Lerp(-1f, 1f, y01);

            // UIElements doesn't currently allow us to force a layout after instantiating a visual element. This means we can't query the instantiated view for its size as it will be NaN until the layout happens at the end of the frame. Because of this, we use the view's parent to determine the size. Hopefully this functionality will be added in a future version of UIElements.
            var size = viewController.View.parent.layout.size;
            //var size = viewController.View.Rect.size;
            return new Vector2(size.x * xScalar, size.y * yScalar) * offScreenScalar;
        }
    }
}