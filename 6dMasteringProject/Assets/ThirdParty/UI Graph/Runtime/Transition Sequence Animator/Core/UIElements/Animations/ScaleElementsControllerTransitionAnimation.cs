﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    // [Developer Note] Currently for internal use only.
    //[CreateAssetMenu(fileName = "New Scale Animation", menuName = "UI Graph/UIElements/Transition Sequence Animator/Scale Animation", order = MenuItemPriority.Group1)]
    public class ScaleElementsControllerTransitionAnimation : ElementsControllerTransitionSequenceAnimation
    {
        public Vector2 offScreenScale;

        private Vector2 startScale;
        private Vector2 endScale;

        public override void InitializeAnimationWithContext(ElementsControllerTransitionContext context)
        {
            base.InitializeAnimationWithContext(context);
            startScale = StartScaleForViewController();
            endScale = EndScaleForViewController();
        }

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);

            var targetTransform = TargetViewController.View.Transform;
            targetTransform.Scale = Vector3.LerpUnclamped(startScale, endScale, EasedProgress01);
        }

        private Vector2 StartScaleForViewController()
        {
            if (transitionDirection == Direction.TransitionOffScreen)
            {
                return Vector2.one;
            }

            return offScreenScale;
        }

        private Vector2 EndScaleForViewController()
        {
            if (transitionDirection == Direction.TransitionOnScreen)
            {
                return Vector2.one;
            }

            return offScreenScale;
        }
    }
}