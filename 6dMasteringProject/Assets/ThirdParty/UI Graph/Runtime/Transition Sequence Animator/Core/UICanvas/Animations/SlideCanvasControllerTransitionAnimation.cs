﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The SlideCanvasControllerTransitionAnimation performs a slide animation of its <see cref="TransitionSequenceAnimation{TViewController, TContext}.TargetViewController"/>.
    /// <para>
    /// The slide animation requires an off-screen position to be slid to/from. This off-screen position is determined by the <see cref="offScreenAngularDirection"/> and the <see cref="offScreenScalar"/>. The angular direction value specifies the direction from the center moving clockwise through 360°. For example, 0° points to the top, 90° points right, 180° points down, and 270° points left. The scalar value determines how far along this direction the off-screen position is. A value of 1 specifies that the off-screen position will be placed exactly off-screen (i.e. the rects will be touching). Values less than one interpolate between the center and this point.
    /// </para>
    /// <para>
    /// Specify a <see cref="TransitionSequenceAnimation{TViewController, TContext}.transitionDirection"/> to determine whether the animator is sliding to or from the center. For example, an offScreenAngularDirection of 90°, an offScreenScalar value of one, and a direction of TransitionOnScreen would slide the view controller from fully off-screen right to the center.
    /// </para>
    /// </summary>
    [CreateAssetMenu(fileName = "New Slide Animation", menuName = "UI Graph/UICanvas/Transition Sequence Animator/Slide Animation", order = MenuItemPriority.Group1)]
    public class SlideCanvasControllerTransitionAnimation : CanvasControllerTransitionSequenceAnimation
    {
        // The slide animation requires an off-screen position to be slid to/from. This off-screen position is determined by an angular direction and a scalar. This allows us to animate a slide in any direction, as well as over varying lengths. The angular direction value specifies the direction from the center moving clockwise through 360°. For example, 0° points to the top, 90° points right, 180° points down and so on... The scalar value determines how far along this direction the off-screen position is. A value of 1 specifies that the off-screen position will be placed exactly off-screen (i.e. the rects will be touching). Values less than one interpolate between the center and this extreme.
        /// <summary>
        /// The direction that the target view controller will be slid.
        /// </summary>
        [Range(0f, 359.9f)] public float offScreenAngularDirection;
        /// <summary>
        /// The normalized amount the target view controller will be slid. A value of one equates to fully off-screen.
        /// </summary>
        [Range(0f, 1f)] public float offScreenScalar = 1f;

        private Vector2 startPosition;
        private Vector2 endPosition;

        public override void InitializeAnimationWithContext(CanvasControllerTransitionContext context)
        {
            base.InitializeAnimationWithContext(context);
            startPosition = StartPositionForViewController(TargetViewController);
            endPosition = EndPositionForViewController(TargetViewController);
        }

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);

            var targetTransform = TargetViewController.View.Transform;
            targetTransform.Position = Vector3.LerpUnclamped(startPosition, endPosition, EasedProgress01);
        }

        private Vector2 StartPositionForViewController(CanvasController viewController)
        {
            if (transitionDirection == Direction.TransitionOffScreen)
            {
                return Vector2.zero;
            }

            return OffScreenPositionForViewController(viewController);
        }

        private Vector2 EndPositionForViewController(CanvasController viewController)
        {
            if (transitionDirection == Direction.TransitionOnScreen)
            {
                return Vector2.zero;
            }

            return OffScreenPositionForViewController(viewController);
        }

        private Vector2 OffScreenPositionForViewController(CanvasController viewController)
        {
            var radians = offScreenAngularDirection * Mathf.Deg2Rad;
            float sinTheta = Mathf.Sin(radians);
            float x01 = Mathf.InverseLerp(-0.5f, 0.5f, sinTheta);
            float xScalar = Mathf.Lerp(-1f, 1f, x01);

            float cosTheta = Mathf.Cos(radians);
            float y01 = Mathf.InverseLerp(-0.5f, 0.5f, cosTheta);
            float yScalar = Mathf.Lerp(-1f, 1f, y01);

            var size = viewController.View.Rect.size;
            return new Vector2(size.x * xScalar, size.y * yScalar) * offScreenScalar;
        }
    }
}