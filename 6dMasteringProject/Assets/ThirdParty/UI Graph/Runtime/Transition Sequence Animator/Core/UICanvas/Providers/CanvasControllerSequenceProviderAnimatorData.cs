﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A CanvasControllerSequenceProviderAnimatorData stores a pair of objects – a <see cref="CanvasControllerTransitionSequenceAnimator"/> with a <see cref="ViewControllerTransitionIdentifier"/> GUID. This allows you to define an animator for a particular transition type. Additionally, it allows you to specify an alternative animator for when that transition is interactive.
    /// </summary>
    [System.Serializable]
    public class CanvasControllerSequenceProviderAnimatorData
    {
        /// <summary>
        /// The transition's GUID, as defined by its <see cref="ViewControllerTransitionIdentifier.Guid"/>.
        /// </summary>
        public string transitionGuid;
        /// <summary>
        /// The animator to use for the specified <see cref="transitionGuid"/>.
        /// </summary>
        public CanvasControllerTransitionSequenceAnimator animator;
        /// <summary>
        /// Use an alternative animator for interactive transitions? If the transition specified by <see cref="transitionGuid"/> is interactive, <see cref="interactiveAnimator"/> will be returned instead.
        /// </summary>
        public bool useAlternateAnimatorForInteractiveTransition;
        /// <summary>
        /// The animator to use for the specified <see cref="transitionGuid"/> when the transition is interactive and <see cref="useAlternateAnimatorForInteractiveTransition"/> is true.
        /// </summary>
        public CanvasControllerTransitionSequenceAnimator interactiveAnimator;

        public CanvasControllerSequenceProviderAnimatorData(string transitionGuid)
        {
            this.transitionGuid = transitionGuid;
        }

        public CanvasControllerTransitionSequenceAnimator AnimatorForContext(CanvasControllerTransitionContext context)
        {
            if (useAlternateAnimatorForInteractiveTransition && context.Interactive)
            {
                return interactiveAnimator;
            }

            return animator;
        }
    }
}