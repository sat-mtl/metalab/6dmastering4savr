﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The SetVisibilityCanvasControllerTransitionAnimation sets its <see cref="TransitionSequenceAnimation{TViewController, TContext}.TargetViewController"/>'s visibility, as specified by the <see cref="visible"/> field. It can be used to set the visibility of a view controller midway through a transition sequence.
    /// <para>
    /// The <see cref="TransitionSequenceAnimation{TViewController, TContext}.transitionDirection"/> and <see cref="TransitionSequenceAnimation{TViewController, TContext}.curve"/> are ignored by this animator.
    /// </para>
    /// </summary>
    [CreateAssetMenu(fileName = "New Set Visibility Animation", menuName = "UI Graph/UICanvas/Transition Sequence Animator/Set Visibility Animation", order = MenuItemPriority.Group1)]
    public class SetVisibilityCanvasControllerTransitionAnimation : CanvasControllerTransitionSequenceAnimation
    {
        /// <summary>
        /// The visibility to apply to the target view controller.
        /// </summary>
        public bool visible = true;

        private bool activated = false;
        public bool? visibilityBeforeAnimation;

        public override void UpdateProgress(float progress01)
        {
            base.UpdateProgress(progress01);
            if (activated == false)
            {
                Activate();
            }
        }

        public override void OnTransitionFinished(bool completed)
        {
            base.OnTransitionFinished(completed);

            // If the transition was not completed, move the view back to its original index if necessary.
            if (completed == false)
            {
                ReverseIfNecessary();
            }
        }

        private void Activate()
        {
            visibilityBeforeAnimation = TargetViewController.View.Visible;
            TargetViewController.View.Visible = visible;
            activated = true;
        }

        private void ReverseIfNecessary()
        {
            if (visibilityBeforeAnimation.HasValue)
            {
                TargetViewController.View.Visible = visibilityBeforeAnimation.Value;
            }
        }

        private void OnValidate()
        {
            initializeWithZeroProgress = false;
        }
    }
}