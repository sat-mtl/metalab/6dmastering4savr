﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The CanvasControllerSequenceProvider is a specific type of <see cref="CanvasControllerTransitionAnimatorProvider"/> for working with the Transition Sequence Animator and UI Canvas Workflow. It provides <see cref="CanvasControllerTransitionSequenceAnimator"/> objects to the View Controller system to determine how the transition is animated. It allows you to provide a <see cref="CanvasControllerTransitionSequenceAnimator"/> for a <see cref="ViewControllerTransitionIdentifier"/>.
    /// </summary>
    [CreateAssetMenu(fileName = "New Sequence Provider", menuName = "UI Graph/UICanvas/Transition Sequence Animator/Sequence Provider", order = MenuItemPriority.Group0)]
    public class CanvasControllerSequenceProvider : CanvasControllerTransitionAnimatorProvider
    {
        /// <summary>
        /// The provider's list of animators.
        /// </summary>
        public CanvasControllerSequenceProviderAnimatorData[] animators;

        public override IViewControllerTransitionAnimator<CanvasControllerTransitionContext> AnimatorForTransition(CanvasControllerTransitionContext context)
        {
            CanvasControllerTransitionSequenceAnimator animator = null;

            string guid = context.Identifier.Guid;
            foreach (CanvasControllerSequenceProviderAnimatorData animatorData in animators)
            {
                if (animatorData.transitionGuid.Equals(guid))
                {
                    animator = animatorData.AnimatorForContext(context);
                    break;
                }
            }

            return (animator != null) ? Instantiate(animator) : null;
        }
    }
}