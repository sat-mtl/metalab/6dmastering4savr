﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The SplitCanvasPositioningTransitionAnimation is a specialist animation for updating a <see cref="SplitCanvasPositioning"/> component. It is designed to be used with a <see cref="SplitCanvasController"/>.
    /// </summary>
    public class SplitCanvasPositioningTransitionAnimation : CanvasControllerTransitionSequenceAnimation
    {
        private CanvasControllerTransitionContext context;
        private SplitCanvasController splitCanvasController;
        private SplitCanvasPositioning splitPositions;

        public override void InitializeAnimationWithContext(CanvasControllerTransitionContext context)
        {
            base.InitializeAnimationWithContext(context);
            this.context = context;
            splitCanvasController = context.OwnerViewController as SplitCanvasController;
            if (splitCanvasController == null)
            {
                Error.Log("SplitCanvasPositioningTransitionAnimation is a specialist animation for use with a split canvas controller.");
                return;
            }

            splitPositions = splitCanvasController.splitPositioning;
        }

        public override void UpdateProgress(float instructionProgress01)
        {
            base.UpdateProgress(instructionProgress01);
            splitPositions.UpdateSplit(context.Identifier, splitCanvasController, EasedProgress01);
        }

        private void OnValidate()
        {
            viewControllerIdentifier = ViewControllerIdentifier.None;
        }
    }
}