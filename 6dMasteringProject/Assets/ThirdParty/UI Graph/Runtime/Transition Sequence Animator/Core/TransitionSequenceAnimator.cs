﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The abstract base type for sequences (animators) to be used with the Transition Sequence Animator.
    /// </summary>
    /// <typeparam name="TRegion"></typeparam>
    /// <typeparam name="TContext"></typeparam>
    public abstract class TransitionSequenceAnimator<TRegion, TContext> : ScriptableObject, IViewControllerTransitionAnimator<TContext>
        where TRegion : ISequencable<TContext>
    {
        /// <summary>
        /// The sequence's duration.
        /// </summary>
        public float duration;
        /// <summary>
        /// The sequence's time update mode.
        /// </summary>
        public ViewControllerTransitionTimeUpdateMode timeUpdateMode;
        /// <summary>
        /// The sequence's animation regions.
        /// </summary>
        public TRegion[] regions;

        float IViewControllerTransitionAnimator<TContext>.TransitionDuration(TContext context)
        {
            return duration;
        }

        ViewControllerTransitionTimeUpdateMode IViewControllerTransitionAnimator<TContext>.TransitionTimeUpdateMode(TContext context)
        {
            return timeUpdateMode;
        }

        void IViewControllerTransitionAnimator<TContext>.ConfigureTransitionAnimation(TContext context)
        {
            foreach (var region in regions)
            {
                region.InitializeWithContext(context);
            }
        }

        void IViewControllerTransitionAnimator<TContext>.UpdateTransitionAnimation(TContext context, float progress01)
        {
            foreach (var region in regions)
            {
                region.UpdateProgress(progress01);
            }
        }

        void IViewControllerTransitionAnimator<TContext>.OnTransitionFinished(TContext context, bool completed)
        {
            foreach (var region in regions)
            {
                region.OnTransitionFinished(completed);
            }
        }

        private void Reset()
        {
            ApplyDefaults();
        }

        private void ApplyDefaults()
        {
            duration = 0.33f;
            timeUpdateMode = ViewControllerTransitionTimeUpdateMode.UnscaledTime;
        }
    }
}