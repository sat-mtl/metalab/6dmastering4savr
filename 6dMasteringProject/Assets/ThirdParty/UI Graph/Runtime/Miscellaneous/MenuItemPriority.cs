﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    public static class MenuItemPriority
    {
        public const int Group0 = Offset + 0;
        public const int Group1 = Offset + 20;
        public const int Group2 = Offset + 40;
        public const int Group3 = Offset + 60;

        public const int Offset = 100;
    }
}