﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

// [Developer Note] Once Unity release UIElements for runtime, the #if UNITY_EDITOR compiler directive can be removed.
#if UNITY_EDITOR
using UnityEditor.UIElements;
[assembly: UxmlNamespacePrefix("Pelican7.UIGraph", "uigraph")]
#endif