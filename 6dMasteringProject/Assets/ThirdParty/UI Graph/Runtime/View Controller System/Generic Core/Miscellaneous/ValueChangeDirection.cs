﻿namespace Pelican7.UIGraph
{
    public class ValueChangeDirection
    {
        private float value;

        public enum ChangeDirection
        {
            Increase,
            Decrease
        }

        public ChangeDirection? Direction { get; private set; }

        public bool UpdateValue(float value)
        {
            float previous = this.value;
            this.value = value;

            ChangeDirection? direction = null;
            if (this.value > previous)
            {
                direction = ChangeDirection.Increase;
            }
            else if (this.value < previous)
            {
                direction = ChangeDirection.Decrease;
            }

            return SetDirection(direction);
        }

        private bool SetDirection(ChangeDirection? direction)
        {
            if (direction.HasValue == false)
            {
                return false;
            }

            ChangeDirection? previous = Direction;
            Direction = direction;

            bool hasChangedDirection = false;
            if (previous.HasValue)
            {
                hasChangedDirection = (previous != Direction);
            }
            return hasChangedDirection;
        }
    }
}