﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public static class ViewCallbackAttributeCollector
    {
        public static MethodInfo[] CollectViewCallbackAttributedMethods(Object obj)
        {
            var methods = new List<MethodInfo>();

            var type = obj.GetType();
            var typeMethods = type.GetMethods(ViewCallbackAttribute.MethodBindingFlags);
            foreach (var method in typeMethods)
            {
                var attribute = method.GetCustomAttribute<ViewCallbackAttribute>();
                bool methodHasViewCallbackAttribute = (attribute != null);
                if (methodHasViewCallbackAttribute)
                {
                    methods.Add(method);
                }
            }

            return methods.ToArray();
        }
    }
}