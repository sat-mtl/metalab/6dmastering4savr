﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A view reference links a view controller field, decorated with the <see cref="ViewReferenceAttribute"/>, with a view object.
    /// </summary>
    public abstract class ViewReference
    {
#pragma warning disable 0649
        [SerializeField] protected string identifier;
#pragma warning restore 0649

        /// <summary>
        /// The view reference's identifier.
        /// </summary>
        public string Identifier
        {
            get
            {
                return identifier;
            }
        }
    }
}