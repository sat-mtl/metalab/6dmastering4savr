﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    public static class ViewControllerTransitionIdentifierExtensions
    {
        public static bool IsDownstreamContainmentTransition(this ViewControllerTransitionIdentifier transitionIdentifier)
        {
            ViewControllerTransitionIdentifier.TransitionType transitionType = transitionIdentifier.Type;
            return ((transitionType != ViewControllerTransitionIdentifier.TransitionType.Dismissal) && transitionIdentifier.IsContainmentTransition());
        }

        public static bool IsContainmentTransition(this ViewControllerTransitionIdentifier transitionIdentifier)
        {
            return (transitionIdentifier.IsNotContainmentTransition() == false);
        }

        public static bool IsNotContainmentTransition(this ViewControllerTransitionIdentifier transitionIdentifier)
        {
            return transitionIdentifier.IsViewControllerPresentOrDismissTransition();
        }

        public static EdgeData.EdgeType GetTransitionEdgeType(this ViewControllerTransitionIdentifier transitionIdentifier)
        {
            ViewControllerTransitionIdentifier.TransitionType transitionType = transitionIdentifier.Type;
            if (transitionType == ViewControllerTransitionIdentifier.TransitionType.Presentation)
            {
                return EdgeData.EdgeType.Presentation;
            }
            else if (transitionType == ViewControllerTransitionIdentifier.TransitionType.Dismissal)
            {
                return EdgeData.EdgeType.Dismissal;
            }
            else if (transitionType == ViewControllerTransitionIdentifier.TransitionType.Replacement)
            {
                return EdgeData.EdgeType.Replacement;
            }
            else
            {
                throw new System.Exception("Unknown transition type.");
            }
        }

        private static bool IsViewControllerPresentOrDismissTransition(this ViewControllerTransitionIdentifier transitionIdentifier)
        {
            return (transitionIdentifier.Equals(CanvasController.PresentTransition) || transitionIdentifier.Equals(CanvasController.DismissTransition) || transitionIdentifier.Equals(ElementsController.PresentTransition) || transitionIdentifier.Equals(ElementsController.DismissTransition));
        }
    }
}