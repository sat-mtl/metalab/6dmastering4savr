﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pelican7.UIGraph
{
    // Support for graphing.
    public abstract partial class ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph> : ScriptableObject, IGraphable
    {
        /// <summary>
        /// The view controller's graph, if instantiated from a graph. Otherwise, null.
        /// </summary>
        protected TGraph graph;

        void IGraphable.SetGraph(object graph)
        {
            this.graph = graph as TGraph;
        }

        IGraphable IGraphable.InstantiateFromResource(IGraphable resource)
        {
            TViewController viewControllerTemplate = resource as TViewController;
            if (viewControllerTemplate == null)
            {
                throw new System.ArgumentNullException(string.Format("Node resource must be of type {0}.", typeof(TViewController).Name));
            }

            return Instantiate(viewControllerTemplate);
        }

        List<GraphableTransitionIdentifier> IGraphable.GraphableTransitionIdentifiers()
        {
            var transitionIdentifiers = new List<GraphableTransitionIdentifier>
            {
                new GraphableTransitionIdentifier(PresentTransitionIdentifier, DismissTransitionIdentifier),
                new GraphableTransitionIdentifier(DismissTransitionIdentifier, PresentTransitionIdentifier)
            };

            var derivedTransitionIdentifiers = GraphableTransitionIdentifiers();
            if (derivedTransitionIdentifiers != null)
            {
                transitionIdentifiers.AddRange(derivedTransitionIdentifiers);
            }

            return transitionIdentifiers;
        }

        void IGraphable.PerformGraphableTransition(GraphableTransition transition)
        {
            var toViewController = transition.DestinationGraphable as TViewController;
            var transitionData = new TTransitionData
            {
                ToViewController = toViewController,
                Animated = transition.Animated,
                Interactive = transition.Interactive
            };

            GraphTransition<TViewController> graphTransition = new GraphTransition<TViewController>(transition);
            if (CanPerformGraphTransition(graphTransition, out Error error) == false)
            {
                error.LogWithPrefix("Cannot perform graph transition.");
                return;
            }

            graphTransition.FromViewController.PrepareForGraphTransition(graphTransition);
            InvokeTransition(transition.TransitionIdentifier, transitionData);
        }

#if UNITY_EDITOR
        bool IGraphable.RequiresRefreshForUpdatedAsset(string assetPath)
        {
            return viewResource.ContainsAsset(assetPath);
        }
#endif

        /// <summary>
        /// Container view controllers override this method to return the transition identifiers that they wish to expose to graphing.
        /// </summary>
        /// <returns>A list of transition identifiers to be exposed to graphing.</returns>
        protected virtual List<GraphableTransitionIdentifier> GraphableTransitionIdentifiers()
        {
            return null;
        }

        /// <summary>
        /// Derived classes can override this method to control whether their graphable transitions (returned by <see cref="GraphableTransitionIdentifiers"/>) are performed or ignored.
        /// </summary>
        /// <param name="graphTransition"></param>
        /// <param name="error"></param>
        /// <returns>True if the transition should be performed. False otherwise.</returns>
        protected virtual bool ShouldPerformGraphTransition(GraphTransition<TViewController> graphTransition, out Error error)
        {
            error = null;
            return true;
        }

        /// <summary>
        /// Derived classes can override this method to be notified when a view controller is about to be involved in a graph transition. This offers an opportunity to pass data between view controllers.
        /// </summary>
        /// <param name="transition"></param>
        protected virtual void PrepareForGraphTransition(GraphTransition<TViewController> transition) { }

        private bool CanPerformGraphTransition(GraphTransition<TViewController> graphTransition, out Error error)
        {
            ViewControllerTransitionIdentifier transitionIdentifier = graphTransition.TransitionIdentifier;
            if (transitionIdentifier.Equals(PresentTransitionIdentifier))
            {
                return CanPresentViewController(graphTransition.ToViewController, out error);
            }
            else if (transitionIdentifier.Equals(DismissTransitionIdentifier))
            {
                return CanDismissViewController(out error);
            }

            return ShouldPerformGraphTransition(graphTransition, out error);
        }
    }

    // Support for graphable view objects.
    public abstract partial class ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph> : ScriptableObject, IGraphableTransitionViewObjectContainer
    {
        private IGraphableTransitionViewObjectListener viewObjectActivationListener;

        IGraphableTransitionViewObject[] IGraphableTransitionViewObjectContainer.GraphableTransitionViewObjects
        {
            get
            {
#if UNITY_EDITOR
                return viewResource.GraphableTransitionViewObjectsInView();
#else
                return null;
#endif
            }
        }

        void IGraphableTransitionViewObjectContainer.BindGraphableTransitionViewObjectActivationListener(IGraphableTransitionViewObjectListener listener)
        {
            // Store the listener and wait until the view is loaded to bind to the view objects. 
            viewObjectActivationListener = listener;
            OnLoadView.AddListener(OnLoadViewReceived);
        }

        protected abstract void BindActivationListenerToViewObjects(IGraphableTransitionViewObjectListener viewObjectActivationListener);

        private void OnLoadViewReceived()
        {
            OnLoadView.RemoveListener(OnLoadViewReceived);
            if (viewObjectActivationListener != null)
            {
                BindActivationListenerToViewObjects(viewObjectActivationListener);
            }
        }
    }

    // Support for manual graphable transition invocation.
    public abstract partial class ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph> : ScriptableObject, IGraphableTransitionInvoker
    {
        private IGraphableTransitionInvokable invokable;

        // Perform the graph transition with this identifier.
        public void PerformGraphTransitionWithIdentifier(string userIdentifier, object invoker)
        {
            if (invokable == null)
            {
                Error.Log("Graph transitions can only be performed on view controllers that have been loaded from a graph.");
                return;
            }

            invokable.InvokeTransitionWithIdentifier(userIdentifier, invoker);
        }

        void IGraphableTransitionInvoker.BindGraphableTransitionInvokable(IGraphableTransitionInvokable invokable)
        {
            this.invokable = invokable;
        }
    }

    // Support for graphable embed view objects.
    public abstract partial class ViewController<TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph> : ScriptableObject, IGraphableEmbedContainer
    {
        private List<GraphableEmbedData> graphableEmbedDatas;

        IGraphableEmbedViewObject[] IGraphableEmbedContainer.GraphableEmbedViewObjects
        {
            get
            {
#if UNITY_EDITOR
                // In the editor, use the view resource to search for the view's graphable embed view objects.
                return viewResource.GraphableEmbedViewObjectsInView();
#else
                return null;
#endif
            }
        }

        void IGraphableEmbedContainer.EmbedGraphables(List<GraphableEmbedData> graphableEmbedDatas)
        {
            // Store the embed data and wait until the view is loaded to initialize. 
            this.graphableEmbedDatas = graphableEmbedDatas;
            OnLoadView.AddListener(OnLoadViewForEmbedReceived);
        }

        protected abstract IGraphableEmbedViewObject[] FindGraphableEmbedViewObjectsInView();

        private void OnLoadViewForEmbedReceived()
        {
            OnLoadView.RemoveListener(OnLoadViewForEmbedReceived);
            InitializeGraphableEmbedViewObjects();
        }

        private void InitializeGraphableEmbedViewObjects()
        {
            IGraphableEmbedViewObject[] graphableEmbedViewObjects = FindGraphableEmbedViewObjectsInView();
            foreach (GraphableEmbedData graphableEmbedData in graphableEmbedDatas)
            {
                IGraphable graphable = graphableEmbedData.graphable;
                var viewController = graphable as TViewController;

                // Give subclasses an opportunity to pass data to embedded view controllers before embedding.
                PrepareForGraphEmbed(viewController);

                // Add the graphable as a child view controller.
                AddChild(viewController);

                // Find the embed view object and initialize it with the graphable.
                string embedViewObjectGuid = graphableEmbedData.embedViewObjectGuid;
                IGraphableEmbedViewObject graphableEmbedViewObject = graphableEmbedViewObjects.Single((embedViewObject) => embedViewObject.Guid.Equals(embedViewObjectGuid));
                graphableEmbedViewObject.Initialize(graphable);
            }
        }

        /// <summary>
        /// Derived classes can override this method to be notified when a view controller is about to embed a view controller, as configured in a graph. This offers an opportunity to pass data to embedded view controllers.
        /// </summary>
        /// <param name="viewControllerToBeEmbedded"></param>
        protected virtual void PrepareForGraphEmbed(TViewController viewControllerToBeEmbedded) { }
    }
}