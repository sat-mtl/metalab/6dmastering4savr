﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    public interface IViewResource
    {
        void OpenView();
        void AddGraphablesToViewElements();
    }

    public abstract partial class ViewResource<TView, TWindow> : ScriptableObject
        where TView : IView<TView, TWindow>
        where TWindow : TView
    {
        public abstract TView Load(out IViewBindable viewOwnerBindings);
    }

#if UNITY_EDITOR
    public abstract partial class ViewResource<TView, TWindow> : ScriptableObject, IViewResource
    {
        public abstract void OpenView();
        public abstract void AddGraphablesToViewElements();
        public abstract IGraphableEmbedViewObject[] GraphableEmbedViewObjectsInView();
        public abstract IGraphableTransitionViewObject[] GraphableTransitionViewObjectsInView();
        public abstract bool ContainsAsset(string assetPath);
    }
#endif
}