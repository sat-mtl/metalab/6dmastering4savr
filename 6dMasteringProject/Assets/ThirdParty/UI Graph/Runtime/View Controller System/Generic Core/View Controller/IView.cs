﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    public interface IView<TView, TWindow>
        where TWindow : TView
    {
        TWindow Window { get; }
        bool Visible { get; set; }
        float Alpha { get; set; }
        bool Interactable { get; set; }
        ITransform Transform { get; set; }
        Rect Rect{ get; }

        void Add(TView child);
        void FitToParent();
        void BringToFront();
        void SendToBack();
        void Unload();
    }
}