﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A ViewControllerInvokeTransitionData is used to pass transition data to <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.InvokeTransition(ViewControllerTransitionIdentifier, TTransitionData, System.Action)"/>.
    /// <para>
    /// You should not use this class directly. Instead use one of its concrete subclasses, <see cref="CanvasControllerInvokeTransitionData"/> or <see cref="ElementsControllerInvokeTransitionData"/>, depending upon the workflow you are using.
    /// </para>
    /// </summary>
    /// <typeparam name="TViewController"></typeparam>
    public abstract class ViewControllerInvokeTransitionData<TViewController>
    {
        public ViewControllerInvokeTransitionData() { }

        public TViewController ToViewController { get; set; }
        public bool Animated { get; set; } = false;
        public bool Interactive { get; set; } = false;
    }
}