﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    public abstract partial class ViewControllerTransitionAnimationDriver<TTransitionContext, TTransitionAnimationDefaultProgressProvider>
        where TTransitionAnimationDefaultProgressProvider : class, IViewControllerTransitionProgressProvider<TTransitionContext>, new()
    {
        private IViewControllerTransitionAnimationDriverResponder responder;
        private TTransitionContext context;
        private float duration;
        private ViewControllerTransitionTimeUpdateMode timeUpdateMode;
        private ViewControllerTransitionAnimationDriverDirection direction = ViewControllerTransitionAnimationDriverDirection.Forwards;
        private float progress01 = 0f;
        private IViewControllerTransitionProgressProvider<TTransitionContext> progressProvider;
        private bool finished = false;

        public ViewControllerTransitionAnimationDriver() { }

        private float Progress01
        {
            get
            {
                return progress01;
            }

            set
            {
                progress01 = Mathf.Clamp01(value);
                responder?.OnDriverProgressUpdated(progress01);
            }
        }

        private IViewControllerTransitionDrivable<TTransitionContext> AsDrivable
        {
            get
            {
                return this as IViewControllerTransitionDrivable<TTransitionContext>;
            }
        }

        public void Initialize(IViewControllerTransitionAnimationDriverResponder responder, TTransitionContext context, float duration, ViewControllerTransitionTimeUpdateMode timeUpdateMode, IViewControllerTransitionProgressProvider<TTransitionContext> interactiveProgressProvider)
        {
            this.responder = responder;
            this.context = context;
            this.duration = duration;
            this.timeUpdateMode = timeUpdateMode;
            progressProvider = interactiveProgressProvider;
        }

        public void Run()
        {
            Progress01 = 0f;
            RunFromCurrentProgress();
        }

        public void ForceImmediateCompletion()
        {
            progressProvider.CancelProgressUpdatesToDrivable(AsDrivable);
            Progress01 = 1f;
            CompleteTransition();
        }

        public void ForceImmediateCancellation()
        {
            progressProvider.CancelProgressUpdatesToDrivable(AsDrivable);
            Progress01 = 0f;
            CancelTransition();
        }

        private void RunFromCurrentProgress()
        {
            if (progressProvider == null)
            {
                progressProvider = new TTransitionAnimationDefaultProgressProvider();
            }

            progressProvider.ProvideProgressToDrivable(AsDrivable);
        }

        private void CompleteTransition()
        {
            if (Progress01 == 1f)
            {
                finished = true;
                responder?.OnDriverCompleted();
                return;
            }

            // Complete the transition using the standard progress provider.
            progressProvider = CreateDefaultProgressProviderToCompleteTransition();
            RunFromCurrentProgress();
        }

        private void CancelTransition()
        {
            if (Progress01 == 0f)
            {
                finished = true;
                responder?.OnDriverCancelled();
                return;
            }

            // Complete the transition cancellation using the default progress provider.
            progressProvider = CreateDefaultProgressProviderToCompleteTransition();
            // The default progress provider will respect the drivable's current direction.
            direction = ViewControllerTransitionAnimationDriverDirection.Reversed;
            RunFromCurrentProgress();
        }

        private TTransitionAnimationDefaultProgressProvider CreateDefaultProgressProviderToCompleteTransition()
        {
            // The default progress provider will begin from the drivable's current progress value.
            TTransitionAnimationDefaultProgressProvider progressProvider = new TTransitionAnimationDefaultProgressProvider();

            // Set the completion curve on the progress provider. The default progress provider will use this curve to ease the remaining progress updates.
            IViewControllerTransitionCompletionProgressProvider completionProgressProvider = (IViewControllerTransitionCompletionProgressProvider)progressProvider;
            completionProgressProvider.ProgressCurve = CompletionCurve;

            return progressProvider;
        }
    }

    public abstract partial class ViewControllerTransitionAnimationDriver<TTransitionContext, TTransitionAnimationDefaultProgressProvider> : IViewControllerTransitionDrivable<TTransitionContext>
    {
        float IViewControllerTransitionDrivable<TTransitionContext>.Duration
        {
            get
            {
                return duration;
            }
        }

        ViewControllerTransitionTimeUpdateMode IViewControllerTransitionDrivable<TTransitionContext>.TimeUpdateMode
        {
            get
            {
                return timeUpdateMode;
            }
        }

        float IViewControllerTransitionDrivable<TTransitionContext>.Progress01
        {
            get
            {
                return Progress01;
            }

            set
            {
                if (CanReceiveProgressProviderUpdate(out Error error) == false)
                {
                    error.LogWithPrefix("Cannot update progress01.");
                    return;
                }

                Progress01 = value;
            }
        }

        ViewControllerTransitionAnimationDriverDirection IViewControllerTransitionDrivable<TTransitionContext>.Direction
        {
            get
            {
                return direction;
            }

            set
            {
                if (CanReceiveProgressProviderUpdate(out Error error) == false)
                {
                    error.LogWithPrefix("Cannot update direction.");
                    return;
                }

                direction = value;
            }
        }

        TTransitionContext IViewControllerTransitionDrivable<TTransitionContext>.Context
        {
            get
            {
                return context;
            }
        }

        // By default, supply an ease-out progress curve to complete with.
        public AnimationCurve CompletionCurve { get; set; } = new AnimationCurve(new Keyframe[]
        {
            new Keyframe(0f, 0f, 3.00052f, 3.00052f),
            new Keyframe(1f, 1f, 0.05463887f, 0.05463887f)
        });

        void IViewControllerTransitionDrivable<TTransitionContext>.CompleteTransition()
        {
            if (CanReceiveProgressProviderUpdate(out Error error) == false)
            {
                error.LogWithPrefix("Cannot complete transition.");
                return;
            }

            CompleteTransition();
        }

        void IViewControllerTransitionDrivable<TTransitionContext>.CancelTransition()
        {
            if (CanReceiveProgressProviderUpdate(out Error error) == false)
            {
                error.LogWithPrefix("Cannot cancel transition.");
                return;
            }

            CancelTransition();
        }

        private bool CanReceiveProgressProviderUpdate(out Error error)
        {
            if (finished)
            {
                error = new Error("The drivable has already finished driving its transition.");
                return false;
            }

            error = null;
            return true;
        }
    }
}