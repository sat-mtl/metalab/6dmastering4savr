﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The IViewControllerTransitionProgressProvider interface is used when performing an interactive transition and supplied to UI Graph via the view controller's <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.interactiveTransitionProgressProvider"/>.
    /// <para>
    /// You do not use this class directly, instead use its concrete subclass, <see cref="ICanvasControllerTransitionProgressProvider"/> or <see cref="IElementsControllerTransitionProgressProvider"/>, depending on the workflow you are using.
    /// </para>
    /// </summary>
    /// <typeparam name="TContext"></typeparam>
    public interface IViewControllerTransitionProgressProvider<TContext>
    {
        /// <summary>
        /// Provide progress updates to <paramref name="drivable"/>. Once this method is called, you are responsible for driving the transition's progress. You must call one of the <paramref name="drivable"/>'s <see cref="IViewControllerTransitionDrivable.CompleteTransition"/> or <see cref="IViewControllerTransitionDrivable.CancelTransition"/> methods when you have finished the transition.
        /// </summary>
        /// <param name="drivable"></param>
        void ProvideProgressToDrivable(IViewControllerTransitionDrivable<TContext> drivable);
        /// <summary>
        /// Cancel providing progress updates to <paramref name="drivable"/>. This is called by the system if your transition has been interrupted, such as if its view controller has been destroyed.
        /// </summary>
        /// <param name="drivable"></param>
        void CancelProgressUpdatesToDrivable(IViewControllerTransitionDrivable<TContext> drivable);
    }
}