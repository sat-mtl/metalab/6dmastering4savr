﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The transition's time update mode.
    /// </summary>
    public enum ViewControllerTransitionTimeUpdateMode
    {
        /// <summary>
        /// Updates are scaled by Unity's time scale.
        /// </summary>
        ScaledTime,
        /// <summary>
        /// Updates are not scaled by Unity's time scale.
        /// </summary>
        UnscaledTime
    }
}