﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using TMPro;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The navigation bar title label component associated with a <see cref="NavigationCanvasController"/>. The navigation controller displays the <see cref="ViewController{TViewController, TView, TWindow, TViewResource, TTransition, TTransitionContext, TTransitionAnimatorProvider, TTransitionProgressProvider, TTransitionAnimationDriver, TTransitionAnimationDefaultProgressProvider, TTransitionData, TGraph}.title"/> in its title label.
    /// </summary>
    public class CanvasNavigationBarTitleLabel : MonoBehaviour
    {
        public TextMeshProUGUI label;
        public TextMeshProUGUI transitionLabel;
        public AnimationCurve alphaTransitionCurve;

        public void TrackNavigationTransition(CanvasControllerTransition transition)
        {
            CanvasControllerTransitionContext context = transition.Context;
            if (context.Animated)
            {
                // In an animated transition, track the transition to crossfade the label.
                transitionLabel.text = context.ToViewController?.title;
                AddTransitionListeners(transition);
            }
        }

        public void ReloadAfterTransition(CanvasControllerTransition transition, bool completed)
        {
            // After a transition, update the main label and hide the transition label.
            var context = transition.Context;
            var topViewController = (completed) ? context.ToViewController : context.FromViewController;
            label.text = topViewController?.title;
            label.alpha = 1f;
            transitionLabel.text = null;
            transitionLabel.alpha = 0f;
        }

        private void AddTransitionListeners(CanvasControllerTransition transition)
        {
            transition.OnTransitionProgressUpdated.AddListener(OnTransitionProgressUpdated);
            transition.OnTransitionDidFinish.AddListener(OnTrackedTransitionFinished);
        }

        private void RemoveTransitionListeners(CanvasControllerTransition transition)
        {
            transition.OnTransitionProgressUpdated.RemoveListener(OnTransitionProgressUpdated);
            transition.OnTransitionDidFinish.RemoveListener(OnTrackedTransitionFinished);
        }

        private void OnTransitionProgressUpdated(CanvasControllerTransition transition, float progress01)
        {
            // Fade the transition label in, whilst fading the main label out.
            var alpha = alphaTransitionCurve.Evaluate(progress01);
            transitionLabel.alpha = alpha;
            label.alpha = 1f - alpha;
        }

        private void OnTrackedTransitionFinished(CanvasControllerTransition transition, bool completed)
        {
            // This is called prior to ReloadAfterTransition and only when we are tracking an animated transition.
            RemoveTransitionListeners(transition);
        }
    }
}