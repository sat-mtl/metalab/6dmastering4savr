// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The StackCanvasController is a container view controller designed for stacking screens on top of each other. It manages a stack of child view controllers and offers transitions for pushing and popping view controllers on and off its stack. Each child view controller fills the <see cref="viewControllersContainer"/> transform and is placed in stack order, making the top-most view controller on the stack visible.
    /// <para>
    /// The StackCanvasController offers transitions for pushing and popping view controllers on and off its stack. Pushing a view controller onto the stack instantiates its view, embeds it within the stack controller's <see cref="viewControllersContainer"/>, and if necessary animates it on-screen. Inversely, popping a view controller off the stack animates it off-screen if necessary, and subsequently destroys it. 
    /// </para>
    /// <para>
    /// Pushing a view controller onto a full-screen StackCanvasController would have the same architectural user-experience as presenting that view controller (albeit possibly with a different transition animation). Where the StackCanvasController becomes particularly useful is in presenting view controllers that are not full-screen. This allows you to create presentation stacks within a view.
    /// </para>
    /// <para>
    /// See <see cref="NavigationCanvasController"/> for a specific type of StackCanvasController designed to provide a user-interface for hierarchical navigation.
    /// </para>
    /// </summary>
    /// <seealso cref="NavigationCanvasController"/>
    public partial class StackCanvasController : CanvasController
    {
        /// <summary>
        /// The transition identifier for the Stack Canvas Controller's Push transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier PushTransition = new ViewControllerTransitionIdentifier("StackCanvasController.Push", ViewControllerTransitionIdentifier.TransitionType.Presentation, "Push");
        /// <summary>
        /// The transition identifier for the Stack Canvas Controller's Pop transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier PopTransition = new ViewControllerTransitionIdentifier("StackCanvasController.Pop", ViewControllerTransitionIdentifier.TransitionType.Dismissal);
        /// <summary>
        /// The transition identifier for the Stack Canvas Controller's Pop To transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier PopToTransition = new ViewControllerTransitionIdentifier("StackCanvasController.PopTo", ViewControllerTransitionIdentifier.TransitionType.Dismissal, "Pop To");
        /// <summary>
        /// The transition identifier for the Stack Canvas Controller's Pop To Root transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier PopToRootTransition = new ViewControllerTransitionIdentifier("StackCanvasController.PopToRoot", ViewControllerTransitionIdentifier.TransitionType.Dismissal);
        /// <summary>
        /// The transition identifier for the Stack Canvas Controller's Pop All transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier PopAllTransition = new ViewControllerTransitionIdentifier("StackCanvasController.PopAll", ViewControllerTransitionIdentifier.TransitionType.Dismissal);
        /// <summary>
        /// The transition identifier for the Stack Canvas Controller's Set Root transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier SetRootTransition = new ViewControllerTransitionIdentifier("StackCanvasController.SetRoot", ViewControllerTransitionIdentifier.TransitionType.Replacement, "Set Root");

        /// <summary>
        /// Allow the root view controller to be popped?
        /// </summary>
        public bool allowsRootViewControllerToBePopped;
        /// <summary>
        /// The container into which the stack controller places its child view controllers.
        /// </summary>
        [ViewReference] public RectTransform viewControllersContainer;

        protected Stack<CanvasController> viewControllers = new Stack<CanvasController>();

        /// <summary>
        /// Retrieve the stack controller's root view controller.
        /// </summary>
        public CanvasController RootViewController { get; private set; }

        /// <summary>
        /// Retrieve the stack controller's top view controller. This is the view controller at the top of the stack.
        /// </summary>
        public CanvasController TopViewController
        {
            get
            {
                CanvasController topViewController = null;
                if (viewControllers.Count > 0)
                {
                    topViewController = viewControllers.Peek();
                }

                return topViewController;
            }
        }

        /// <summary>
        /// Retrieve the stack controller's stack of view controllers as a list.
        /// </summary>
        public CanvasController[] ViewControllers
        {
            get
            {
                return viewControllers.ToArray();
            }
        }

        /// <summary>
        /// Retrieve the lowest allowed stack count. This value is dependent upon <see cref="allowsRootViewControllerToBePopped"/>.
        /// </summary>
        public int LowestAllowedStackCount
        {
            get
            {
                return (allowsRootViewControllerToBePopped) ? 0 : 1;
            }
        }

        protected virtual ViewControllerTransitionIdentifier PushTransitionIdentifier
        {
            get
            {
                return PushTransition;
            }
        }

        protected virtual ViewControllerTransitionIdentifier PopTransitionIdentifier
        {
            get
            {
                return PopTransition;
            }
        }

        protected virtual ViewControllerTransitionIdentifier PopToTransitionIdentifier
        {
            get
            {
                return PopToTransition;
            }
        }

        protected virtual ViewControllerTransitionIdentifier PopToRootTransitionIdentifier
        {
            get
            {
                return PopToRootTransition;
            }
        }

        protected virtual ViewControllerTransitionIdentifier PopAllTransitionIdentifier
        {
            get
            {
                return PopAllTransition;
            }
        }

        protected virtual ViewControllerTransitionIdentifier SetRootTransitionIdentifier
        {
            get
            {
                return SetRootTransition;
            }
        }

        /// <summary>
        /// Push <paramref name="viewController"/> onto the stack. The specified view controller will be added as a child view controller and its view will be added to the <see cref="viewControllersContainer"/> before being transitioned on-screen.
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void Push(CanvasController viewController, bool animated = true, System.Action completion = null)
        {
            Push(viewController, animated, false, completion);
        }

        /// <summary>
        /// Push <paramref name="viewController"/> onto the stack interactively. The specified view controller will be added as a child view controller and its view will be added to the <see cref="viewControllersContainer"/> before being transitioned on-screen interactively. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, this view controller will be pushed without interactivity.
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="completion"></param>
        public void PushInteractively(CanvasController viewController, System.Action completion = null)
        {
            Push(viewController, true, true, completion);
        }

        /// <summary>
        /// Pop the top view controller off the stack. The top view controller will be transitioned off-screen and subsequently destroyed.
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void Pop(bool animated = true, System.Action completion = null)
        {
            Pop(animated, false, completion);
        }

        /// <summary>
        /// Pop the top view controller off the stack interactively. The top view controller will be transitioned off-screen interactively and subsequently destroyed. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, the top view controller will be popped without interactivity.
        /// </summary>
        /// <param name="completion"></param>
        public void PopInteractively(System.Action completion = null)
        {
            Pop(true, true, completion);
        }

        /// <summary>
        /// Pop to a specific <paramref name="viewController"/> in the stack. The top view controller will be transitioned off-screen and subsequently destroyed. Any intermediary view controllers will be hidden prior to transition and subsequently destroyed on completion.
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void PopTo(CanvasController viewController, bool animated = true, System.Action completion = null)
        {
            PopToViewController(viewController, animated, false, completion);
        }

        /// <summary>
        /// Pop to a specific <paramref name="viewController"/> in the stack interactively. The top view controller will be transitioned off-screen and subsequently destroyed. Any intermediary view controllers will be hidden prior to transition and subsequently destroyed on completion. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, the view controller(s) will be popped without interactivity.
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="completion"></param>
        public void PopToInteractively(CanvasController viewController, System.Action completion = null)
        {
            PopToViewController(viewController, true, true, completion);
        }

        /// <summary>
        /// Pop to the root view controller. The top view controller will be transitioned off-screen and subsequently destroyed. Any intermediary view controllers will be hidden prior to transition and subsequently destroyed on completion. 
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void PopToRoot(bool animated = true, System.Action completion = null)
        {
            PopToViewController(RootViewController, animated, false, completion);
        }

        /// <summary>
        /// Pop to the root view controller interactively. The top view controller will be transitioned off-screen and subsequently destroyed. Any intermediary view controllers will be hidden prior to transition and subsequently destroyed on completion. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, the view controller(s) will be popped without interactivity.
        /// </summary>
        /// <param name="completion"></param>
        public void PopToRootInteractively(System.Action completion = null)
        {
            PopToViewController(RootViewController, true, true, completion);
        }

        /// <summary>
        /// Pop all view controllers off the stack. The top view controller will be transitioned off-screen and subsequently destroyed. Any intermediary view controllers will be hidden prior to transition and subsequently destroyed on completion. This method requires <see cref="allowsRootViewControllerToBePopped"/> to be true.
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void PopAll(bool animated = true, System.Action completion = null)
        {
            PopToViewController(null, animated, false, completion);
        }

        /// <summary>
        /// Pop all view controllers off the stack interactively. The top view controller will be transitioned off-screen and subsequently destroyed. Any intermediary view controllers will be hidden prior to transition and subsequently destroyed on completion. This method requires <see cref="allowsRootViewControllerToBePopped"/> to be true. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, the view controller(s) will be popped without interactivity.
        /// </summary>
        /// <param name="completion"></param>
        public void PopAllInteractively(System.Action completion = null)
        {
            PopToViewController(null, true, true, completion);
        }

        /// <summary>
        /// Set the root view controller. Use this method to configure the stack controller's root view controller prior to presentation. Additionally, if the stack controller's view has already been loaded when this method is called, a replacement transition will be performed, unloading the existing stack of view controllers and transitioning to the new root view controller.
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void SetRoot(CanvasController viewController, bool animated = false, System.Action completion = null)
        {
            if (CanSetRootViewController(viewController, out Error error) == false)
            {
                error.LogWithPrefix("Cannot set root view controller.");
                return;
            }

            RootViewController = viewController;
            if (ViewIsLoaded)
            {
                var fromViewController = TopViewController;
                AddViewControllerAsChildAndPlaceViewInContainer(RootViewController);

                // Pass the current stack of view controllers (minus the top) as intermediaries and destroy them upon completion.
                Stack<CanvasController> intermediaryViewControllers = new Stack<CanvasController>(viewControllers.Count - 1);
                foreach (CanvasController vc in viewControllers)
                {
                    if (vc.Equals(fromViewController) == false)
                    {
                        intermediaryViewControllers.Push(vc);
                    }
                }

                viewControllers.Clear();
                viewControllers.Push(RootViewController);

                PerformChildTransition(SetRootTransitionIdentifier, RootViewController, fromViewController, animated, false, intermediaryViewControllers, OnTransitionDidFinish: (transition, completed) =>
                {
                    var context = transition.Context;
                    if (completed)
                    {
                        // Destroy all intermediary view controllers and the 'from' view controller.
                        foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                        {
                            intermediaryViewController.Destroy();
                        }
                        context.FromViewController.Destroy();

                        completion?.Invoke();
                    }
                });
            }
        }

        /// <summary>
        /// Can <paramref name="viewController"/> be pushed by the stack controller?
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="error"></param>
        /// <returns>True if <paramref name="viewController"/> can be pushed. False otherwise.</returns>
        public bool CanPushViewController(CanvasController viewController, out Error error)
        {
            if (CanPerformPushOrPopTransition(out error) == false)
            {
                return false;
            }

            if (viewController == null)
            {
                error = new Error("The provided view controller is null.");
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Can the stack controller pop a view controller?
        /// </summary>
        /// <param name="error"></param>
        /// <returns>True if a view controller can be popped. False otherwise.</returns>
        public bool CanPopViewController(out Error error)
        {
            if (CanPerformPushOrPopTransition(out error) == false)
            {
                return false;
            }

            if (viewControllers.Count == 0)
            {
                error = new Error("There are no view controllers on the stack.");
                return false;
            }

            if ((allowsRootViewControllerToBePopped == false) && (viewControllers.Count < 2))
            {
                error = new Error("Popping would pop the root view controller and the stack controller's 'allowsRootViewControllerToBePopped' is false.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Can the stack controller pop to <paramref name="viewController"/>?
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="error"></param>
        /// <returns>True if the stack controller can pop to <paramref name="viewController"/>. False otherwise.</returns>
        public bool CanPopToViewController(CanvasController viewController, out Error error)
        {
            if (CanPerformPushOrPopTransition(out error) == false)
            {
                return false;
            }

            if ((viewController != null) && (viewControllers.Contains(viewController) == false))
            {
                error = new Error("The provided view controller is not on the stack.");
                return false;
            }

            if (viewController == TopViewController)
            {
                error = new Error("The provided view controller is the top view controller.");
                return false;
            }

            if ((viewController == null) && (allowsRootViewControllerToBePopped == false))
            {
                error = new Error("The stack controller's 'allowsRootViewControllerToBePopped' is false.");
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Can the stack controller set its <see cref="RootViewController"/> to <paramref name="viewController"/>?
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="error"></param>
        /// <returns>True if the stack controller set its <see cref="RootViewController"/> to <paramref name="viewController"/>. False otherwise.</returns>
        public bool CanSetRootViewController(CanvasController viewController, out Error error)
        {
            if (IsPerformingTransitionBetweenChildren)
            {
                error = new Error("The stack controller is already performing a transition.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();
            if (RootViewController != null)
            {
                Push(RootViewController, false);
            }
        }

        protected override bool ImplementsTransition(ViewControllerTransitionIdentifier transitionIdentifier)
        {
            return (transitionIdentifier.Equals(PushTransitionIdentifier) || transitionIdentifier.Equals(PopTransitionIdentifier) || transitionIdentifier.Equals(PopToTransitionIdentifier) || transitionIdentifier.Equals(PopToRootTransitionIdentifier) || transitionIdentifier.Equals(PopAllTransitionIdentifier) || transitionIdentifier.Equals(SetRootTransitionIdentifier));
        }

        protected override void PerformInvokedTransition(ViewControllerTransitionIdentifier transitionIdentifier, CanvasControllerInvokeTransitionData transitionData, System.Action completion)
        {
            if (transitionIdentifier.Equals(PushTransitionIdentifier))
            {
                if (transitionData.Interactive)
                {
                    PushInteractively(transitionData.ToViewController, completion);
                }
                else
                {
                    Push(transitionData.ToViewController, transitionData.Animated, completion);
                }
            }
            else if (transitionIdentifier.Equals(PopTransitionIdentifier))
            {
                if (transitionData.Interactive)
                {
                    PopInteractively(completion);
                }
                else
                {
                    Pop(transitionData.Animated, completion);
                }
            }
            else if (transitionIdentifier.Equals(PopToTransitionIdentifier))
            {
                if (transitionData.Interactive)
                {
                    PopToInteractively(transitionData.ToViewController, completion);
                }
                else
                {
                    PopTo(transitionData.ToViewController, transitionData.Animated, completion);
                }
            }
            else if (transitionIdentifier.Equals(PopToRootTransitionIdentifier))
            {
                if (transitionData.Interactive)
                {
                    PopToRootInteractively(completion);
                }
                else
                {
                    PopToRoot(transitionData.Animated, completion);
                }
            }
            else if (transitionIdentifier.Equals(PopAllTransitionIdentifier))
            {
                if (transitionData.Interactive)
                {
                    PopAllInteractively(completion);
                }
                else
                {
                    PopAll(transitionData.Animated, completion);
                }
            }
            else if (transitionIdentifier.Equals(SetRootTransitionIdentifier))
            {
                SetRoot(transitionData.ToViewController, transitionData.Animated, completion);
            }
        }

        private void Push(CanvasController viewController, bool animated, bool interactive, System.Action completion)
        {
            if (CanPushViewController(viewController, out Error error) == false)
            {
                error.LogWithPrefix("Cannot push view controller.");
                return;
            }

            var fromViewController = TopViewController;
            viewControllers.Push(viewController);
            if ((RootViewController == null))
            {
                RootViewController = viewController;
            }
            AddViewControllerAsChildAndPlaceViewInContainer(viewController);

            PerformChildTransition(PushTransitionIdentifier, viewController, fromViewController, animated, interactive, OnTransitionDidFinish: (transition, completed) =>
            {
                var context = transition.Context;
                if (completed == false)
                {
                    // Pop the previously pushed view controller and destroy it.
                    var stackController = context.OwnerViewController as StackCanvasController;
                    stackController.viewControllers.Pop();
                    context.ToViewController.Destroy();
                }

                if (completed)
                {
                    completion?.Invoke();
                }
            });
        }

        private void Pop(bool animated, bool interactive, System.Action completion)
        {
            if (CanPopViewController(out Error error) == false)
            {
                error.LogWithPrefix("Cannot pop view controller.");
                return;
            }

            // Retrieve a reference to the second item on the stack by popping, peeking, and pushing back on.
            var topViewController = viewControllers.Pop();
            var toViewController = TopViewController;
            viewControllers.Push(topViewController);
            PopToViewController(toViewController, animated, interactive, completion);
        }

        private void PopToViewController(CanvasController viewController, bool animated, bool interactive, System.Action completion)
        {
            // If the stack view controller itself is passed, this signifies a 'PopAll' transition, which pops to null.
            if ((viewController != null) && viewController.Equals(this))
            {
                viewController = null;
            }

            if (CanPopToViewController(viewController, out Error error) == false)
            {
                error.LogWithPrefix("Cannot pop to view controller.");
                return;
            }

            var fromViewController = viewControllers.Pop();
            var intermediaryViewControllers = new Stack<CanvasController>();
            while ((TopViewController != null) && (TopViewController != viewController))
            {
                var topViewController = viewControllers.Pop();
                intermediaryViewControllers.Push(topViewController);
            }

            PerformChildTransition(PopTransitionIdentifier, viewController, fromViewController, animated, interactive, intermediaryViewControllers, OnTransitionBegan: (transition) =>
            {
                // Hide all intermediary view controllers before transition.
                var context = transition.Context;
                foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                {
                    intermediaryViewController.AsAppearable.BeginAppearanceTransition(false);
                    intermediaryViewController.View.Visible = false;
                    intermediaryViewController.AsAppearable.EndAppearanceTransition();
                }
            },
            OnTransitionDidFinish: (transition, completed) =>
            {
                var context = transition.Context;
                if (completed)
                {
                    // Destroy all intermediary view controllers after transition.
                    foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                    {
                        intermediaryViewController.Destroy();
                    }

                    context.FromViewController.Destroy();

                    completion?.Invoke();
                }
                else
                {
                    // From and intermediary view controllers are pushed back onto the stack and appear back on screen.
                    var stackController = context.OwnerViewController as StackCanvasController;
                    foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                    {
                        stackController.viewControllers.Push(intermediaryViewController);

                        intermediaryViewController.AsAppearable.BeginAppearanceTransition(true);
                        intermediaryViewController.View.Visible = true;
                        intermediaryViewController.AsAppearable.EndAppearanceTransition();
                    }

                    stackController.viewControllers.Push(context.FromViewController);
                }
            });
        }

        private void AddViewControllerAsChildAndPlaceViewInContainer(CanvasController viewController)
        {
            AddChild(viewController);

            var childView = viewController.View;
            viewControllersContainer.Add(childView);
            childView.FitToParent();
            childView.BringToFront();
        }

        private bool CanPerformPushOrPopTransition(out Error error)
        {
            if (ViewIsLoaded == false)
            {
                error = new Error("The stack controller's view has not been loaded.");
                return false;
            }

            if (IsPerformingTransitionBetweenChildren)
            {
                error = new Error("The stack controller is already performing a transition.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }
    }

    // Graphing support.
    public partial class StackCanvasController : CanvasController, IGraphableFieldProvider
    {
        private const string RootViewControllerGraphableFieldName = "Root View Controller";

        GraphableField[] IGraphableFieldProvider.GraphableFields
        {
            get
            {
                return new GraphableField[]
                {
                    new GraphableField(RootViewControllerGraphableFieldName)
                };
            }
        }

        void IGraphableFieldProvider.SetGraphableFieldValue(string fieldName, IGraphable[] graphables)
        {
            if (fieldName.Equals(RootViewControllerGraphableFieldName))
            {
                RootViewController = (CanvasController)graphables[0];
            }
        }

        protected override List<GraphableTransitionIdentifier> GraphableTransitionIdentifiers()
        {
            return new List<GraphableTransitionIdentifier>
            {
                new GraphableTransitionIdentifier(PushTransitionIdentifier, PopToTransitionIdentifier),
                new GraphableTransitionIdentifier(PopToTransitionIdentifier, PushTransitionIdentifier),
                new GraphableTransitionIdentifier(SetRootTransitionIdentifier)
            };
        }

        protected override bool ShouldPerformGraphTransition(GraphTransition<CanvasController> graphTransition, out Error error)
        {
            ViewControllerTransitionIdentifier transitionIdentifier = graphTransition.TransitionIdentifier;
            if (transitionIdentifier.Equals(PushTransitionIdentifier))
            {
                return CanPushViewController(graphTransition.ToViewController, out error);
            }
            else if (transitionIdentifier.Equals(PopToTransitionIdentifier))
            {
                CanvasController toViewController = graphTransition.ToViewController;

                // When graphing, the destination view controller can be the stack itself, which signifies a 'PopAll' transition.
                if (toViewController.Equals(this))
                {
                    toViewController = null;
                }

                return CanPopToViewController(toViewController, out error);
            }
            else if (transitionIdentifier.Equals(SetRootTransitionIdentifier))
            {
                return CanSetRootViewController(graphTransition.ToViewController, out error);
            }

            return base.ShouldPerformGraphTransition(graphTransition, out error);
        }
    }
}