// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A NavigationCanvasController is a specific type of <see cref="StackCanvasController"/>, designed to provide a user-interface for hierarchical navigation. Like a stack controller, it manages a stack of child view controllers. Each child view controller fills the <see cref="StackCanvasController.viewControllersContainer"/> transform and is placed in stack order, making the top-most view controller on the stack visible.
    /// <para>
    /// The NavigationCanvasController offers transitions for pushing and popping view controllers on and off its stack. Pushing a view controller onto the stack instantiates its view, embeds it within the navigation controller's <see cref="StackCanvasController.viewControllersContainer"/>, and if necessary animates it on-screen. Inversely, popping a view controller off the stack animates it off-screen if necessary, and subsequently destroys it. 
    /// </para>
    /// <para>
    /// In addition, the NavigationCanvasController shows a navigation bar, which includes a title label and back button. Pressing the back button pops a view controller off the stack. By default, a navigation controller animates child view controllers sideways to reflect moving through levels of a hierarchy. 
    /// </para>
    /// <para>
    /// Use a NavigationCanvasController to provide a hierarchical interface scheme.
    /// </para>
    /// </summary>
    public partial class NavigationCanvasController : StackCanvasController
    {
        /// <summary>
        /// The transition identifier for the Navigation Canvas Controller's Push transition.
        /// </summary>
        public new static readonly ViewControllerTransitionIdentifier PushTransition = new ViewControllerTransitionIdentifier("NavigationCanvasController.Push", ViewControllerTransitionIdentifier.TransitionType.Presentation, "Push");
        /// <summary>
        /// The transition identifier for the Navigation Canvas Controller's Pop transition.
        /// </summary>
        public new static readonly ViewControllerTransitionIdentifier PopTransition = new ViewControllerTransitionIdentifier("NavigationCanvasController.Pop", ViewControllerTransitionIdentifier.TransitionType.Dismissal);
        /// <summary>
        /// The transition identifier for the Navigation Canvas Controller's Pop To transition.
        /// </summary>
        public new static readonly ViewControllerTransitionIdentifier PopToTransition = new ViewControllerTransitionIdentifier("NavigationCanvasController.PopTo", ViewControllerTransitionIdentifier.TransitionType.Dismissal, "Pop To");
        /// <summary>
        /// The transition identifier for the Navigation Canvas Controller's Pop To Root transition.
        /// </summary>
        public new static readonly ViewControllerTransitionIdentifier PopToRootTransition = new ViewControllerTransitionIdentifier("NavigationCanvasController.PopToRoot", ViewControllerTransitionIdentifier.TransitionType.Dismissal);
        /// <summary>
        /// The transition identifier for the Navigation Canvas Controller's Set Root transition.
        /// </summary>
        public new static readonly ViewControllerTransitionIdentifier SetRootTransition = new ViewControllerTransitionIdentifier("NavigationCanvasController.SetRoot", ViewControllerTransitionIdentifier.TransitionType.Replacement, "Set Root");

        protected override ViewControllerTransitionIdentifier PushTransitionIdentifier
        {
            get
            {
                return PushTransition;
            }
        }

        protected override ViewControllerTransitionIdentifier PopTransitionIdentifier
        {
            get
            {
                return PopTransition;
            }
        }

        protected override ViewControllerTransitionIdentifier PopToTransitionIdentifier
        {
            get
            {
                return PopToTransition;
            }
        }

        protected override ViewControllerTransitionIdentifier PopToRootTransitionIdentifier
        {
            get
            {
                return PopToRootTransition;
            }
        }

        protected override ViewControllerTransitionIdentifier SetRootTransitionIdentifier
        {
            get
            {
                return SetRootTransition;
            }
        }

        protected virtual void OnValidate()
        {
            if (allowsRootViewControllerToBePopped == true)
            {
                allowsRootViewControllerToBePopped = false;
                Error.Log("A navigation controller does not allow its root view controller to be popped.", Error.Severity.Log);
            }
        }
    }
}