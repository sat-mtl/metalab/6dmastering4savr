﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.Events;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The tab bar item component associated with a <see cref="CanvasTabBar"/>.
    /// </summary>
    public class CanvasTabBarItem : MonoBehaviour
    {
        /// <summary>
        /// The tab bar item's index. You may set this to a value in order to predefine a tab bar item for a specific index.
        /// </summary>
        public int index;

        protected bool selected;

        private readonly CanvasTabBarItemEvent OnClicked = new CanvasTabBarItemEvent();

        /// <summary>
        /// An event raised when the tab bar item is configured, passing the <see cref="CanvasTabBarItem"/>, <see cref="CanvasTabBar"/>, and the corresponding <see cref="CanvasController"/>.
        /// </summary>
        public CanvasTabBarItemConfigurationEvent OnConfiguredForTabBarWithViewController;
        /// <summary>
        /// An event raised when the tab bar item is selected.
        /// </summary>
        public CanvasTabBarItemEvent OnSelected;
        /// <summary>
        /// An event raised when the tab bar item is deselected.
        /// </summary>
        public CanvasTabBarItemEvent OnDeselected;

        /// <summary>
        /// Configure the item. This method sets the item's index and invokes the <see cref="OnConfiguredForTabBarWithViewController"/> event.
        /// </summary>
        /// <param name="tabBar"></param>
        /// <param name="viewController"></param>
        /// <param name="index"></param>
        public void ConfigureForTabBarWithViewControllerAtIndex(CanvasTabBar tabBar, CanvasController viewController, int index)
        {
            this.index = index;
            OnConfiguredForTabBarWithViewController.Invoke(this, tabBar, viewController);
        }

        /// <summary>
        /// Bind <paramref name="callback"/> to the 'on clicked' event.
        /// </summary>
        /// <param name="callback"></param>
        public void BindToClick(UnityAction<CanvasTabBarItem> callback)
        {
            OnClicked.AddListener(callback);
        }

        /// <summary>
        /// Set the selection state of the tab bar item. This method invokes the appropriate event – <see cref="OnSelected"/> or <see cref="OnDeselected"/>.
        /// </summary>
        /// <param name="selected"></param>
        public void SetSelected(bool selected)
        {
            this.selected = selected;
            if (selected)
            {
                OnSelected.Invoke(this);
            }
            else
            {
                OnDeselected.Invoke(this);
            }
        }

        /// <summary>
        /// Invoke the tab bar item's 'on clicked' event.
        /// </summary>
        public void InvokeOnClicked()
        {
            OnClicked.Invoke(this);
        }

        /// <summary>
        /// Event type for <see cref="CanvasTabBarItem"/> selection.
        /// </summary>
        [System.Serializable] public class CanvasTabBarItemEvent : UnityEvent<CanvasTabBarItem> { }
        /// <summary>
        /// Event type for <see cref="CanvasTabBarItem"/> configuration.
        /// </summary>
        [System.Serializable] public class CanvasTabBarItemConfigurationEvent : UnityEvent<CanvasTabBarItem, CanvasTabBar, CanvasController> { }
    }
}