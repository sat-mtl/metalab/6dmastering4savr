﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The SplitCanvasController is a container view controller designed for presenting two screens alongside one other. It manages two child view controllers – <see cref="MainViewController"/> and <see cref="SecondaryViewController"/>. It offers transitions for replacing the main and secondary view controllers, as well as showing and hiding the <see cref="SecondaryViewController"/>. The child view controllers fill their respective containers, <see cref="mainViewControllerContainer"/> and <see cref="secondaryViewControllerContainer"/>.
    /// <para>
    /// Replacing the main or secondary view controllers instantiates and embeds the provided view controller in the appropriate container, <see cref="MainViewController"/> and <see cref="SecondaryViewController"/>. Hiding the secondary view controller only hides it, it does not unload it. 
    /// </para>
    /// <para>
    /// The SplitCanvasController uses a <see cref="SplitCanvasPositioning"/> component to determine the position of its two content view controllers in each state – Secondary Visible and Secondary Hidden. Use the relevant transform objects to configure the placement of the main and secondary view controllers in each state. By default, the split controller uses a bespoke transition animation to animate the <see cref="SecondaryViewController"/> visibility transition, interpolating between the <see cref="SplitCanvasPositioning"/> component's two states. 
    /// </para>
    /// </summary>
    public partial class SplitCanvasController : CanvasController
    {
        /// <summary>
        /// The transition identifier for the Split Canvas Controller's Set Main transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier SetMainViewControllerTransition = new ViewControllerTransitionIdentifier("SplitCanvasController.SetMain", ViewControllerTransitionIdentifier.TransitionType.Replacement, "Set Main");
        /// <summary>
        /// The transition identifier for the Split Canvas Controller's Set Secondary transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier SetSecondaryViewControllerTransition = new ViewControllerTransitionIdentifier("SplitCanvasController.SetSecondary", ViewControllerTransitionIdentifier.TransitionType.Replacement, "Set Secondary");
        /// <summary>
        /// The transition identifier for the Split Canvas Controller's Show Secondary transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier ShowSecondaryViewControllerTransition = new ViewControllerTransitionIdentifier("SplitCanvasController.ShowSecondary", ViewControllerTransitionIdentifier.TransitionType.Presentation);
        /// <summary>
        /// The transition identifier for the Split Canvas Controller's Hide Secondary transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier HideSecondaryViewControllerTransition = new ViewControllerTransitionIdentifier("SplitCanvasController.HideSecondary", ViewControllerTransitionIdentifier.TransitionType.Dismissal);

        private const int MainViewControllerIndex = 0;
        private const int SecondaryViewControllerIndex = 1;
        private const int ViewControllerCount = 2;
        private readonly CanvasController[] viewControllers = new CanvasController[ViewControllerCount];

        /// <summary>
        /// Is the secondary view controller visible on load? If enabled the secondary view controller will be made visible on load, otherwise it will be hidden.
        /// </summary>
        public bool secondaryViewControllerIsVisibleOnLoad = true;
        /// <summary>
        /// Allow transitions to be interrupted?  If true, invoking one of the split view controller's transitions whilst it is already in progress will cancel the current transition before starting a new transition. If false, invoking one of the split view controller's transitions whilst it is already in progress will be blocked.
        /// </summary>
        public bool allowTransitionsToBeInterrupted = true;
        /// <summary>
        /// Hide the secondary view controller when main is set? If enabled, the secondary view controller will be hidden if visible when a SetMain transition is invoked.
        /// </summary>
        public bool hideSecondaryOnSetMain = true;
        /// <summary>
        /// The container into which the split controller will place its <see cref="MainViewController"/>.
        /// </summary>
        [ViewReference] public RectTransform mainViewControllerContainer;
        /// <summary>
        /// The container into which the split controller will place its <see cref="SecondaryViewController"/>.
        /// </summary>
        [ViewReference] public RectTransform secondaryViewControllerContainer;
        /// <summary>
        /// The split controller's split positioning component. This component is responsible for determining the layout of the main and secondary containers in each configuration - Secondary Visible and Secondary Hidden.
        /// </summary>
        [ViewReference] public SplitCanvasPositioning splitPositioning;

        /// <summary>
        /// The split controller's main view controller.
        /// </summary>
        public CanvasController MainViewController
        {
            get
            {
                return viewControllers[MainViewControllerIndex];
            }

            private set
            {
                viewControllers[MainViewControllerIndex] = value;
            }
        }

        /// <summary>
        /// The split controller's secondary view controller.
        /// </summary>
        public CanvasController SecondaryViewController
        {
            get
            {
                return viewControllers[SecondaryViewControllerIndex];
            }

            private set
            {
                viewControllers[SecondaryViewControllerIndex] = value;
            }
        }

        /// <summary>
        /// Is the secondary view controller currently visible?
        /// </summary>
        /// <seealso cref="ShowSecondaryViewController(bool, System.Action)"/>
        /// <seealso cref="HideSecondaryViewController(bool, System.Action)"/>
        public bool SecondaryViewControllerIsVisible { get; private set; } = true;

        private bool AnySecondaryViewControllerTransitionIsInProgress
        {
            get
            {
                return TryGetChildTransition(SetSecondaryViewControllerTransition, out _) || TryGetChildTransition(ShowSecondaryViewControllerTransition, out _) || TryGetChildTransition(HideSecondaryViewControllerTransition, out _);
            }
        }

        /// <summary>
        /// Set the split controller's main view controller. <paramref name="viewController"/> will be added as a child view controller and its view placed in the <see cref="mainViewControllerContainer"/>. Use this method to configure the split controller's main view controller prior to presentation. Additionally, if the split controller's view has already been loaded when this method is called, a replacement transition will be performed, unloading the current main view controller and transitioning to the new one.
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void SetMainViewController(CanvasController viewController, bool animated = false, System.Action completion = null)
        {
            if (CanSetMainViewController(viewController, out Error error) == false)
            {
                error.LogWithPrefix("Cannot set main view controller.");
                return;
            }

            var fromViewController = MainViewController;
            MainViewController = viewController;
            if (ViewIsLoaded)
            {
                ReplaceMainViewController(MainViewController, fromViewController, animated, completion);
                if (hideSecondaryOnSetMain && SecondaryViewControllerIsVisible)
                {
                    HideSecondaryViewController(animated);
                }
            }
        }

        /// <summary>
        /// Set the split controller's secondary view controller. <paramref name="viewController"/> will be added as a child view controller and its view placed in the <see cref="secondaryViewControllerContainer"/>. Use this method to configure the split controller's secondary view controller prior to presentation. Additionally, if the split controller's view has already been loaded when this method is called, a replacement transition will be performed, unloading the current secondary view controller and transitioning to the new one.
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void SetSecondaryViewController(CanvasController viewController, bool animated = false, System.Action completion = null)
        {
            if (CanSetSecondaryViewController(viewController, out Error error) == false)
            {
                error.LogWithPrefix("Cannot set secondary view controller.");
                return;
            }

            var fromViewController = SecondaryViewController;
            SecondaryViewController = viewController;
            if (ViewIsLoaded)
            {
                ReplaceSecondaryViewController(SecondaryViewController, fromViewController, animated, completion);
            }
        }

        /// <summary>
        /// Show the split controller's secondary view controller.
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void ShowSecondaryViewController(bool animated = true, System.Action completion = null)
        {
            ShowSecondaryViewController(animated, false, completion);
        }

        /// <summary>
        /// Show the split controller's secondary view controller interactively. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, the secondary view controller will be shown without interactivity.
        /// </summary>
        /// <param name="completion"></param>
        public void ShowSecondaryViewControllerInteractively(System.Action completion = null)
        {
            ShowSecondaryViewController(true, true, completion);
        }

        /// <summary>
        /// Hide the split controller's secondary view controller.
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void HideSecondaryViewController(bool animated = true, System.Action completion = null)
        {
            HideSecondaryViewController(animated, false, completion);
        }

        /// <summary>
        /// Hide the split controller's secondary view controller. Prior to calling this method, you should set the view controller's <see cref="interactiveTransitionProgressProvider"/> to an appropriate object. If no <see cref="interactiveTransitionProgressProvider"/> has been configured, the secondary view controller will be hidden without interactivity.
        /// </summary>
        /// <param name="completion"></param>
        public void HideSecondaryViewControllerInteractively(System.Action completion = null)
        {
            HideSecondaryViewController(true, true, completion);
        }

        /// <summary>
        /// Can the split controller set its <see cref="MainViewController"/> to <paramref name="viewController"/>?
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="error"></param>
        /// <returns>True if the split controller can set its <see cref="MainViewController"/> to <paramref name="viewController"/>. False otherwise.</returns>
        public bool CanSetMainViewController(CanvasController viewController, out Error error)
        {
            if (viewController == null)
            {
                error = new Error("The provided view controller is null.", Error.Severity.Error);
                return false;
            }

            if ((allowTransitionsToBeInterrupted == false) && TryGetChildTransition(SetMainViewControllerTransition, out _))
            {
                error = new Error("The view controller transition is already in progress and allowTransitionsToBeInterrupted is false.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Can the split controller set its <see cref="SecondaryViewController"/> to <paramref name="viewController"/>?
        /// </summary>
        /// <param name="viewController"></param>
        /// <param name="error"></param>
        /// <returns>True if the split controller can set its <see cref="SecondaryViewController"/> to <paramref name="viewController"/>. False otherwise.</returns>
        public bool CanSetSecondaryViewController(CanvasController viewController, out Error error)
        {
            if (viewController == null)
            {
                error = new Error("The provided view controller is null.", Error.Severity.Error);
                return false;
            }

            if ((allowTransitionsToBeInterrupted == false) && AnySecondaryViewControllerTransitionIsInProgress)
            {
                error = new Error("The view controller transition is already in progress and allowTransitionsToBeInterrupted is false.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Can the split view controller toggle its <see cref="SecondaryViewController"/> visibility?
        /// </summary>
        /// <param name="visible"></param>
        /// <param name="error"></param>
        /// <returns>True if the split controller can toggle its <see cref="SecondaryViewController"/> visibility. False otherwise.</returns>
        public bool CanToggleSecondaryViewControllerVisibility(bool visible, out Error error)
        {
            if (visible == SecondaryViewControllerIsVisible)
            {
                error = new Error(string.Format("The secondary view controller is already {0}.", ((visible) ? "visible" : "hidden")), Error.Severity.Log);
                return false;
            }

            if (SecondaryViewController == null)
            {
                error = new Error("The secondary view controller is null.", Error.Severity.Error);
                return false;
            }

            if ((allowTransitionsToBeInterrupted == false) && AnySecondaryViewControllerTransitionIsInProgress)
            {
                error = new Error("The view controller transition is already in progress and allowTransitionsToBeInterrupted is false.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }

        /// <summary>
        /// Toggle the <see cref="SecondaryViewController"/> visibility. A convenience method for calling either <see cref="ShowSecondaryViewController(bool, System.Action)"/> or <see cref="HideSecondaryViewController(bool, System.Action)"/> depending upon that state of <see cref="SecondaryViewControllerIsVisible"/>.
        /// </summary>
        /// <param name="animated"></param>
        /// <param name="completion"></param>
        public void ToggleSecondaryViewControllerVisibility(bool animated = true, System.Action completion = null)
        {
            if (SecondaryViewControllerIsVisible)
            {
                HideSecondaryViewController(animated, completion);
            }
            else
            {
                ShowSecondaryViewController(animated, completion);
            }
        }

        /// <summary>
        /// Toggle the <see cref="SecondaryViewController"/> visibility. A convenience method for calling either <see cref="ShowSecondaryViewControllerInteractively(System.Action)"/> or <see cref="HideSecondaryViewControllerInteractively(System.Action)"/> depending upon that state of <see cref="SecondaryViewControllerIsVisible"/>.
        /// </summary>
        /// <param name="completion"></param>
        public void ToggleSecondaryViewControllerVisibilityInteractively(System.Action completion = null)
        {
            if (SecondaryViewControllerIsVisible)
            {
                HideSecondaryViewControllerInteractively(completion);
            }
            else
            {
                ShowSecondaryViewControllerInteractively(completion);
            }
        }

        /// <summary>
        /// A view callback for toggling the <see cref="SecondaryViewController"/> visibility. This method calls <see cref="ToggleSecondaryViewControllerVisibility(bool, System.Action)"/>.
        /// </summary>
        /// <param name="animated"></param>
        [ViewCallback]
        public void ToggleSecondaryViewControllerVisibility(bool animated)
        {
            ToggleSecondaryViewControllerVisibility(animated, null);
        }

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (MainViewController != null)
            {
                AddViewControllerAsChildAndAddViewToContainer(MainViewController, mainViewControllerContainer);
            }

            SecondaryViewControllerIsVisible = secondaryViewControllerIsVisibleOnLoad;
            if (SecondaryViewController != null)
            {
                AddViewControllerAsChildAndAddViewToContainer(SecondaryViewController, secondaryViewControllerContainer);

                bool animated = false;
                if (SecondaryViewControllerIsVisible)
                {
                    PerformShowSecondaryViewControllerTransition(SecondaryViewController, null, animated, false, null);
                }
                else
                {
                    PerformHideSecondaryViewControllerTransition(null, SecondaryViewController, animated, false, null);
                }
            }
        }

        protected override bool ImplementsTransition(ViewControllerTransitionIdentifier transitionIdentifier)
        {
            return (transitionIdentifier.Equals(SetMainViewControllerTransition) || transitionIdentifier.Equals(SetSecondaryViewControllerTransition) || transitionIdentifier.Equals(ShowSecondaryViewControllerTransition) || transitionIdentifier.Equals(HideSecondaryViewControllerTransition));
        }

        protected override void PerformInvokedTransition(ViewControllerTransitionIdentifier transitionIdentifier, CanvasControllerInvokeTransitionData transitionData, System.Action completion)
        {
            if (transitionIdentifier.Equals(SetMainViewControllerTransition))
            {
                SetMainViewController(transitionData.ToViewController, transitionData.Animated, completion);
            }
            else if (transitionIdentifier.Equals(SetSecondaryViewControllerTransition))
            {
                SetSecondaryViewController(transitionData.ToViewController, transitionData.Animated, completion);
            }
            else if (transitionIdentifier.Equals(ShowSecondaryViewControllerTransition))
            {
                if (transitionData.Interactive)
                {
                    ShowSecondaryViewControllerInteractively(completion);
                }
                else
                {
                    ShowSecondaryViewController(transitionData.Animated, completion);
                }
            }
            else if (transitionIdentifier.Equals(HideSecondaryViewControllerTransition))
            {
                if (transitionData.Interactive)
                {
                    HideSecondaryViewControllerInteractively(completion);
                }
                else
                {
                    HideSecondaryViewController(transitionData.Animated, completion);
                }
            }
        }

        private void ShowSecondaryViewController(bool animated, bool interactive, System.Action completion)
        {
            bool visible = true;

            // If the view has not been loaded, update the secondaryViewControllerIsVisibleOnLoad to respect the latest set value.
            if (ViewIsLoaded == false)
            {
                secondaryViewControllerIsVisibleOnLoad = visible;
            }

            if (CanToggleSecondaryViewControllerVisibility(visible, out Error error) == false)
            {
                error.LogWithPrefix("Cannot show secondary view controller.");
                return;
            }

            SecondaryViewControllerIsVisible = visible;
            if (ViewIsLoaded)
            {
                PerformShowSecondaryViewControllerTransition(SecondaryViewController, null, animated, interactive, completion);
            }
        }

        private void HideSecondaryViewController(bool animated, bool interactive, System.Action completion)
        {
            bool visible = false;

            // If the view has not been loaded, update the secondaryViewControllerIsVisibleOnLoad to respect the latest set value.
            if (ViewIsLoaded == false)
            {
                secondaryViewControllerIsVisibleOnLoad = visible;
            }

            if (CanToggleSecondaryViewControllerVisibility(visible, out Error error) == false)
            {
                error.LogWithPrefix("Cannot hide secondary view controller.");
                return;
            }

            SecondaryViewControllerIsVisible = visible;
            if (ViewIsLoaded)
            {
                PerformHideSecondaryViewControllerTransition(null, SecondaryViewController, animated, interactive, completion);
            }
        }

        private void ReplaceMainViewController(CanvasController toViewController, CanvasController fromViewController, bool animated, System.Action completion)
        {
            PerformReplacementViewControllerTransition(SetMainViewControllerTransition, toViewController, mainViewControllerContainer, fromViewController, animated, completion);
        }

        private void ReplaceSecondaryViewController(CanvasController toViewController, CanvasController fromViewController, bool animated, System.Action completion)
        {
            // If the secondary view controller is not visible, do not animate the transition and set the to-view-controller to be hidden.
            if (SecondaryViewControllerIsVisible == false)
            {
                // [Developer Note] In this scenario, the to-view-controller's appearance methods are called here. Ideally, they should be delayed until the view controller is subsequently shown.
                animated = false;
                PerformReplacementViewControllerTransition(SetSecondaryViewControllerTransition, toViewController, secondaryViewControllerContainer, fromViewController, animated, completion);
                toViewController.View.Visible = false;
            }
            else
            {
                PerformReplacementViewControllerTransition(SetSecondaryViewControllerTransition, toViewController, secondaryViewControllerContainer, fromViewController, animated, completion);
            }
        }

        private void PerformReplacementViewControllerTransition(ViewControllerTransitionIdentifier identifier, CanvasController toViewController, RectTransform toViewControllerContainer, CanvasController fromViewController, bool animated, System.Action completion)
        {
            CancelTransitionsIfNecessary(identifier);

            AddViewControllerAsChildAndAddViewToContainer(toViewController, toViewControllerContainer);
            PerformChildTransition(identifier, toViewController, fromViewController, animated, false, OnTransitionDidFinish: (transition, completed) =>
            {
                if (completed)
                {
                    var context = transition.Context;
                    context.FromViewController?.Destroy();

                    completion?.Invoke();
                }
            });
        }

        private void PerformShowSecondaryViewControllerTransition(CanvasController toViewController, CanvasController fromViewController, bool animated, bool interactive, System.Action completion)
        {
            CancelTransitionsIfNecessary(ShowSecondaryViewControllerTransition, HideSecondaryViewControllerTransition);

            toViewController.View.Visible = true;
            PerformChildTransition(ShowSecondaryViewControllerTransition, toViewController, fromViewController, animated, interactive, OnTransitionWillFinish: (transition, completed) =>
            {
                if (completed)
                {
                    CanvasControllerTransitionContext context = transition.Context;
                    SplitCanvasController splitCanvasController = context.OwnerViewController as SplitCanvasController;
                    splitCanvasController.splitPositioning.UpdateSplit(context.Identifier, splitCanvasController, 1f);
                }
            },
            OnTransitionDidFinish: (transition, completed) =>
            {
                if (completed)
                {
                    completion?.Invoke();
                }
                else
                {
                    toViewController.View.Visible = false;
                    SecondaryViewControllerIsVisible = false;
                }
            });
        }

        private void PerformHideSecondaryViewControllerTransition(CanvasController toViewController, CanvasController fromViewController, bool animated, bool interactive, System.Action completion)
        {
            CancelTransitionsIfNecessary(ShowSecondaryViewControllerTransition, HideSecondaryViewControllerTransition);
            PerformChildTransition(HideSecondaryViewControllerTransition, toViewController, fromViewController, animated, interactive, OnTransitionWillFinish: (transition, completed) =>
            {
                if (completed)
                {
                    CanvasControllerTransitionContext context = transition.Context;
                    SplitCanvasController splitCanvasController = context.OwnerViewController as SplitCanvasController;
                    splitCanvasController.splitPositioning.UpdateSplit(context.Identifier, splitCanvasController, 1f);
                }
            }, OnTransitionDidFinish: (transition, completed) =>
            {
                if (completed)
                {
                    var context = transition.Context;
                    context.FromViewController.View.Visible = false;

                    completion?.Invoke();
                }
                else
                {
                    SecondaryViewControllerIsVisible = true;
                }
            });
        }

        private void AddViewControllerAsChildAndAddViewToContainer(CanvasController viewController, RectTransform container)
        {
            AddChild(viewController);

            var childView = viewController.View;
            container.Add(childView);
            childView.FitToParent();
        }

        private void CancelTransitionsIfNecessary(params ViewControllerTransitionIdentifier[] transitionIdentifiers)
        {
            foreach (ViewControllerTransitionIdentifier transitionIdentifier in transitionIdentifiers)
            {
                if (TryGetChildTransition(transitionIdentifier, out var transition))
                {
                    transition.ForceImmediateCompletion();
                }
            }
        }
    }

    // Graphing support.
    public partial class SplitCanvasController : CanvasController, IGraphableFieldProvider
    {
        private const string MainViewControllerGraphableFieldName = "Main View Controller";
        private const string SecondaryViewControllerGraphableFieldName = "Secondary View Controller";

        GraphableField[] IGraphableFieldProvider.GraphableFields
        {
            get
            {
                return new GraphableField[]
                {
                    new GraphableField(MainViewControllerGraphableFieldName),
                    new GraphableField(SecondaryViewControllerGraphableFieldName)
                };
            }
        }

        void IGraphableFieldProvider.SetGraphableFieldValue(string fieldName, IGraphable[] graphables)
        {
            if (fieldName.Equals(MainViewControllerGraphableFieldName))
            {
                MainViewController = (CanvasController)graphables[0];
            }
            else if (fieldName.Equals(SecondaryViewControllerGraphableFieldName))
            {
                SecondaryViewController = (CanvasController)graphables[0];
            }
        }

        protected override List<GraphableTransitionIdentifier> GraphableTransitionIdentifiers()
        {
            return new List<GraphableTransitionIdentifier>
            {
                new GraphableTransitionIdentifier(SetMainViewControllerTransition),
                new GraphableTransitionIdentifier(SetSecondaryViewControllerTransition)
            };
        }

        protected override bool ShouldPerformGraphTransition(GraphTransition<CanvasController> graphTransition, out Error error)
        {
            ViewControllerTransitionIdentifier transitionIdentifier = graphTransition.TransitionIdentifier;
            if (transitionIdentifier.Equals(SetMainViewControllerTransition))
            {
                return CanSetMainViewController(graphTransition.ToViewController, out error);
            }
            else if (transitionIdentifier.Equals(SetSecondaryViewControllerTransition))
            {
                return CanSetSecondaryViewController(graphTransition.ToViewController, out error);
            }

            return base.ShouldPerformGraphTransition(graphTransition, out error);
        }
    }
}