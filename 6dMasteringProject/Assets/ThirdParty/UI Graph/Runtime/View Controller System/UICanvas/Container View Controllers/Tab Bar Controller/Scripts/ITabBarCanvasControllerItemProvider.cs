﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// Use an ITabBarCanvasControllerItemProvider to manually instantiate a tab bar controller's items from script. Set the <see cref="TabBarCanvasController.itemProvider"/> to your implementation and the tab bar will ask your provider for a new tab bar item when it is loaded.
    /// </summary>
    public interface ITabBarCanvasControllerItemProvider
    {
        /// <summary>
        /// Instantiate a new tab bar item for the <paramref name="viewController"/> at <paramref name="index"/>. You are responsible for instantiating the tab bar item and configuring any custom components. The tab bar will perform the additional configuration required to place the item in the <see cref="CanvasTabBar.tabBarItemsContainer"/>, set the item's index, and bind its click to the tab bar.
        /// </summary>
        /// <param name="tabBar"></param>
        /// <param name="viewController"></param>
        /// <param name="index"></param>
        /// <returns>A newly instantiated <see cref="CanvasTabBarItem"/></returns>
        CanvasTabBarItem TabBarItemForViewControllerAtIndex(CanvasTabBar tabBar, CanvasController viewController, int index);
    }
}