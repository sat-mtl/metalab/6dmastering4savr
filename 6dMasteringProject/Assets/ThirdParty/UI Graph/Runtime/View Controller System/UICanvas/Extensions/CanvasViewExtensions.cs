﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// Canvas view extension methods.
    /// </summary>
    public static class CanvasViewExtensions
    {
        /// <summary>
        /// Convert <paramref name="screenPosition"/> to a normalized local position within <paramref name="view"/>.
        /// </summary>
        /// <param name="view"></param>
        /// <param name="screenPosition"></param>
        /// <returns></returns>
        public static Vector2 ScreenPositionToNormalizedLocalPosition(this CanvasView view, Vector2 screenPosition)
        {
            Vector2 localPosition = ScreenPositionToLocalPosition(view, screenPosition);
            return Rect.PointToNormalized(view.RectTransform.rect, localPosition);
        }

        /// <summary>
        /// Convert <paramref name="screenPosition"/> to a local position within <paramref name="view"/>.
        /// </summary>
        /// <param name="view"></param>
        /// <param name="screenPosition"></param>
        /// <returns></returns>
        public static Vector2 ScreenPositionToLocalPosition(this CanvasView view, Vector2 screenPosition)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(view.RectTransform, screenPosition, view.Canvas.worldCamera, out Vector2 localPosition);
            return localPosition;
        }
    }
}