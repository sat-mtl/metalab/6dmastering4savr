﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// RectTransform extension methods.
    /// </summary>
    public static class RectTransformExtensions
    {
        /// <summary>
        /// Convenience for adding a <see cref="CanvasView"/> as a child of a RectTransform.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="canvasView"></param>
        public static void Add(this RectTransform parent, CanvasView canvasView)
        {
            var transform = canvasView.RectTransform;
            transform.SetParent(parent, false);
        }

        /// <summary>
        /// Make a RectTransform fill its parent.
        /// </summary>
        /// <param name="rectTransform"></param>
        public static void FillParent(this RectTransform rectTransform)
        {
            rectTransform.anchorMin = Vector2.zero;
            rectTransform.anchorMax = Vector2.one;
            rectTransform.pivot = new Vector2(0.5f, 0.5f);

            rectTransform.offsetMin = Vector2.zero;
            rectTransform.offsetMax = Vector2.zero;
        }

        /// <summary>
        /// Interpolate between two RectTransform components.
        /// </summary>
        /// <param name="target"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="progress01"></param>
        public static void LerpUnclamped(this RectTransform target, RectTransform from, RectTransform to, float progress01)
        {
            target.anchorMin = Vector2.LerpUnclamped(from.anchorMin, to.anchorMin, progress01);
            target.anchorMax = Vector2.LerpUnclamped(from.anchorMax, to.anchorMax, progress01);
            target.pivot = Vector2.LerpUnclamped(from.pivot, to.pivot, progress01);

            Vector2 size = Vector2.LerpUnclamped(from.rect.size, to.rect.size, progress01);
            target.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
            target.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);

            target.position = Vector3.LerpUnclamped(from.position, to.position, progress01);
            target.rotation = Quaternion.SlerpUnclamped(from.rotation, to.rotation, progress01);
            target.localScale = Vector3.LerpUnclamped(from.localScale, to.localScale, progress01);
        }
    }
}