﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Reflection;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public class CanvasViewCallbackFactory
    {
        public static CanvasViewCallback Create(MethodInfo methodInfo, Transform parent)
        {
            GameObject gameObject = new GameObject();
            gameObject.transform.SetParent(parent, false);

            System.Type viewCallbackType = TypeForMethod(methodInfo);
            var viewCallback = gameObject.AddComponent(viewCallbackType) as CanvasViewCallback;
            viewCallback.Initialize(methodInfo);

            gameObject.name = viewCallback.DisplayName;

            return viewCallback;
        }

        private static System.Type TypeForMethod(MethodInfo methodInfo)
        {
            System.Type type = null;

            ParameterInfo[] parameters = methodInfo.GetParameters();
            if (parameters.Length > 1)
            {
                type = typeof(CanvasViewCallbackMultipleParameters);
            }
            else if (parameters.Length == 1)
            {
                var parameterType = parameters[0].ParameterType;
                if (parameterType == typeof(int))
                {
                    type = typeof(CanvasViewCallbackInt);
                }
                else if (parameterType == typeof(bool))
                {
                    type = typeof(CanvasViewCallbackBool);
                }
                else if (parameterType == typeof(float))
                {
                    type = typeof(CanvasViewCallbackFloat);
                }
                else if (parameterType == typeof(string))
                {
                    type = typeof(CanvasViewCallbackString);
                }
                else if ((parameterType == typeof(Object)) || parameterType.IsSubclassOf(typeof(Object)))
                {
                    type = typeof(CanvasViewCallbackUnityObject);
                }
                else if ((parameterType == typeof(object)) || parameterType.IsSubclassOf(typeof(object)))
                {
                    type = typeof(CanvasViewCallbackSystemObject);
                }
            }

            if (type == null)
            {
                type = typeof(CanvasViewCallbackNoParameters);
            }

            return type;
        }
    }
}