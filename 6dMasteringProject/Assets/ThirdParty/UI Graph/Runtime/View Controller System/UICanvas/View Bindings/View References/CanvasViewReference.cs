﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// A canvas view reference links a canvas controller field, decorated with the <see cref="ViewReferenceAttribute"/>, with a component in its view.
    /// </summary>
    [System.Serializable]
    public class CanvasViewReference : ViewReference
    {
        /// <summary>
        /// The component in the view to which this view reference points.
        /// </summary>
        public Object objectReference;

#pragma warning disable 0649
        [SerializeField] private string assemblyQualifiedTypeName;
#pragma warning restore 0649

        public CanvasViewReference(string identifier, string assemblyQualifiedTypeName)
        {
            this.identifier = identifier;
            this.assemblyQualifiedTypeName = assemblyQualifiedTypeName;
        }

        /// <summary>
        /// Does the view reference point to a valid component in the view?
        /// </summary>
        public bool HasObjectReference
        {
            get
            {
                return (objectReference != null);
            }
        }

#if UNITY_EDITOR
        public string AssemblyQualifiedTypeName
        {
            get
            {
                return assemblyQualifiedTypeName;
            }
        }
#endif
    }
}