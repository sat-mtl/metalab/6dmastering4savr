﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Pelican7.UIGraph
{
    public partial class CanvasViewResource : ViewResource<CanvasView, CanvasWindow>
    {
        public CanvasView prefab;

        public override CanvasView Load(out IViewBindable viewOwnerBindings)
        {
            CanvasView viewInstance = Instantiate(prefab);
            viewOwnerBindings = ViewOwnerBindingsInView(viewInstance) as IViewBindable;
            if (viewOwnerBindings == null)
            {
                Error.Log("ViewReference and ViewCallback attributes won't be configured as no bindings object was found in the canvas view. It should be placed in the root of the prefab.", Error.Severity.Log);
            }

            return viewInstance;
        }

        private CanvasViewOwnerBindings ViewOwnerBindingsInView(CanvasView view)
        {
            return ViewOwnerBindingsUnderObject(view.gameObject);
        }

        private CanvasViewOwnerBindings ViewOwnerBindingsUnderObject(GameObject gameObject)
        {
            // The canvas view owner bindings behaviour is expected to be in the root of the prefab.
            CanvasViewOwnerBindings viewOwnerBindings = null;
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                var child = gameObject.transform.GetChild(i);
                viewOwnerBindings = child.GetComponent<CanvasViewOwnerBindings>();
                if (viewOwnerBindings != null)
                {
                    break;
                }
            }

            return viewOwnerBindings;
        }
    }

    public partial class CanvasViewResource : ViewResource<CanvasView, CanvasWindow>
    {
#if UNITY_EDITOR
        public CanvasViewOwnerBindings ViewOwnerBindingsInPrefab(GameObject prefab = null)
        {
            if (prefab == null)
            {
                prefab = this.prefab?.gameObject;
                if (prefab == null)
                {
                    return null;
                }
            }

            return ViewOwnerBindingsUnderObject(prefab);
        }

        public override void OpenView()
        {
            AssetDatabase.OpenAsset(prefab);
        }

        public override void AddGraphablesToViewElements()
        {
            AddGraphablesToAllKnownViewElements();
        }

        public override IGraphableEmbedViewObject[] GraphableEmbedViewObjectsInView()
        {
            return prefab.GetComponentsInChildren<IGraphableEmbedViewObject>(true);
        }

        public override IGraphableTransitionViewObject[] GraphableTransitionViewObjectsInView()
        {
            return prefab.GetComponentsInChildren<IGraphableTransitionViewObject>(true);
        }

        public override bool ContainsAsset(string assetPath)
        {
            string prefabPath = AssetDatabase.GetAssetPath(prefab);
            return assetPath.Equals(prefabPath);
        }
#endif
    }

    // Graphable auto-add support.
    public partial class CanvasViewResource : ViewResource<CanvasView, CanvasWindow>
    {
#if UNITY_EDITOR
        private static readonly GraphableComponent[] GraphableComponents = new GraphableComponent[]
        {
            new GraphableComponent(typeof(UnityEngine.UI.Button), typeof(GraphableButtonComponent)),
            new GraphableComponent(typeof(UnityEngine.UI.Toggle), typeof(GraphableToggleComponent))
        };

        private void AddGraphablesToAllKnownViewElements()
        {
            foreach (GraphableComponent graphableComponentData in GraphableComponents)
            {
                System.Type componentType = graphableComponentData.componentType;
                System.Type graphableComponentType = graphableComponentData.graphableComponentType;

                Component[] graphablesInView = prefab.GetComponentsInChildren(componentType, true);
                foreach (Component graphableInView in graphablesInView)
                {
                    Component graphableComponent = graphableInView.GetComponent(graphableComponentType);
                    if (graphableComponent == null)
                    {
                        graphableInView.gameObject.AddComponent(graphableComponentType);
                    }
                }
            }

            AssetDatabase.SaveAssets();
        }

        private class GraphableComponent
        {
            public System.Type componentType;
            public System.Type graphableComponentType;

            public GraphableComponent(System.Type componentType, System.Type graphableComponentType)
            {
                this.componentType = componentType;
                this.graphableComponentType = graphableComponentType;
            }
        }
#endif
    }
}