﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph
{
    /// <summary>
    /// <para>
    /// The Elements Controller class is the base view controller type for working with Unity's UIElements workflow. It manages a view built with Unity's UIElements system and stores this view in a pair of Uxml/Uss files.
    /// </para>
    /// <para>
    /// The UIElements workflow and Elements Controller are currently for internal use only.
    /// </para>
    /// </summary>
    public partial class ElementsController : ViewController<ElementsController, ElementsView, ElementsWindow, ElementsViewResource, ElementsControllerTransition, ElementsControllerTransitionContext, ElementsControllerTransitionAnimatorProvider, IElementsControllerTransitionProgressProvider, ElementsControllerTransitionAnimationDriver, ElementsControllerTransitionProgressProvider, ElementsControllerInvokeTransitionData, ElementsGraph>
    {
        /// <summary>
        /// The transition identifier for the Elements Controller's Present transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier PresentTransition = new ViewControllerTransitionIdentifier("ElementsController.Present", ViewControllerTransitionIdentifier.TransitionType.Presentation, "Present");
        /// <summary>
        /// The transition identifier for the Elements Controller's Dismiss transition.
        /// </summary>
        public static readonly ViewControllerTransitionIdentifier DismissTransition = new ViewControllerTransitionIdentifier("ElementsController.Dismiss", ViewControllerTransitionIdentifier.TransitionType.Dismissal, "Dismiss To");

        sealed protected override ViewControllerTransitionIdentifier PresentTransitionIdentifier
        {
            get
            {
                return PresentTransition;
            }
        }

        sealed protected override ViewControllerTransitionIdentifier DismissTransitionIdentifier
        {
            get
            {
                return DismissTransition;
            }
        }
    }

    public partial class ElementsController : ViewController<ElementsController, ElementsView, ElementsWindow, ElementsViewResource, ElementsControllerTransition, ElementsControllerTransitionContext, ElementsControllerTransitionAnimatorProvider, IElementsControllerTransitionProgressProvider, ElementsControllerTransitionAnimationDriver, ElementsControllerTransitionProgressProvider, ElementsControllerInvokeTransitionData, ElementsGraph>
    {
        sealed protected override void BindActivationListenerToViewObjects(IGraphableTransitionViewObjectListener viewObjectActivationListener)
        {
            throw new System.NotImplementedException("ElementsController graphing is not currently supported.");
        }

        sealed protected override IGraphableEmbedViewObject[] FindGraphableEmbedViewObjectsInView()
        {
            throw new System.NotImplementedException("ElementsController graphing is not currently supported.");
        }
    }
}