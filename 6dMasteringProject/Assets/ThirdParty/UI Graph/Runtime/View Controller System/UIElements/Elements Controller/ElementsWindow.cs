﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEngine;

namespace Pelican7.UIGraph
{
    public class ElementsWindow : ElementsView
    {
        private ElementsControllerTransition transition;

        public ElementsWindow()
        {
            AddToClassList(FitToParentUssClass);
        }

        public ElementsController RootViewController { get; set; }

        private bool IsPerformingTransition
        {
            get
            {
                return (transition != null);
            }
        }

        public void Present(bool animated = false, System.Action completion = null)
        {
            if (CanPerformTransition(out Error error) == false)
            {
                error.LogWithPrefix("Cannot present window.");
                return;
            }

            // Add the root view to the window.
            var rootView = RootViewController.View;
            Add(rootView);
            rootView.FitToParent();

            // Transition to the root view controller.
            transition = new ElementsControllerTransition(ElementsController.PresentTransition, RootViewController, null, RootViewController, animated);
            transition.Perform(OnTransitionDidFinish: (transition, completed) =>
            {
                // Equivalent to "this.transition = null;" without capturing 'this' in lambda expression.
                ElementsControllerTransitionContext context = transition.Context;
                ElementsController owner = context.OwnerViewController;
                ElementsWindow ownerWindow = owner.View.Window;
                ownerWindow.transition = null;

                completion?.Invoke();
            });
        }

        public void Dismiss(bool animated = false, System.Action completion = null)
        {
            if (CanPerformTransition(out Error error) == false)
            {
                error.LogWithPrefix("Cannot dismiss window.");
                return;
            }

            // Transition the current view controller stack off-screen.
            Stack<ElementsController> viewControllerStack = RootViewController.PresentedViewControllerStack;
            ElementsController topViewController = viewControllerStack.Pop();
            transition = new ElementsControllerTransition(ElementsController.DismissTransition, null, topViewController, topViewController, animated, intermediaryViewControllers: viewControllerStack);
            transition.Perform(OnTransitionBegan: (transition) =>
            {
                // Hide all intermediary view controllers.
                var context = transition.Context;
                foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                {
                    intermediaryViewController.AsAppearable.BeginAppearanceTransition(false);
                    intermediaryViewController.View.visible = false;
                    intermediaryViewController.AsAppearable.EndAppearanceTransition();
                }
            },
            OnTransitionDidFinish: (transition, completed) =>
            {
                ElementsControllerTransitionContext context = transition.Context;
                ElementsController owner = context.OwnerViewController;
                ElementsWindow ownerWindow = owner.View.Window;

                // Destroy all view controllers in the window.
                context.FromViewController.Destroy();
                foreach (var intermediaryViewController in context.IntermediaryViewControllers)
                {
                    intermediaryViewController.Destroy();
                }

                // Destroy window.
                ownerWindow.RemoveFromHierarchy();

                completion?.Invoke();
            });
        }

        private bool CanPerformTransition(out Error error)
        {
            if (RootViewController == null)
            {
                error = new Error("The window's root view controller has not been set.");
                return false;
            }

            if (IsPerformingTransition)
            {
                error = new Error("The window is already transitioning its root view controller.", Error.Severity.Log);
                return false;
            }

            error = null;
            return true;
        }
    }
}