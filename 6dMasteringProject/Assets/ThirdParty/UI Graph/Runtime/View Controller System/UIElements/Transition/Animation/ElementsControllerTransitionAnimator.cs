﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph
{
    public abstract class ElementsControllerTransitionAnimator : ScriptableObject, IViewControllerTransitionAnimator<ElementsControllerTransitionContext>
    {
        public abstract float TransitionDuration(ElementsControllerTransitionContext context);
        public abstract ViewControllerTransitionTimeUpdateMode TransitionTimeUpdateMode(ElementsControllerTransitionContext context);
        public abstract void ConfigureTransitionAnimation(ElementsControllerTransitionContext context);
        public abstract void UpdateTransitionAnimation(ElementsControllerTransitionContext context, float progress01);
        public virtual void OnTransitionFinished(ElementsControllerTransitionContext context, bool completed) { }
    }
}