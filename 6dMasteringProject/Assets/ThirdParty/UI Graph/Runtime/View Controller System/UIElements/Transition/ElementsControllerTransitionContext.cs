﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph
{
    /// <summary>
    /// The Elements Controller transition context provides detailed information about a view controller transition, such as the view controllers involved.
    /// </summary>
    public class ElementsControllerTransitionContext : ViewControllerTransitionContext<ElementsController>
    {
        public ElementsControllerTransitionContext(ViewControllerTransitionIdentifier identifier, ElementsController toViewController, ElementsController fromViewController, ElementsController ownerViewController, bool animated, bool interactive, Stack<ElementsController> intermediaryViewControllers = null) : base(identifier, toViewController, fromViewController, ownerViewController, animated, interactive, intermediaryViewControllers) { }
    }
}