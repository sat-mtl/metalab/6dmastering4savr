﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph
{
    public class ElementsControllerTransitionProgressProvider : IElementsControllerTransitionProgressProvider, IViewControllerTransitionCompletionProgressProvider
    {
        private IViewControllerTransitionDrivable<ElementsControllerTransitionContext> drivable;
        private IVisualElementScheduledItem routine;
        private bool driverCancelled;

        void IViewControllerTransitionProgressProvider<ElementsControllerTransitionContext>.ProvideProgressToDrivable(IViewControllerTransitionDrivable<ElementsControllerTransitionContext> drivable)
        {
            this.drivable = drivable;
            Run();
        }

        void IViewControllerTransitionProgressProvider<ElementsControllerTransitionContext>.CancelProgressUpdatesToDrivable(IViewControllerTransitionDrivable<ElementsControllerTransitionContext> drivable)
        {
            routine.Pause();
            driverCancelled = true;
        }

        public float Duration
        {
            get
            {
                return drivable.Duration;
            }
        }

        public ViewControllerTransitionAnimationDriverDirection Direction
        {
            get
            {
                return drivable.Direction;
            }
        }

        public AnimationCurve ProgressCurve { get; set; }

        private ViewControllerTransitionTimeUpdateMode TimeUpdateMode
        {
            get
            {
                return drivable.TimeUpdateMode;
            }
        }

        private void Run()
        {
            ElementsControllerTransitionContext context = drivable.Context;
            ElementsView ownerView = context.OwnerViewController.View;

            float startProgress = drivable.Progress01;
            float completedProgress = (Direction.IsForwards()) ? 1f : 0f;
            float remainingProgress = completedProgress - startProgress;
            float time = startProgress * Duration;
            routine = ownerView.schedule.Execute((timerState) =>
            {
                float progress01 = time / Duration;

                // The progress curve is applied over the remaining progress only. This means that when the progress provider picks up an in-progress transition, such as to complete/cancel an interactive transition, the progress curve is applied from that point onwards.
                if (ProgressCurve != null)
                {
                    float curveProgress01 = Mathf.InverseLerp(startProgress, completedProgress, progress01);
                    progress01 = startProgress + (remainingProgress * ProgressCurve.Evaluate(curveProgress01));
                }

                drivable.Progress01 = progress01;

                float deltaTime = (TimeUpdateMode == ViewControllerTransitionTimeUpdateMode.ScaledTime) ? (timerState.deltaTime / 1000f) : Time.unscaledDeltaTime;
                time += (Direction.IsForwards()) ? deltaTime : -deltaTime;
            })
            .Every(16).Until(() =>
            {
                if (driverCancelled)
                {
                    return true;
                }

                bool complete = ((time > Duration) || (time < 0f));
                if (complete)
                {
                    drivable.Progress01 = completedProgress;
                    if (Direction.IsForwards())
                    {
                        drivable.CompleteTransition();
                    }
                    else
                    {
                        drivable.CancelTransition();
                    }
                }

                return complete;
            });
        }
    }
}