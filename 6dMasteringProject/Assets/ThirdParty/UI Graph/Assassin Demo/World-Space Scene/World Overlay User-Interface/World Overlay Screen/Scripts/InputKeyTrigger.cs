// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.EventSystems;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class InputKeyTrigger : MonoBehaviour
    {
        public KeyCode keyCode;
        public GameObject targetObject;

        private void Update()
        {
            if (Input.GetKeyDown(keyCode))
            {
                Trigger();
            }
        }

        private void Reset()
        {
            targetObject = gameObject;
        }

        private void Trigger()
        {
            var pointer = new PointerEventData(EventSystem.current);
            ExecuteEvents.Execute(targetObject, pointer, ExecuteEvents.pointerClickHandler);
        }
    }
}