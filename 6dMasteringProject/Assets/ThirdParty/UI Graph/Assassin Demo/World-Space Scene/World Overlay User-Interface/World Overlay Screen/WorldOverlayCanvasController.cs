// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class WorldOverlayCanvasController : CanvasController
    {
        protected override void PrepareForGraphTransition(GraphTransition<CanvasController> transition)
        {
            base.PrepareForGraphTransition(transition);

            // If we are transitioning to the loading canvas controller, configure it with the scene name to load.
            if (transition.ToViewController is LoadingCanvasController loadingCanvasController)
            {
                // We use the user identifier (configured in the graph) as the scene name.
                loadingCanvasController.sceneNameToLoad = transition.UserIdentifier;
            }
        }
    }
}