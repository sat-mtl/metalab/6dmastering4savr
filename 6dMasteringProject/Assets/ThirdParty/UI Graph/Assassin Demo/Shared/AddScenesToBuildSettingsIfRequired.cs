﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Pelican7.UIGraph.Demos.Assassin
{
    [ExecuteInEditMode]
    public class AddScenesToBuildSettingsIfRequired : MonoBehaviour
    {
#if UNITY_EDITOR
        public SceneAsset[] scenes;

        private void Awake()
        {
            if (Application.isPlaying == false)
            {
                // Determine which scenes are not in the build scenes.
                var scenesToAdd = new List<SceneAsset>();
                EditorBuildSettingsScene[] buildScenes = EditorBuildSettings.scenes;
                foreach (SceneAsset scene in scenes)
                {
                    string scenePath = AssetDatabase.GetAssetPath(scene);
                    bool isInBuildScenes = buildScenes.Any(s => s.path.Equals(scenePath));
                    if (isInBuildScenes == false)
                    {
                        scenesToAdd.Add(scene);
                    }
                }

                if (scenesToAdd.Count > 0)
                {
                    // Add missing required scenes to the build.
                    var newBuildScenes = new List<EditorBuildSettingsScene>(buildScenes);
                    foreach (var sceneToAdd in scenesToAdd)
                    {
                        string scenePath = AssetDatabase.GetAssetPath(sceneToAdd);
                        var buildScene = new EditorBuildSettingsScene(scenePath, true);
                        newBuildScenes.Add(buildScene);
                    }

                    EditorBuildSettings.scenes = newBuildScenes.ToArray();
                }
            }
        }
#endif
    }
}