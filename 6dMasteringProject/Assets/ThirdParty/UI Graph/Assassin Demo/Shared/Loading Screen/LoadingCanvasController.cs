// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class LoadingCanvasController : SceneTransitionCanvasController
    {
        // Perform any custom loading logic for the newly loaded scene.
        protected override IEnumerator PrepareLoadedScene(Scene scene)
        {
            // Wait to simulate loading.
            yield return new WaitForSeconds(2f);
        }
    }
}