// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Demos.Assassin
{
    public partial class OptionsListCanvasController : CanvasController
    {
        [ViewReference]
        public ListView listView;
        public OptionsListDataModel dataModel;
        public OptionListItem itemTemplate;

        private OptionListItem selectedOptionItem;

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();

            listView.DataSource = this;
            listView.OnListViewSelectedItemAtIndex.AddListener(OnListViewSelectedItemAtIndex);
            listView.ReloadData();
        }

        private void OnListViewSelectedItemAtIndex(ListView list, ListViewItem listViewItem, int index)
        {
            OptionDataItem dataItem = dataModel[index];
            dataItem.value = !dataItem.value;

            OptionListItem item = (OptionListItem)listViewItem;
            item.ConfigureWithItem(dataItem);

            // Deselect the current option, if necessary.
            selectedOptionItem?.SetSelected(false);

            // Select the new option.
            selectedOptionItem = item;
            selectedOptionItem.SetSelected(true);
        }
    }

    public partial class OptionsListCanvasController : CanvasController, ListView.IListViewDataSource
    {
        int ListView.IListViewDataSource.NumberOfItemsForListView(ListView list)
        {
            return dataModel.ItemCount;
        }

        ListViewItem ListView.IListViewDataSource.ListViewItemForIndex(ListView list, int index)
        {
            OptionListItem item = Instantiate(itemTemplate);

            OptionDataItem dataItem = dataModel[index];
            item.ConfigureWithItem(dataItem);

            return item;
        }
    }
}