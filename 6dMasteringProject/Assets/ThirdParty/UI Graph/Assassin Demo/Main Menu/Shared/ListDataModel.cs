﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph.Demos.Assassin
{
    public class ListDataModel<T> : ScriptableObject
        where T : class
    {
        public T[] items;

        public T this[int i]
        {
            get
            {
                return items[i];
            }
        }

        public int ItemCount
        {
            get
            {
                return items.Length;
            }
        }
    }
}