﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    public class ElementsEditorController : ElementsController
    {
        protected bool IsAsset
        {
            get
            {
                return (IsInstance == false);
            }
        }

        protected bool IsInstance
        {
            get
            {
                return string.IsNullOrEmpty(AssetDatabase.GetAssetPath(this));
            }
        }

        sealed public override void Destroy()
        {
            if (IsAsset)
            {
                Error.Log("Cannot destroy view controller asset. Only instances should be destroyed this way.");
                return;
            }

            DestroyImmediate(this, false);
        }
    }
}