﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(ElementsViewOwnerBindingsEditorData))]
    public class ElementsViewOwnerBindingsEditorDataPropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement();

            Label headerLabel = new Label("View References");
            headerLabel.AddToClassList(VisualElementEditorStyles.HeaderLabelStyleClass);
            container.Add(headerLabel);

            AddArrayPropertySectionToContainer(property, "viewReferenceDatas", container);

            return container;
        }

        private void AddArrayPropertySectionToContainer(SerializedProperty property, string arrayPropertyPath, VisualElement container)
        {
            VisualElement section = new VisualElement();
            section.AddToClassList(VisualElementEditorStyles.SectionStyleClass);
            var arrayProperty = property.FindPropertyRelative(arrayPropertyPath);

            if (arrayProperty.isArray)
            {
                if (arrayProperty.arraySize > 0)
                {
                    for (int i = 0; i < arrayProperty.arraySize; i++)
                    {
                        var prop = arrayProperty.GetArrayElementAtIndex(i);
                        section.Add(new PropertyField(prop));
                    }
                }
                else
                {
                    var text = string.Format("No {0} Declared In Owner", arrayProperty.displayName);
                    section.Add(new Label(text));
                }
            }

            container.Add(section);
        }
    }
}