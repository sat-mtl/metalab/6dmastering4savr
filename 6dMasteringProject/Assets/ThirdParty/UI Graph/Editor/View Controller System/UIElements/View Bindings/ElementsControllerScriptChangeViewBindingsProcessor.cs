﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Editor
{
    public class ElementsControllerScriptChangeViewBindingsProcessor : ViewControllerScriptChangeViewBindingsProcessor<ElementsController, ElementsView, ElementsWindow, ElementsViewResource, ElementsControllerTransition, ElementsControllerTransitionContext, ElementsControllerTransitionAnimatorProvider, IElementsControllerTransitionProgressProvider, ElementsControllerTransitionAnimationDriver, ElementsControllerTransitionProgressProvider, ElementsControllerInvokeTransitionData, ElementsGraph>
    {
        protected override void UpdateViewControllerBindings(ElementsController viewController)
        {
            var viewResource = viewController.ViewResource;
            var viewResourceBindingsUpdater = new ElementsViewResourceOwnerBindingsUpdater(viewResource);
            viewResourceBindingsUpdater.UpdateBindingsWithOwner(viewController);
        }
    }
}