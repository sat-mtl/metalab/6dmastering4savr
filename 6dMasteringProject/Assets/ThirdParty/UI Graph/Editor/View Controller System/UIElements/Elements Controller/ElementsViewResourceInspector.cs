﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomEditor(typeof(ElementsViewResource), true)]
    public class ElementsViewResourceInspector : InspectorBase
    {
        public override VisualElement CreateInspectorGUI()
        {
            var container = CreateBaseInspector();

            SerializedProperty uxml = serializedObject.FindProperty("uxml");
            container.Add(new PropertyField(uxml));

            SerializedProperty styleSheet = serializedObject.FindProperty("styleSheet");
            container.Add(new PropertyField(styleSheet));

            Label viewBindingsHeaderLabel = new Label("View Bindings");
            viewBindingsHeaderLabel.AddToClassList(VisualElementEditorStyles.HeaderLabelStyleClass);
            container.Add(viewBindingsHeaderLabel);

            SerializedProperty ownerBindingsEditorData = serializedObject.FindProperty("ownerBindingsEditorData");
            PropertyField ownerBindingsPropertyField = new PropertyField(ownerBindingsEditorData);
            ownerBindingsPropertyField.SetEnabled(false);
            container.Add(ownerBindingsPropertyField);

            return container;
        }
    }
}