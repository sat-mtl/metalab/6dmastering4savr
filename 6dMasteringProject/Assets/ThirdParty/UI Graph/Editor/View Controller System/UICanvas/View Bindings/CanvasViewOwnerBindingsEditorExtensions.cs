﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Editor
{
    public static class CanvasViewOwnerBindingsEditorExtensions
    {
        public static bool TryGetViewReferenceWithIdentifier(this CanvasViewOwnerBindings canvasViewOwnerBindings, string identifier, out CanvasViewReference viewReference)
        {
            viewReference = canvasViewOwnerBindings.ReferenceWithIdentifier(identifier);
            return (viewReference != null);
        }

        public static bool TryGetViewCallbackWithIdentifier(this CanvasViewOwnerBindings canvasViewOwnerBindings, string identifier, out CanvasViewCallback viewCallback)
        {
            viewCallback = canvasViewOwnerBindings.CallbackWithIdentifier(identifier);
            return (viewCallback != null);
        }
    }
}