﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(CanvasViewCallback))]
    public class CanvasViewCallbackPropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement()
            {
                style =
                {
                    flexDirection = FlexDirection.Row
                }
            };

            var viewCallback = property.objectReferenceValue as CanvasViewCallback;
            if (viewCallback != null)
            {
                Label methodSignatureLabel = new Label(viewCallback.DisplayName)
                {
                    style =
                    {
                        flexGrow = 1
                    }
                };
                container.Add(methodSignatureLabel);

                var showInSceneButton = new Button(() =>
                {
                    EditorWindowUtility.ShowSceneHierarchyEditorWindow();
                    EditorGUIUtility.PingObject(property.objectReferenceValue);
                })
                {
                    text = "Show"
                };
                container.Add(showInSceneButton);
            }

            return container;
        }
    }
}