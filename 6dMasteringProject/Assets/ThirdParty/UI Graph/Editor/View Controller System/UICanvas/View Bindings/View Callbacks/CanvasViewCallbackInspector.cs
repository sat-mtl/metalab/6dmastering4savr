﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    [CustomEditor(typeof(CanvasViewCallback), true)]
    public class CanvasViewCallbackInspector : InspectorBase
    {
        public override void OnInspectorGUI()
        {
            var viewCallback = target as CanvasViewCallback;
            EditorGUILayout.LabelField("Method Signature", viewCallback.DisplayName);

            serializedObject.ApplyModifiedProperties();
        }
    }
}