﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomEditor(typeof(CanvasViewResource), true)]
    public class CanvasViewResourceInspector : InspectorBase
    {
        public override VisualElement CreateInspectorGUI()
        {
            var container = CreateBaseInspector();

            SerializedProperty prefab = serializedObject.FindProperty("prefab");
            container.Add(new PropertyField(prefab));

            Label viewBindingsHeaderLabel = new Label("View Bindings");
            viewBindingsHeaderLabel.AddToClassList(VisualElementEditorStyles.HeaderLabelStyleClass);
            container.Add(viewBindingsHeaderLabel);

            CanvasViewResource viewResource = target as CanvasViewResource;
            CanvasViewOwnerBindings viewOwnerBindings = viewResource.ViewOwnerBindingsInPrefab();
            if (viewOwnerBindings != null)
            {
                InspectorElement viewOwnerBindingsInspector = new InspectorElement(viewOwnerBindings);
                viewOwnerBindingsInspector.SetEnabled(false);
                container.Add(viewOwnerBindingsInspector);
            }
            else
            {
                container.Add(new Label("Unable to load view bindings."));
            }

            return container;
        }
    }
}