﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    public class StackCanvasControllerInstanceCreationDialog : ViewControllerInstanceCreationDialog<CanvasController, CanvasViewResource, CanvasControllerTransitionAnimatorProvider>
    {
        [MenuItem("Assets/Create/UI Graph/UICanvas/Stack Canvas Controller", false, MenuItemPriority.Group0)]
        private static void Present()
        {
            string title = "New Stack Canvas Controller";
            string defaultName = "NewStackCanvasController";
            var template = UIGraphProjectSettings.Instance.createStackCanvasControllerInstanceTemplate;

            var createDialog = new StackCanvasControllerInstanceCreationDialog();
            createDialog.Present(title, defaultName, template);
        }
    }
}