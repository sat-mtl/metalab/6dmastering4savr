﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class CanvasWindowCreator
    {
        public static GameObject CreateCanvasWindowGameObject()
        {
            UIGraphProjectSettings settings = UIGraphProjectSettings.Instance;
            CanvasWindow canvasWindowTemplate = settings.createCanvasWindowTemplate;
            return (GameObject)PrefabUtility.InstantiatePrefab(canvasWindowTemplate.gameObject);
        }

        [MenuItem("GameObject/UI/Canvas Window (UI Graph)", false, 10)]
        private static void CreateCanvasWindowGameObject(MenuCommand menuCommand)
        {
            GameObject canvasWindowInstance = CreateCanvasWindowGameObject();
            GameObjectUtility.SetParentAndAlign(canvasWindowInstance, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(canvasWindowInstance, "Create Canvas Window");
            Selection.activeObject = canvasWindowInstance;
        }
    }
}