﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public class ViewControllerInstanceCreationDialog<TViewController, TViewResource, TTransitionAnimatorProvider>
        where TViewController : ScriptableObject
        where TViewResource : ScriptableObject
        where TTransitionAnimatorProvider : Object
    {
        protected void Present(string title, string defaultName, CreateViewControllerInstanceTemplate<TViewController, TViewResource, TTransitionAnimatorProvider> template)
        {
            string initialPath = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (string.IsNullOrEmpty(initialPath))
            {
                initialPath = "Assets";
            }

            string assetPath = EditorUtility.SaveFilePanelInProject(title, defaultName, "asset", "Choose a name and directory.", initialPath);
            if (string.IsNullOrEmpty(assetPath) == false)
            {
                FileNamesForPath(assetPath, out string directory, out string assetName, out string viewName);
                template.Create(directory, assetName, viewName);
            }
        }

        private void FileNamesForPath(string path, out string directory, out string assetName, out string viewName)
        {
            directory = Path.GetDirectoryName(path);
            assetName = Path.GetFileNameWithoutExtension(path);
            viewName = assetName + " - View";
        }
    }
}