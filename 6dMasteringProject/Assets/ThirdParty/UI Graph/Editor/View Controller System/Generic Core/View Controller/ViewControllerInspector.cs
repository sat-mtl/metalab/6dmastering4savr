﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public abstract class ViewControllerInspector : InspectorBase
    {
        public override VisualElement CreateInspectorGUI()
        {
            var container = CreateBaseInspector();

            // Can't find a way to draw a disabled but selectable property field in UIElements yet (2019.1).
            container.Add(new IMGUIContainer(() =>
            {
                EditorGUI.BeginDisabledGroup(true);
                SerializedProperty script = serializedObject.FindProperty("m_Script");
                EditorGUILayout.PropertyField(script);
                EditorGUI.EndDisabledGroup();
            }));

            AddViewControllerPropertiesToContainer(container);

            Label viewResourceHeaderLabel = new Label("View Resource");
            viewResourceHeaderLabel.AddToClassList(VisualElementEditorStyles.HeaderLabelStyleClass);
            container.Add(viewResourceHeaderLabel);

            SerializedProperty viewResource = serializedObject.FindProperty("viewResource");
            container.Add(new PropertyField(viewResource));

            InspectorElement viewResourceInspector = new InspectorElement(viewResource.objectReferenceValue);
            viewResourceInspector.SetEnabled(false);
            container.Add(viewResourceInspector);

            return container;
        }

        private void AddViewControllerPropertiesToContainer(VisualElement container)
        {
            HashSet<string> excludedPropertyNames = GenerateExcludedPropertyNames();
            SerializedProperty property = serializedObject.GetIterator();
            property.NextVisible(true);
            do
            {
                var propertyName = property.propertyPath;
                if (excludedPropertyNames.Contains(propertyName) == false)
                {
                    var propertyField = new PropertyField(property);
                    container.Add(propertyField);
                }
            }
            while (property.NextVisible(false));
        }

        private HashSet<string> GenerateExcludedPropertyNames()
        {
            var excludedPropertyNames = new HashSet<string>
            {
                "m_Script",
                "viewResource"
            };

            var viewReferenceFields = ViewReferenceAttributeCollector.CollectViewReferenceAttributedFields(target);
            foreach (var viewReferenceField in viewReferenceFields)
            {
                string fieldName = viewReferenceField.Name;
                excludedPropertyNames.Add(fieldName);
            }

            return excludedPropertyNames;
        }
    }
}