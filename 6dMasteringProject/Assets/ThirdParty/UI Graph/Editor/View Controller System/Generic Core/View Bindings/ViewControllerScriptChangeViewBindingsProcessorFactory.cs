﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.Callbacks;

namespace Pelican7.UIGraph.Editor
{
    public class ViewControllerScriptChangeViewBindingsProcessorFactory
    {
        [DidReloadScripts]
        private static void DidReloadScripts()
        {
            ProcessUpdateQueue();
        }

        private static void ProcessUpdateQueue()
        {
            AssetDatabase.StartAssetEditing();
            var processorQueue = ViewControllerScriptChangeViewBindingsProcessorQueue.Instance;
            while ((processorQueue != null) && (processorQueue.Count > 0))
            {
                var viewControllerScriptPath = processorQueue.DequeueScriptForProcessing();
                var processor = ProcessorForViewControllerScript(viewControllerScriptPath);
                if (processor != null)
                {
                    processor.ProcessViewControllerScript(viewControllerScriptPath);
                }
                else
                {
                    Error.Log(string.Format("No processor found for view controller {0}.", viewControllerScriptPath));
                }
            }
            AssetDatabase.StopAssetEditing();
        }

        private static IViewControllerScriptChangeViewBindingsProcessor ProcessorForViewControllerScript(string viewControllerScriptPath)
        {
            var script = AssetDatabase.LoadAssetAtPath<MonoScript>(viewControllerScriptPath);
            if (script == null)
            {
                return null;
            }

            var scriptType = script.GetClass();
            if (scriptType == null)
            {
                return null;
            }

            if (scriptType.IsSubclassOf(typeof(CanvasController)))
            {
                return new CanvasControllerScriptChangeViewBindingsProcessor();
            }
            else if (scriptType.IsSubclassOf(typeof(ElementsController)))
            {
                return new ElementsControllerScriptChangeViewBindingsProcessor();
            }

            return null;
        }
    }
}