﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    internal class CreateCanvasControllerTransitionSequenceAnimation
    {
        private const string ScriptTemplateGuid = "1487f41da6086254f8a16a13739bd36c";
        private const string ClassNameScriptTemplateSymbol = "___CLASSNAME___";

        [MenuItem("Assets/Create/UI Graph/UICanvas/Transition Sequence Animator/Custom Sequence Animation (Script)", false, MenuItemPriority.Group0)]
        private static void CreateNewCanvasControllerTransitionSequenceAnimation()
        {
            string title = "New Transition Sequence Animation";
            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (string.IsNullOrEmpty(path))
            {
                path = "Assets";
            }

            string assetPath = EditorUtility.SaveFilePanelInProject(title, "NewTransitionAnimation", "cs", "Choose a name and directory.", path);
            if (string.IsNullOrEmpty(assetPath) == false)
            {
                string scriptDirectory = Path.GetDirectoryName(assetPath);
                string scriptName = Path.GetFileNameWithoutExtension(assetPath);
                scriptName = StripIllegalCharactersFromClassName(scriptName);

                // Create script.
                string scriptPath = Path.ChangeExtension(Path.Combine(scriptDirectory, scriptName), "cs");
                string scriptContents = GenerateScript(scriptName);
                File.WriteAllText(scriptPath, scriptContents);

                // Import script.
                AssetDatabase.ImportAsset(scriptPath);
            }
        }

        private static string StripIllegalCharactersFromClassName(string className)
        {
            // For now, only remove spaces.
            return className.Replace(" ", string.Empty);
        }

        private static string GenerateScript(string scriptName)
        {
            TextAsset scriptTemplate = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<TextAsset>(ScriptTemplateGuid);
            string scriptContents = scriptTemplate.text;
            scriptContents = scriptContents.Replace(ClassNameScriptTemplateSymbol, scriptName);
            return scriptContents;
        }
    }
}