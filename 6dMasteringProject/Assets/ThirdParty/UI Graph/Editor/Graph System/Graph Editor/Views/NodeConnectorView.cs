﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class NodeConnectorView : VisualElement
    {
        private const string ConnectableIconAssetGuid = "800872f4fb8a59848a0264b9a25bc714";
        private const string ConnectedIconAssetGuid = "ae46b229b8b119a4bbb9b51c84f4681e";
        private const int ConnectorMouseButton = 0;
        private const string UssClass = "node-connector-view";
        private const string IconUssClass = "node-connector-view-icon";
        private static readonly Color DefaultTintColor = (EditorGUIUtility.isProSkin) ? new Color(0.851f, 0.851f, 0.851f, 1) : new Color(0.3921f, 0.3921f, 0.3921f, 1f);
        private static readonly Color HoveredTintColor = (EditorGUIUtility.isProSkin) ? new Color(1f, 1f, 1f, 1) : new Color(0f, 0f, 0f, 1f);
        private static readonly Color PressedTintColor = (EditorGUIUtility.isProSkin) ? new Color(1f, 1f, 1f, 1) : new Color(0f, 0f, 0f, 1f);
        private static readonly Color ConnectingWireColor = (EditorGUIUtility.isProSkin) ? new Color(1f, 1f, 1f, 1) : new Color(0.3921f, 0.3921f, 0.3921f, 1f);

        private readonly VisualElement icon;
        private bool allowNewConnections;
        private HighlightState highlight;
        private IMGUIContainer connectingWire;
        private Vector2 mousePosition;

        public NodeConnectorView()
        {
            icon = new VisualElement()
            {
                pickingMode = PickingMode.Ignore,
                style =
                {
                    flexGrow = 1,
                    unityBackgroundScaleMode = ScaleMode.ScaleToFit
                }
            };
            Add(icon);

            ConfigureUssClasses();
            UpdateIcon();
            UpdateIconTintColor();
        }

        public MakeConnectionEvent OnMakingConnection = new MakeConnectionEvent();

        public enum MakeConnectionState
        {
            Began,
            Moved,
            Ended
        }

        private enum HighlightState
        {
            None,
            Hovered,
            Pressed
        }

        public bool AllowNewConnections
        {
            get
            {
                return allowNewConnections;
            }

            set
            {
                allowNewConnections = value;
                UpdateIcon();
            }
        }

        private HighlightState Highlight
        {
            get
            {
                return highlight;
            }

            set
            {
                highlight = value;
                UpdateIconTintColor();
            }
        }

        private IMGUIContainer ConnectingWire
        {
            get
            {
                if (connectingWire == null)
                {
                    CreateConnectingWire();
                }

                return connectingWire;
            }
        }

        protected override void ExecuteDefaultActionAtTarget(EventBase evt)
        {
            base.ExecuteDefaultActionAtTarget(evt);

            if (AllowNewConnections == false)
            {
                return;
            }

            if(evt.eventTypeId == MouseDownEvent.TypeId())
            {
                HandleMouseDownEvent(evt as MouseDownEvent);
            }
            else if (evt.eventTypeId == MouseMoveEvent.TypeId())
            {
                HandleMouseMoveEvent(evt as MouseMoveEvent);
            }
            else if (evt.eventTypeId == MouseUpEvent.TypeId())
            {
                HandleMouseUpEvent(evt as MouseUpEvent);
            }
            else if (evt.eventTypeId == MouseOverEvent.TypeId())
            {
                HandleMouseOverEvent(evt as MouseOverEvent);
            }
            else if (evt.eventTypeId == MouseOutEvent.TypeId())
            {
                HandleMouseOutEvent(evt as MouseOutEvent);
            }
        }

        private void ConfigureUssClasses()
        {
            AddToClassList(UssClass);
            icon.AddToClassList(IconUssClass);
        }

        private void HandleMouseDownEvent(MouseDownEvent evt)
        {
            if (evt.button != ConnectorMouseButton)
            {
                return;
            }

            this.CaptureMouse();
            evt.StopPropagation();
            Highlight = HighlightState.Pressed;
            UpdateConnectingWire(evt.mousePosition);
            OnMakingConnection.Invoke(this, MakeConnectionState.Began, mousePosition);
        }

        private void HandleMouseMoveEvent(MouseMoveEvent evt)
        {
            if ((this.HasMouseCapture() == false))
            {
                return;
            }

            UpdateConnectingWire(evt.mousePosition);
            OnMakingConnection.Invoke(this, MakeConnectionState.Moved, mousePosition);
        }

        private void HandleMouseUpEvent(MouseUpEvent evt)
        {
            if ((evt.button != ConnectorMouseButton) || (this.HasMouseCapture() == false))
            {
                return;
            }

            this.ReleaseMouse();
            evt.StopPropagation();
            if (ContainsPoint(evt.localMousePosition))
            {
                Highlight = HighlightState.Hovered;
            }
            else
            {
                Highlight = HighlightState.None;
            }

            DestroyConnectingWire();
            OnMakingConnection.Invoke(this, MakeConnectionState.Ended, mousePosition);
        }

        private void HandleMouseOverEvent(MouseOverEvent evt)
        {
            if (Highlight != HighlightState.Pressed)
            {
                Highlight = HighlightState.Hovered;
            }
        }

        private void HandleMouseOutEvent(MouseOutEvent evt)
        {
            if (Highlight != HighlightState.Pressed)
            {
                Highlight = HighlightState.None;
            }
        }

        private void UpdateIcon()
        {
            string iconAssetGuid = (AllowNewConnections) ? ConnectableIconAssetGuid : ConnectedIconAssetGuid;
            icon.style.backgroundImage = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<Texture2D>(iconAssetGuid);
        }

        private void UpdateIconTintColor()
        {
            Color color = DefaultTintColor;
            if (highlight == HighlightState.Hovered)
            {
                color = HoveredTintColor;
            }
            else if (highlight == HighlightState.Pressed)
            {
                color = PressedTintColor;
            }

            icon.style.unityBackgroundImageTintColor = color;
        }

        private void UpdateConnectingWire(Vector2 mousePosition)
        {
            this.mousePosition = mousePosition;
            ConnectingWire.MarkDirtyRepaint();
        }

        private void CreateConnectingWire()
        {
            connectingWire = new IMGUIContainer(() =>
            {
                if (this.HasMouseCapture())
                {
                    Vector2 startPosition = connectingWire.WorldToLocal(worldBound.center);
                    Vector2 endPosition = connectingWire.WorldToLocal(mousePosition);
                    Handles.DrawBezier(startPosition, endPosition, startPosition, endPosition, ConnectingWireColor, null, 4f);
                }
            })
            {
                pickingMode = PickingMode.Ignore,
                style =
                {
                    position = Position.Absolute,
                    left = 0,
                    top = 0,
                    right = 0,
                    bottom = 0
                }
            };

            panel.visualTree.Add(connectingWire);
        }

        private void DestroyConnectingWire()
        {
            connectingWire.RemoveFromHierarchy();
            connectingWire = null;
        }

        public new class UxmlFactory : UxmlFactory<NodeConnectorView> { }
        public class MakeConnectionEvent : UnityEvent<NodeConnectorView, MakeConnectionState, Vector2> { }
    }
}