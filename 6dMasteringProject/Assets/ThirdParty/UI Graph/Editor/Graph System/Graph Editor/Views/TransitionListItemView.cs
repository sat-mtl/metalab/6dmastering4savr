﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class TransitionListItemView : VisualElement
    {
        private const string ArrowAssetGuid = "351ccdfba77c5964bb76ba5f9d30e234";

        public Label transitionIdentifierLabel;
        public Label destinationNameLabel;

        public System.Action OnSelected;

        public TransitionListItemView()
        {
            AddToClassList("TransitionListItemView");

            transitionIdentifierLabel = new Label()
            {
                pickingMode = PickingMode.Ignore
            };
            Add(transitionIdentifierLabel);

            VisualElement arrow = new VisualElement()
            {
                pickingMode = PickingMode.Ignore,
                style =
                {
                    width = 20,
                    backgroundImage = new StyleBackground(AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<Texture2D>(ArrowAssetGuid))
                }
            };
            Add(arrow);

            destinationNameLabel = new Label()
            {
                pickingMode = PickingMode.Ignore
            };
            Add(destinationNameLabel);
        }

        protected override void ExecuteDefaultActionAtTarget(EventBase evt)
        {
            base.ExecuteDefaultActionAtTarget(evt);

            if (evt.eventTypeId == MouseUpEvent.TypeId())
            {
                OnSelected?.Invoke();
            }
        }
    }
}