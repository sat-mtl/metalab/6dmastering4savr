﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class DragManipulator : MouseManipulator
    {
        private Vector2 startPosition;
        private bool active;

        public DragManipulator(MouseButton button)
        {
            activators.Add(new ManipulatorActivationFilter
            {
                button = button
            });
            active = false;
        }

        public System.Action<Vector2, DragState> OnDraggedToLocalPosition;

        public enum DragState
        {
            Began,
            Moved,
            Ended
        }

        protected override void RegisterCallbacksOnTarget()
        {
            target.RegisterCallback<MouseDownEvent>(OnMouseDown);
            target.RegisterCallback<MouseMoveEvent>(OnMouseMove);
            target.RegisterCallback<MouseUpEvent>(OnMouseUp);
        }

        protected override void UnregisterCallbacksFromTarget()
        {
            target.UnregisterCallback<MouseDownEvent>(OnMouseDown);
            target.UnregisterCallback<MouseMoveEvent>(OnMouseMove);
            target.UnregisterCallback<MouseUpEvent>(OnMouseUp);
        }

        protected void OnMouseDown(MouseDownEvent e)
        {
            if (active)
            {
                active = false;
                target.ReleaseMouse();
                e.StopImmediatePropagation();
                return;
            }

            if (CanStartManipulation(e))
            {
                startPosition = e.localMousePosition;

                active = true;
                target.CaptureMouse();
                e.StopPropagation();

                DragTarget(Vector2.zero, DragState.Began);
            }
        }

        protected void OnMouseMove(MouseMoveEvent e)
        {
            if ((active == false) || (target.HasMouseCapture() == false))
            {
                return;
            }

            e.StopPropagation();

            Vector2 movement = e.localMousePosition - startPosition;
            DragTarget(movement, DragState.Moved);
        }

        protected void OnMouseUp(MouseUpEvent e)
        {
            if ((active == false) || (target.HasMouseCapture() == false) || (CanStopManipulation(e) == false))
            {
                return;
            }

            active = false;
            target.ReleaseMouse();
            e.StopPropagation();

            Vector2 movement = e.localMousePosition - startPosition;
            DragTarget(movement, DragState.Ended);
        }

        private void DragTarget(Vector2 movement, DragState state)
        {
            Vector2 position = (Vector2)target.transform.position + movement;
            target.transform.position = position;

            OnDraggedToLocalPosition?.Invoke(position, state);
        }
    }
}