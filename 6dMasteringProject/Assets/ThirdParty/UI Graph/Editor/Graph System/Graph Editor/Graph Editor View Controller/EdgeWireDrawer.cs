// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class EdgeWireDrawer : IMGUIContainer
    {
        private const float TangentOffsetX = 12;
        private const float LineWeight = 4f;

        public EdgeWireDrawer()
        {
            pickingMode = PickingMode.Ignore;
            style.position = Position.Absolute;
            style.left = 0f;
            style.top = 0f;
            style.right = 0f;
            style.bottom = 0f;
        }

        public void DrawEdgeWire(EdgeData edge, Vector2 sourcePosition, Vector2 destinationPosition, float scale)
        {
            bool isDismissalEdge = (edge.type == EdgeData.EdgeType.Dismissal);
            float tangentOffset = TangentOffsetX * (isDismissalEdge ? -1 : 1) * scale;
            Vector2 sourceTangent = new Vector2(sourcePosition.x + tangentOffset, sourcePosition.y);
            Vector2 destinationTangent = new Vector2(destinationPosition.x - tangentOffset, destinationPosition.y);

            float lineWeight = LineWeight * scale;
            Color lineColor = LineColorForEdge(edge);
            Handles.DrawBezier(sourcePosition, destinationPosition, sourceTangent, destinationTangent, lineColor, null, lineWeight);
        }

        private Color LineColorForEdge(EdgeData edge)
        {
            Color lineColor;
            switch (edge.type)
            {
                case EdgeData.EdgeType.Dismissal:
                {
                    lineColor = ColorUtility.GraphUpstreamColorTransparent;
                    break;
                }

                case EdgeData.EdgeType.Embed:
                {
                    lineColor = ColorUtility.GraphEmbedColorTransparent;
                    break;
                }

                default:
                {
                    lineColor = ColorUtility.GraphDownstreamColorTransparent;
                    break;
                }
            }

            return lineColor;
        }
    }
}