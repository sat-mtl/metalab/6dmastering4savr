// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public abstract class NodeViewController : ElementsEditorController
    {
        private const string DarkStyleSheetAssetGuid = "eb44b9ab0133641438e3139578e29033";
        private const string LightStyleSheetAssetGuid = "dfefe4bdcbb1df146ac1803aed9c8b76";

        protected NodeData node;
        [ViewReference] protected VisualElement originArrow;
        [ViewReference] protected Foldout nodeSettingsVisibilityFoldout;
        [ViewReference] protected VisualElement nodeSettingsContainer;

        public void InitializeWithNode(NodeData node)
        {
            this.node = node;
        }

        public NodeData Node
        {
            get
            {
                return node;
            }
        }

        public abstract void ReloadData();
        public abstract Vector2 SourceWirePositionForEdge(EdgeData edge);
        public abstract Vector2 DestinationWirePositionForEdge(EdgeData edge);

        public void SetOriginArrowVisible(bool visible)
        {
            originArrow.EnableInClassList("hidden", (visible == false));
        }

        protected override void ViewDidLoad()
        {
            base.ViewDidLoad();

            View.transform.position = node.Position;
            if (node.HasValidResource() == false)
            {
                CreateMissingResourceView();
                return;
            }

            ConfigureSkinSpecificStyleSheet();
            ConfigureDragManipulator();
            ConfigureContextualMenuManipulator();
            ConfigureOriginArrow();
            ConfigureVisibilityFoldout("nodeSettingsVisible", nodeSettingsVisibilityFoldout, nodeSettingsContainer);
        }

        protected abstract void RefreshPorts(DropdownMenuAction action);

        protected void ConfigureVisibilityFoldout(string propertyPath, Foldout foldout, VisualElement container)
        {
            SerializedObject serializedNode = new SerializedObject(node);
            SerializedProperty visibilityProperty = serializedNode.FindProperty(propertyPath);

            bool visible = visibilityProperty.boolValue;
            foldout.value = visible;
            SetContainerVisibility(container, visible);

            foldout.RegisterValueChangedCallback((e) =>
            {
                e.StopImmediatePropagation();

                bool isOpen = e.newValue;
                serializedNode.Update();
                visibilityProperty.boolValue = isOpen;
                serializedNode.ApplyModifiedProperties();

                SetContainerVisibility(container, isOpen);
            });
        }

        protected void SetContainerVisibility(VisualElement container, bool visible)
        {
            container.EnableInClassList("hidden", (visible == false));
        }

        private void ConfigureSkinSpecificStyleSheet()
        {
            string skinSpecificStyleSheetGuid = (EditorGUIUtility.isProSkin) ? DarkStyleSheetAssetGuid : LightStyleSheetAssetGuid;
            StyleSheet skinSpecificStyleSheet = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(skinSpecificStyleSheetGuid);
            View.styleSheets.Add(skinSpecificStyleSheet);
        }

        private void ConfigureDragManipulator()
        {
            var dragManipulator = new DragManipulator(MouseButton.LeftMouse);
            dragManipulator.OnDraggedToLocalPosition += OnDraggedToPosition;
            View.AddManipulator(dragManipulator);
        }

        private void ConfigureContextualMenuManipulator()
        {
            View.AddManipulator(new ContextualMenuManipulator((evt) =>
            {
                evt.menu.AppendAction("Make Initial", MakeOrigin, (a) => DropdownMenuAction.Status.Normal);
                evt.menu.AppendAction("Refresh Ports", RefreshPorts, (a) => DropdownMenuAction.Status.Normal);
                evt.menu.AppendSeparator();
                evt.menu.AppendAction("Delete", DeleteNode, (a) => DropdownMenuAction.Status.Normal);
                evt.StopPropagation();
            }));
        }

        private void ConfigureOriginArrow()
        {
            GraphResource graph = node.graph;
            bool isOrigin = (graph.origin == node);
            SetOriginArrowVisible(isOrigin);
        }

        private void OnDraggedToPosition(Vector2 position, DragManipulator.DragState state)
        {
            if (state == DragManipulator.DragState.Ended)
            {
                node.SetPosition(position);
            }
            else if (state == DragManipulator.DragState.Began)
            {
                View.BringToFront();
            }
        }

        private void DeleteNode(DropdownMenuAction action)
        {
            GraphResource graph = node.graph;
            graph.DeleteNode(node);
        }

        private void MakeOrigin(DropdownMenuAction action)
        {
            GraphResource graph = node.graph;
            if (graph.TrySetOrigin(node, out Error error) == false)
            {
                error.LogWithPrefix("Cannot make this node the graph's origin.");
            }
        }

        private void CreateMissingResourceView()
        {
            VisualElement missingResourceView = new VisualElement()
            {
                style =
                {
                    position = Position.Absolute,
                    left = 0,
                    top = 0,
                    right = 0,
                    bottom = 0,
                    backgroundColor = new Color(0f, 0f, 0f, 0.4f)
                }
            };

            Label missingResourceLabel = new Label("Resource has been deleted.")
            {
                style =
                {
                    position = Position.Absolute,
                    left = 0,
                    top = 0,
                    right = 0,
                    bottom = 0,
                    backgroundColor = new Color(0f, 0f, 0f, 0.5f),
                    unityTextAlign = TextAnchor.MiddleCenter
                }
            };
            missingResourceView.Add(missingResourceLabel);

            // Only allow deletion.
            missingResourceView.AddManipulator(new ContextualMenuManipulator((evt) =>
            {
                evt.menu.AppendAction("Delete", DeleteNode, (a) => DropdownMenuAction.Status.Normal);
                evt.StopPropagation();
            }));

            View.Add(missingResourceView);
        }
    }
}