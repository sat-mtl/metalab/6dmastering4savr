// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class NodeViewControllerFactory
    {
        private const string ViewControllerNodeViewControllerAssetGuid = "12389c0dfcbde824389a38ff1be903a5";

        public static NodeViewController CreateNodeViewControllerForNode(NodeData node)
        {
            NodeViewController nodeViewController = null;
            if (node is ViewControllerNodeData viewControllerNode)
            {
                ViewControllerNodeViewController viewControllerNodeViewControllerTemplate = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<ViewControllerNodeViewController>(ViewControllerNodeViewControllerAssetGuid);
                ViewControllerNodeViewController viewControllerNodeViewController = Object.Instantiate(viewControllerNodeViewControllerTemplate);
                viewControllerNodeViewController.InitializeWithNode(viewControllerNode);

                nodeViewController = viewControllerNodeViewController;
            }

            if (nodeViewController == null)
            {
                Error.Log("Unknown node type.");
            }

            return nodeViewController;
        }
    }
}