// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using Pelican7.UIGraph;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(FieldEdgeData))]
    public class FieldEdgeDataPropertyDrawer : PropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement();
            container.AddToClassList("field-port-edge");

            // property.FindPropertyRelative() does not work when property is a ScriptableObject.
            FieldEdgeData fieldEdge = (FieldEdgeData)property.objectReferenceValue;
            NodeData destinationNode = fieldEdge.destinationNode;
            string destinationName = destinationNode.resource.Name;
            container.Add(new Label(destinationName));

            SerializedObject nodeSerializedObject = property.serializedObject;
            NodeData node = (NodeData)nodeSerializedObject.targetObject;
            Button deleteButton = new Button(() =>
            {
                GraphResource graph = node.graph;
                graph.DeleteFieldEdge(fieldEdge);
            })
            {
                text = "Delete"
            };
            container.Add(deleteButton);

            return container;
        }
    }
}