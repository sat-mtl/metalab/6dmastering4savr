// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(TransitionViewObjectEdgeData))]
    public class TransitionViewObjectEdgeDataPropertyDrawer : TransitionEdgeDataPropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement();
            bool hasEdge = (property.objectReferenceValue != null);
            if (hasEdge)
            {
                TransitionViewObjectEdgeData edge = (TransitionViewObjectEdgeData)property.objectReferenceValue;
                CreateDrawerForEdgeInContainer(edge, container);
                CreateDeleteButtonForEdgeInContainer(edge, container);
            }
            else
            {
                Label emptyLabel = new Label("Not connected.");
                container.Add(emptyLabel);
            }

            return container;
        }

        private void CreateDeleteButtonForEdgeInContainer(TransitionViewObjectEdgeData edge, VisualElement container)
        {
            Button deleteButton = new Button(() =>
            {
                GraphResource graph = edge.graph;
                graph.DeleteTransitionViewObjectEdge(edge);
            })
            {
                text = "Delete"
            };
            deleteButton.AddToClassList("delete-button");
            container.Add(deleteButton);
        }
    }
}