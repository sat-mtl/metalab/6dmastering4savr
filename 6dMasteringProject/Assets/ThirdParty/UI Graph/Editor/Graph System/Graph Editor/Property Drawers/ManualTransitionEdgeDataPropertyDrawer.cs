// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using Pelican7.UIGraph;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(ManualTransitionEdgeData))]
    public class ManualTransitionEdgeDataPropertyDrawer : TransitionEdgeDataPropertyDrawer
    {
        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualElement container = new VisualElement();
            container.AddToClassList("property-panel");

            ManualTransitionEdgeData edge = (ManualTransitionEdgeData)property.objectReferenceValue;
            CreateDrawerForEdgeInContainer(edge, container);
            CreateDeleteButtonForEdgeInContainer(edge, container);

            return container;
        }

        private void CreateDeleteButtonForEdgeInContainer(ManualTransitionEdgeData edge, VisualElement container)
        {
            Button deleteButton = new Button(() =>
            {
                GraphResource graph = edge.graph;
                graph.DeleteManualEdge(edge);
            })
            {
                text = "Delete"
            };
            deleteButton.AddToClassList("delete-button");
            container.Add(deleteButton);
        }

        //private const string UxmlGuid = "06128dcbe1d8a244bbbbed963ff9978a";
        //private const string UssGuid = "d721b68a1262a6b479c7c6c983803301";

        //public override VisualElement CreatePropertyGUI(SerializedProperty property)
        //{
        //    VisualTreeAsset uxml = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<VisualTreeAsset>(UxmlGuid);
        //    TemplateContainer container = uxml.CloneTree();
        //    StyleSheet uss = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(UssGuid);
        //    container.styleSheets.Add(uss);

        //    // property.FindPropertyRelative() does not work when property is a ScriptableObject. binding-path doesn't appear to work in this case either. Configure manually.
        //    ManualTransitionEdgeData edge = (ManualTransitionEdgeData)property.objectReferenceValue;

        //    TextField userIdentifierTextField = (TextField)container.Q("userIdentifierTextField");
        //    userIdentifierTextField.value = edge.userIdentifier;
        //    userIdentifierTextField.RegisterValueChangedCallback((ChangeEvent<string> evt) =>
        //    {
        //        Undo.RecordObject(edge, "Set Edge Identifier");
        //        edge.userIdentifier = evt.newValue;
        //    });

        //    Label transitionGuidLabel = (Label)container.Q("transitionGuidLabel");
        //    transitionGuidLabel.text = edge.transitionDisplayName;

        //    NodeData destinationNode = edge.destinationNode;
        //    string destinationName = destinationNode.resource.Name;
        //    Label destinationLabel = (Label)container.Q("destinationNameLabel");
        //    destinationLabel.text = destinationName;

        //    Button deleteButton = (Button)container.Q("deleteButton");
        //    deleteButton.clickable.clicked += () =>
        //    {
        //        GraphResource graph = edge.graph;
        //        graph.DeleteManualEdge(edge);
        //    };

        //    return container;
        //}
    }
}