// Copyright � 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    [CustomPropertyDrawer(typeof(GraphableEmbedViewObjectPortData))]
    public class GraphableEmbedViewObjectPortDataPropertyDrawer : PropertyDrawer
    {
        private const string UxmlGuid = "cef78ac6fd47a9f4ca0a93a8d1e8b316";
        private const string UssGuid = "5e7dbc97b41fde247a5379f27508c799";
        private const string DarkUssGuid = "e0ad901ab6758ba4bb39dbbc74b2a41f";
        private const string LightUssGuid = "15394adbc76abcc41a066665cff1a4cc";

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            VisualTreeAsset uxml = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<VisualTreeAsset>(UxmlGuid);
            TemplateContainer container = uxml.CloneTree();
            StyleSheet uss = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(UssGuid);
            container.styleSheets.Add(uss);

            AddSkinSpecificStyleSheetToContainer(container);

            Label titleLabel = (Label)container.Q("titleLabel");
            SerializedProperty displayNameProperty = property.FindPropertyRelative("embedViewObjectDisplayName");
            string displayName = displayNameProperty.stringValue;
            titleLabel.text = displayName;

            SerializedProperty edgeProperty = property.FindPropertyRelative("edge");
            bool hasNoEdge = (edgeProperty.objectReferenceValue == null);

            NodeConnectorView nodeConnectorView = (NodeConnectorView)container.Q("nodeConnectorView");
            nodeConnectorView.AllowNewConnections = hasNoEdge;

            return container;
        }

        private void AddSkinSpecificStyleSheetToContainer(VisualElement container)
        {
            string skinSpecificStyleSheetGuid = (EditorGUIUtility.isProSkin) ? DarkUssGuid : LightUssGuid;
            StyleSheet skinSpecificStyleSheet = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<StyleSheet>(skinSpecificStyleSheetGuid);
            container.styleSheets.Add(skinSpecificStyleSheet);
        }
    }
}