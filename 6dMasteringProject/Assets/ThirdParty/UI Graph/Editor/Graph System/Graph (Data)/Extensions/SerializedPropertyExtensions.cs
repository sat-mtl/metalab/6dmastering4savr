﻿using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class SerializedPropertyExtensions
    {
        public static void AddArrayElement(this SerializedProperty property, Object obj)
        {
            if (property.isArray == false)
            {
                Error.Log("Serialized property is not an array.");
                return;
            }

            property.arraySize += 1;
            int lastIndex = property.arraySize - 1;
            var arrayElement = property.GetArrayElementAtIndex(lastIndex);
            arrayElement.objectReferenceValue = obj;
        }

        public static void RemoveArrayElementAtIndex(this SerializedProperty property, int index)
        {
            if (property.isArray == false)
            {
                Error.Log("Serialized property is not an array.");
                return;
            }

            // By assigning null to the element before deletion, Unity will resize the array for us.
            var arrayElement = property.GetArrayElementAtIndex(index);
            arrayElement.objectReferenceValue = null;
            property.DeleteArrayElementAtIndex(index);
        }
    }
}