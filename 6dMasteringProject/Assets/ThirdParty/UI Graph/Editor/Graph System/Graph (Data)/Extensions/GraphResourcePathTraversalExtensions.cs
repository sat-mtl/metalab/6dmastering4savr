﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;

namespace Pelican7.UIGraph.Editor
{
    public static class GraphResourcePathTraversalExtensions
    {
        public enum TransitionEdgePathDirection
        {
            NotConnected,   // A NotConnected path occurs when two nodes have no set of edges connecting them in either direction.
            Presentation,   // A Presentation path occurs when two nodes have either a set of Presentation and/or Embed edges connecting them, or a set of Dismissal edges connecting them in reverse.
            Dismissal,      // A Dismissal path occurs when two nodes have either a set of Dismissal edges connecting them, or a set of Presentation and/or Embed edges connecting them in reverse.
            Replacement,    // A Replacement path occurs when two nodes have a set of Replacement and Presentation/Embed edges connecting them in either direction.
        }

        public static bool TryGetAnyPathBetweenNodesViaEdgesWithTypes(this GraphResource graph, NodeData source, NodeData destination, EdgeData.EdgeType edgeTypes, out Path path)
        {
            // Search for a path from source to destination. If no path is found, search from destination to source.
            if (graph.BreadthFirstSearchViaEdgesWithTypes(source, destination, edgeTypes, out path))
            {
                return true;
            }
            else
            {
                return graph.BreadthFirstSearchViaEdgesWithTypes(destination, source, edgeTypes, out path);
            }
        }

        public static TransitionEdgePathDirection TransitionEdgePathDirectionFromNodeToNode(this GraphResource graph, NodeData source, NodeData destination)
        {
            if (graph.BreadthFirstSearchViaEdgesWithTypes(source, destination, EdgeData.EdgeType.Presentation | EdgeData.EdgeType.Embed))
            {
                // Source contains a path of presentation/embed edges to destination.
                return TransitionEdgePathDirection.Presentation;
            }
            else if (graph.BreadthFirstSearchViaEdgesWithTypes(source, destination, EdgeData.EdgeType.Dismissal))
            {
                // Source contains a path of dismissal edges to destination.
                return TransitionEdgePathDirection.Dismissal;
            }
            else if (graph.BreadthFirstSearchViaEdgesWithTypes(source, destination, EdgeData.EdgeType.AllDownstreamTypes))
            {
                // Source contains a path of downstream edges to destination that aren't presentation/embed only.
                return TransitionEdgePathDirection.Replacement;
            }
            else if (graph.BreadthFirstSearchViaEdgesWithTypes(destination, source, EdgeData.EdgeType.Presentation | EdgeData.EdgeType.Embed))
            {
                // Destination contains a path of presentation/embed edges to source. Therefore source to destination is a dismissal direction.
                return TransitionEdgePathDirection.Dismissal;
            }
            else if (graph.BreadthFirstSearchViaEdgesWithTypes(destination, source, EdgeData.EdgeType.Dismissal))
            {
                // Destination contains a path of dismissal edges to source. Therefore source to destination is a presentation direction.
                return TransitionEdgePathDirection.Presentation;
            }
            else if (graph.BreadthFirstSearchViaEdgesWithTypes(destination, source, EdgeData.EdgeType.AllDownstreamTypes))
            {
                // Destination contains a path of downstream edges to source that aren't presentation/embed only. Therefore source to destination is replacement.
                return TransitionEdgePathDirection.Replacement;
            }
            else
            {
                return TransitionEdgePathDirection.NotConnected;
            }
        }

        private static bool BreadthFirstSearchViaEdgesWithTypes(this GraphResource graph, NodeData origin, NodeData target, EdgeData.EdgeType edgeTypes, out Path shortestPath)
        {
            Queue<Path> queue = new Queue<Path>();
            var visitedNodes = new Dictionary<string, NodeData>
            {
                { origin.guid, origin }
            };
            queue.Enqueue(new Path(origin));

            while (queue.Count > 0)
            {
                Path path = queue.Dequeue();
                NodeData node = path.EndNode;

                List<EdgeData> edges = node.EdgesWithTypes(edgeTypes);
                foreach (EdgeData edge in edges)
                {
                    NodeData n = edge.destinationNode;
                    if (visitedNodes.ContainsKey(n.guid) == false)
                    {
                        path.Add(edge);
                        if (n.Equals(target))
                        {
                            shortestPath = path;
                            return true;
                        }

                        visitedNodes.Add(n.guid, n);
                        queue.Enqueue(new Path(path));
                    }
                }
            }

            shortestPath = null;
            return false;
        }

        private static bool BreadthFirstSearchViaEdgesWithTypes(this GraphResource graph, NodeData origin, NodeData target, EdgeData.EdgeType edgeTypes)
        {
            return graph.BreadthFirstSearchViaEdgesWithTypes(origin, target, edgeTypes, out _);
        }

        public class Path : List<EdgeData>
        {
            public NodeData origin;

            public Path(NodeData origin)
            {
                this.origin = origin;
            }

            public Path(Path path) : this(path.origin)
            {
                foreach (EdgeData edge in path)
                {
                    Add(edge);
                }
            }

            public EdgeData Start
            {
                get
                {
                    return (Count > 0) ? this[0] : null;
                }
            }

            public EdgeData End
            {
                get
                {
                    return (Count > 0) ? this[Count - 1] : null;
                }
            }

            public NodeData EndNode
            {
                get
                {
                    return (End != null) ? End.destinationNode : origin;
                }
            }
        }
    }
}