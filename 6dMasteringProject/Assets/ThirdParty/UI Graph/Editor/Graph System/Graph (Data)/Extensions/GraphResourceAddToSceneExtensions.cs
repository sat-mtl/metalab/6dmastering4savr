﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace Pelican7.UIGraph.Editor
{
    public static class GraphResourceAddToSceneExtensions
    {
        public static void AddToCurrentScene(this GraphResource graph)
        {
            if (graph.IsCanvasBased())
            {
                graph.AddCanvasGraphResourceToCurrentScene();
            }
            else
            {
                Error.Log("Unable to determine graph's view controller type. This can happen if a graph has no nodes.");
            }
        }

        private static bool IsCanvasBased(this GraphResource graph)
        {
            NodeData anyNode = null;
            if (graph.nodes.Count > 0)
            {
                anyNode = graph.nodes[0];
            }

            if (anyNode != null)
            {
                IGraphable graphable = anyNode.resource.Value;
                if (typeof(CanvasController).IsAssignableFrom(graphable.GetType()))
                {
                    return true;
                }
            }

            return false;
        }

        private static void AddCanvasGraphResourceToCurrentScene(this GraphResource graph)
        {
            string undoGroupName = "Add Graph To Current Scene";

            GameObject graphPresenterObject = new GameObject("Graph Presenter");
            CanvasGraphPresenter graphPresenter = graphPresenterObject.AddComponent<CanvasGraphPresenter>();
            graphPresenter.graphResource = graph;
            Undo.RegisterCreatedObjectUndo(graphPresenterObject, undoGroupName);

            GameObject canvasWindowObject = CanvasWindowCreator.CreateCanvasWindowGameObject();
            CanvasWindow canvasWindow = canvasWindowObject.GetComponent<CanvasWindow>();
            GameObjectUtility.SetParentAndAlign(canvasWindowObject, graphPresenterObject);
            Undo.RegisterCreatedObjectUndo(canvasWindow, undoGroupName);

            Undo.RecordObject(graphPresenter, undoGroupName);
            graphPresenter.window = canvasWindow;

            AddEventSystemToActiveSceneIfNecessary(undoGroupName);

            Selection.activeObject = graphPresenterObject;
        }

        private static void AddEventSystemToActiveSceneIfNecessary(string undoGroupName)
        {
            Scene scene = SceneManager.GetActiveScene();
            if (SceneExtensions.FindAllComponentsInScene<EventSystem>(scene).Count == 0)
            {
                GameObject eventSystemObject = new GameObject("Event System");
                eventSystemObject.AddComponent<StandaloneInputModule>();
                Undo.RegisterCreatedObjectUndo(eventSystemObject, undoGroupName);
            }
        }
    }
}