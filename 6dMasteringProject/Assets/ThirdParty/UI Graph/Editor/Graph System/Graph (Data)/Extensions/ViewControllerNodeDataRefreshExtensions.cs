﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class ViewControllerNodeDataRefreshExtensions
    {
        public static void Refresh(this ViewControllerNodeData node)
        {
            string undoGroupName = "Refresh Node";
            Undo.SetCurrentGroupName(undoGroupName);
            int undoGroupIndex = Undo.GetCurrentGroup();

            Undo.RegisterCompleteObjectUndo(node, undoGroupName);
            node.RefreshFieldPortsWithUndo(undoGroupName);
            node.RefreshEmbedViewObjectPortsWithUndo(undoGroupName);
            node.RefreshTransitionViewObjectPortsWithUndo(undoGroupName);

            Undo.CollapseUndoOperations(undoGroupIndex);
        }

        private static void RefreshFieldPortsWithUndo(this ViewControllerNodeData node, string undoGroupName)
        {
            GraphableReference resource = node.resource;
            IGraphable graphable = resource.Value;
            if (graphable is IGraphableFieldProvider graphableFieldProvider)
            {
                List<GraphableFieldPortData> newFieldPorts = node.BuildFieldsPortsList(graphableFieldProvider, out List<GraphableFieldPortData> removedFieldPorts);
                node.DeleteAllEdgesInFieldPorts(removedFieldPorts, undoGroupName);

                node.fieldPorts = newFieldPorts;
            }
        }

        private static List<GraphableFieldPortData> BuildFieldsPortsList(this ViewControllerNodeData node, IGraphableFieldProvider graphableFieldProvider, out List<GraphableFieldPortData> removedFieldPorts)
        {
            GraphableField[] graphableFields = graphableFieldProvider.GraphableFields;
            List<GraphableFieldPortData> oldFieldPorts = node.fieldPorts;
            List<GraphableFieldPortData> newFieldPorts = new List<GraphableFieldPortData>(graphableFields.Length);
            foreach (GraphableField graphableField in graphableFields)
            {
                GraphableFieldPortData fieldPort = new GraphableFieldPortData(graphableField);
                GraphableFieldPortData oldFieldPort = oldFieldPorts.SingleOrDefault((fp) => fp.fieldName.Equals(graphableField.FieldName));
                if (oldFieldPort != null)
                {
                    fieldPort.edges = oldFieldPort.edges;
                    oldFieldPorts.Remove(oldFieldPort);
                }

                bool newFieldPortsAlreadyContainsPortWithName = newFieldPorts.Any((fp) => fp.fieldName.Equals(fieldPort.fieldName));
                if (newFieldPortsAlreadyContainsPortWithName == false)
                {
                    newFieldPorts.Add(fieldPort);
                }
                else
                {
                    Error.Log(string.Format("No graphable field port was created for '{0}' because a port with the same name already exists.", fieldPort.fieldName));
                }
            }

            removedFieldPorts = oldFieldPorts;
            return newFieldPorts;
        }

        private static void DeleteAllEdgesInFieldPorts(this ViewControllerNodeData node, List<GraphableFieldPortData> fieldPorts, string undoGroupName)
        {
            GraphResource graph = node.graph;
            foreach (GraphableFieldPortData fieldPort in fieldPorts)
            {
                // Take a copy of the array to avoid mutating whilst iterating (DeleteFieldEdge modifies fieldPort.edges).
                List<FieldEdgeData> fieldEdges = new List<FieldEdgeData>(fieldPort.edges);
                foreach (FieldEdgeData fieldEdge in fieldEdges)
                {
                    graph.DeleteFieldEdge(fieldEdge, undoGroupName);
                }
            }
        }

        private static void RefreshEmbedViewObjectPortsWithUndo(this ViewControllerNodeData node, string undoGroupName)
        {
            GraphableReference resource = node.resource;
            IGraphable graphable = resource.Value;
            if (graphable is IGraphableEmbedContainer graphableEmbedContainer)
            {
                List<GraphableEmbedViewObjectPortData> newEmbedViewObjectPorts = node.BuildEmbedViewObjectPortsList(graphableEmbedContainer, out List<GraphableEmbedViewObjectPortData> removedTransitionViewObjectPorts);
                node.DeleteAllEdgesInEmbedViewObjectPorts(removedTransitionViewObjectPorts, undoGroupName);

                node.embedViewObjectPorts = newEmbedViewObjectPorts;
            }
        }

        private static List<GraphableEmbedViewObjectPortData> BuildEmbedViewObjectPortsList(this ViewControllerNodeData node, IGraphableEmbedContainer graphableEmbedContainer, out List<GraphableEmbedViewObjectPortData> removedEmbedViewObjectPorts)
        {
            IGraphableEmbedViewObject[] graphableEmbedViewObjects = graphableEmbedContainer.GraphableEmbedViewObjects;
            List<GraphableEmbedViewObjectPortData> oldEmbedViewObjectPorts = node.embedViewObjectPorts;
            List<GraphableEmbedViewObjectPortData> newEmbedViewObjectPorts = new List<GraphableEmbedViewObjectPortData>(graphableEmbedViewObjects.Length);
            foreach (IGraphableEmbedViewObject graphableEmbedViewObject in graphableEmbedViewObjects)
            {
                GraphableEmbedViewObjectPortData embedViewObjectPort = new GraphableEmbedViewObjectPortData(graphableEmbedViewObject);
                GraphableEmbedViewObjectPortData oldEmbedViewObjectPort = oldEmbedViewObjectPorts.SingleOrDefault((evop) => evop.embedViewObjectGuid.Equals(graphableEmbedViewObject.Guid));
                if (oldEmbedViewObjectPort != null)
                {
                    embedViewObjectPort.edge = oldEmbedViewObjectPort.edge;
                    oldEmbedViewObjectPorts.Remove(oldEmbedViewObjectPort);
                }

                bool newEmbedViewObjectPortsAlreadyContainsPortWithGuid = newEmbedViewObjectPorts.Any((evop) => evop.embedViewObjectGuid.Equals(embedViewObjectPort.embedViewObjectGuid));
                if (newEmbedViewObjectPortsAlreadyContainsPortWithGuid == false)
                {
                    newEmbedViewObjectPorts.Add(embedViewObjectPort);
                }
                else
                {
                    Error.Log(string.Format("No graphable embed port was created for '{0}' because a port with the same identifier already exists ({1}).", embedViewObjectPort.embedViewObjectDisplayName, embedViewObjectPort.embedViewObjectGuid));
                }
            }

            removedEmbedViewObjectPorts = oldEmbedViewObjectPorts;
            return newEmbedViewObjectPorts;
        }

        private static void DeleteAllEdgesInEmbedViewObjectPorts(this ViewControllerNodeData node, List<GraphableEmbedViewObjectPortData> embedViewObjectPorts, string undoGroupName)
        {
            GraphResource graph = node.graph;
            foreach (GraphableEmbedViewObjectPortData embedViewObjectPort in embedViewObjectPorts)
            {
                EmbedViewObjectEdgeData edge = embedViewObjectPort.edge;
                if (edge != null)
                {
                    graph.DeleteEmbedViewObjectEdge(edge, undoGroupName);
                }
            }
        }

        private static void RefreshTransitionViewObjectPortsWithUndo(this ViewControllerNodeData node, string undoGroupName)
        {
            GraphableReference resource = node.resource;
            IGraphable graphable = resource.Value;
            if (graphable is IGraphableTransitionViewObjectContainer graphableViewObjectContainer)
            {
                List<GraphableTransitionViewObjectPortData> newTransitionViewObjectPorts = node.BuildTransitionViewObjectPortsList(graphableViewObjectContainer, out List<GraphableTransitionViewObjectPortData> removedTransitionViewObjectPorts);
                node.DeleteAllEdgesInTransitionViewObjectPorts(removedTransitionViewObjectPorts, undoGroupName);

                node.transitionViewObjectPorts = newTransitionViewObjectPorts;
            }
        }

        private static List<GraphableTransitionViewObjectPortData> BuildTransitionViewObjectPortsList(this ViewControllerNodeData node, IGraphableTransitionViewObjectContainer graphableTransitionViewObjectContainer, out List<GraphableTransitionViewObjectPortData> removedTransitionViewObjectPorts)
        {
            IGraphableTransitionViewObject[] graphableTransitionViewObjects = graphableTransitionViewObjectContainer.GraphableTransitionViewObjects;
            List<GraphableTransitionViewObjectPortData> oldTransitionViewObjectPorts = node.transitionViewObjectPorts;
            List<GraphableTransitionViewObjectPortData> newTransitionViewObjectPorts = new List<GraphableTransitionViewObjectPortData>(graphableTransitionViewObjects.Length);
            foreach (IGraphableTransitionViewObject graphableTransitionViewObject in graphableTransitionViewObjects)
            {
                GraphableTransitionViewObjectPortData transitionViewObjectPort = new GraphableTransitionViewObjectPortData(graphableTransitionViewObject);
                GraphableTransitionViewObjectPortData oldTransitionViewObjectPort = oldTransitionViewObjectPorts.SingleOrDefault((tvop) => tvop.transitionViewObjectGuid.Equals(graphableTransitionViewObject.Guid));
                if (oldTransitionViewObjectPort != null)
                {
                    foreach (TransitionViewObjectEdgeData edge in oldTransitionViewObjectPort.edges)
                    {
                        transitionViewObjectPort.AddEdge(edge);
                    }

                    oldTransitionViewObjectPorts.Remove(oldTransitionViewObjectPort);
                }

                bool newTransitionViewObjectPortsAlreadyContainsPortWithGuid = newTransitionViewObjectPorts.Any((tvop) => tvop.transitionViewObjectGuid.Equals(transitionViewObjectPort.transitionViewObjectGuid));
                if (newTransitionViewObjectPortsAlreadyContainsPortWithGuid == false)
                {
                    newTransitionViewObjectPorts.Add(transitionViewObjectPort);
                }
                else
                {
                    Error.Log(string.Format("No graphable transition port was created for '{0}' because a port with the same identifier already exists ({1}).", transitionViewObjectPort.transitionViewObjectDisplayName, transitionViewObjectPort.transitionViewObjectGuid));
                }
            }

            removedTransitionViewObjectPorts = oldTransitionViewObjectPorts;
            return newTransitionViewObjectPorts;
        }

        private static void DeleteAllEdgesInTransitionViewObjectPorts(this ViewControllerNodeData node, List<GraphableTransitionViewObjectPortData> transitionViewObjectPorts, string undoGroupName)
        {
            GraphResource graph = node.graph;
            foreach (GraphableTransitionViewObjectPortData transitionViewObjectPort in transitionViewObjectPorts)
            {
                // Take a copy of the array to avoid mutating whilst iterating (DeleteTransitionViewObjectEdge modifies transitionViewObjectPort.edges).
                List<TransitionViewObjectEdgeData> transitionViewObjectEdges = new List<TransitionViewObjectEdgeData>(transitionViewObjectPort.edges);
                foreach (TransitionViewObjectEdgeData transitionViewObjectEdge in transitionViewObjectEdges)
                {
                    graph.DeleteTransitionViewObjectEdge(transitionViewObjectEdge, undoGroupName);
                }
            }
        }
    }
}