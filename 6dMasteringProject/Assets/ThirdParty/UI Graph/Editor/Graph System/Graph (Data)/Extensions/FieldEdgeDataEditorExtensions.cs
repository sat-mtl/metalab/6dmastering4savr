﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Editor
{
    public static class FieldEdgeDataEditorExtensions
    {
        public static void Initialize(this FieldEdgeData fieldEdge, GraphResource graph, string fieldPortName, NodeData sourceNode, NodeData destinationNode)
        {
            fieldEdge.guid = System.Guid.NewGuid().ToString();
            fieldEdge.graph = graph;
            fieldEdge.fieldPortName = fieldPortName;
            fieldEdge.sourceNode = sourceNode;
            fieldEdge.destinationNode = destinationNode;
            fieldEdge.targetNode = sourceNode; // A field edge's target is always its source.
            fieldEdge.type = EdgeData.EdgeType.Embed; // A field edge's type is always embed.

            fieldEdge.name = string.Format("[FieldEdge] {0} --> {1} ({2})", sourceNode.resource.Name, destinationNode.resource.Name, fieldEdge.guid);
        }
    }
}