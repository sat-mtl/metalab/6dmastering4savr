﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class NodeDataFactory
    {
        public static bool TryCreateNodeForGraphable(IGraphable graphable, out NodeData node, out Error error)
        {
            node = null;
            if (typeof(CanvasController).IsAssignableFrom(graphable.GetType()) || typeof(ElementsController).IsAssignableFrom(graphable.GetType()))
            {
                node = ScriptableObject.CreateInstance<ViewControllerNodeData>();
            }

            bool success = (node != null);
            error = (success == false) ? new Error("Unknown graphable type.") : null;

            return success;
        }
    }
}