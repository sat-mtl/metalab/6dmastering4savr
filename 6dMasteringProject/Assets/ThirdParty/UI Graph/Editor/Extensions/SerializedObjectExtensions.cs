﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public static class SerializedObjectExtensions
    {
        public static void AddAllPropertyFieldsToContainer(this SerializedObject serializedObject, VisualElement container, HashSet<string> excludedPropertyPaths)
        {
            SerializedProperty property = serializedObject.GetIterator();
            property.NextVisible(true);
            do 
            {
                if (excludedPropertyPaths.Contains(property.propertyPath) == false)
                {
                    var propertyField = new PropertyField(property);
                    container.Add(propertyField);
                }
            }
            while (property.NextVisible(false));
        }

        public static void AddAllPropertyFieldsToContainer(this SerializedObject serializedObject, VisualElement container)
        {
            AddAllPropertyFieldsToContainer(serializedObject, container, new HashSet<string>());
        }
    }
}