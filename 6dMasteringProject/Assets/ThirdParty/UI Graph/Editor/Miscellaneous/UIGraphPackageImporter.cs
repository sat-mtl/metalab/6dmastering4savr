﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    [InitializeOnLoad]
    public static class UIGraphPackageImporter
    {
        private const string UIGraphPackageName = "UI Graph A Menu System For Unity";

        static UIGraphPackageImporter()
        {
            var settings = AssetDatabaseExtensions.LoadFirstAssetOfType<UIGraphProjectSettings>();
            if ((settings == null) && EditorApplication.isUpdating)
            {
                AssetDatabase.importPackageCompleted += AssetDatabaseImportPackageCompleted;
            }
        }

        private static void AssetDatabaseImportPackageCompleted(string packageName)
        {
            if (packageName.Equals(UIGraphPackageName))
            {
                AssetDatabase.importPackageCompleted -= AssetDatabaseImportPackageCompleted;
                if (UIGraphProjectSettings.InstanceExists == false)
                {
                    UIGraphProjectSettingsGenerator.GenerateDefaultProjectSettings();
                }

                // Default view controller templates/classes will be added to the processing queue upon first import. Remove them.
                var processorQueue = ViewControllerScriptChangeViewBindingsProcessorQueue.Instance;
                processorQueue?.Clear();
            }
        }
    }
}