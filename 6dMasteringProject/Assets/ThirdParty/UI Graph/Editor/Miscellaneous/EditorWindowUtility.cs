﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;

namespace Pelican7.UIGraph.Editor
{
    public static class EditorWindowUtility
    {
        public static void ShowInspectorEditorWindow()
        {
            string inspectorWindowTypeName = "UnityEditor.InspectorWindow";
            ShowEditorWindowWithTypeName(inspectorWindowTypeName);
        }

        public static void ShowSceneEditorWindow()
        {
            string sceneWindowTypeName = "UnityEditor.SceneView";
            ShowEditorWindowWithTypeName(sceneWindowTypeName);
        }

        public static void ShowSceneHierarchyEditorWindow()
        {
            string sceneHierarchyWindowTypeName = "UnityEditor.SceneHierarchyWindow";
            ShowEditorWindowWithTypeName(sceneHierarchyWindowTypeName);
        }

        public static void ShowProjectBrowserEditorWindow()
        {
            string projectBrowserWindowTypeName = "UnityEditor.ProjectBrowser";
            ShowEditorWindowWithTypeName(projectBrowserWindowTypeName);
        }

        public static void ShowEditorWindowWithTypeName(string windowTypeName)
        {
            var windowType = typeof(UnityEditor.Editor).Assembly.GetType(windowTypeName);
            EditorWindow.GetWindow(windowType);
        }
    }
}