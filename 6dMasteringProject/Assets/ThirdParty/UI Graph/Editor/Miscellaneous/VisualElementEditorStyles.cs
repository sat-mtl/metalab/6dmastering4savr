﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

namespace Pelican7.UIGraph.Editor
{
    public static class VisualElementEditorStyles
    {
        public const string SectionStyleClass = "section";
        public const string HeaderLabelStyleClass = "header-label";
    }
}