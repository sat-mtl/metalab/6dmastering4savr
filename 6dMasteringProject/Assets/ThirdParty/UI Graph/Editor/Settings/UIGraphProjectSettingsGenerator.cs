﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.IO;
using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public static class UIGraphProjectSettingsGenerator
    {
        private const string ProjectSettingsLocalFolderPath = "Project Settings (User)";
        private const string TransitionSettingsLocalFolderPath = ProjectSettingsLocalFolderPath + "/Transition Settings";
        private const string UICanvasTransitionSettingsLocalFolderPath = TransitionSettingsLocalFolderPath + "/UICanvas";
        private const string UIElementsTransitionSettingsLocalFolderPath = TransitionSettingsLocalFolderPath + "/UIElements";
        private const string DefaultProjectSettingsTemplateGuid = "4b969756f05810c43a8239a6bdd813e0";
        private const string DefaultCanvasControllerSequenceProviderAssetTemplateGuid = "9063bfbbd92fe884f980fc8c4655e9c0";
        private const string DefaultNavigationCanvasControllerSequenceProviderAssetTemplateGuid = "8183580754e5aa04e987458b214fbfdc";
        private const string DefaultSplitCanvasControllerSequenceProviderAssetTemplateGuid = "027afcd8ba90b2c42af632cc9a883d7e";
        private const string DefaultStackCanvasControllerSequenceProviderAssetTemplateGuid = "fb4bf803d9b3f9049bbe1fbfbbb18e50";
        private const string DefaultTabBarCanvasControllerSequenceProviderAssetTemplateGuid = "5e43ec876648bfb44b21ba5d6e1c1840";

        public static UIGraphProjectSettings GenerateDefaultProjectSettings()
        {
            UIGraphFolderLocation rootFolder = UIGraphFolderLocation.UIGraphPackageRootFolder;

            CreateProjectSettingsDirectoryIfNecessary(rootFolder);
            CreateTransitionSettingsDirectoryIfNecessary(rootFolder);

            // Generate.
            string settingsAssetPath = WriteSettingsAssetFile(rootFolder);

            // UI Canvas.
            CreateUICanvasTransitionSettingsDirectoryIfNecessary(rootFolder);
            string defaultCanvasControllerSequenceProviderAssetPath = WriteDefaultCanvasControllerSequenceProviderAssetFile(rootFolder);
            string defaultNavigationCanvasControllerSequenceProviderAssetPath = WriteDefaultNavigationCanvasControllerSequenceProviderAssetFile(rootFolder);
            string defaultSplitCanvasControllerSequenceProviderAssetPath = WriteDefaultSplitCanvasControllerSequenceProviderAssetFile(rootFolder);
            string defaultStackCanvasControllerSequenceProviderAssetPath = WriteDefaultStackCanvasControllerSequenceProviderAssetFile(rootFolder);
            string defaultTabBarCanvasControllerSequenceProviderAssetPath = WriteDefaultTabBarCanvasControllerSequenceProviderAssetFile(rootFolder);
            var containerProviderPaths = new string[]
            {
                defaultNavigationCanvasControllerSequenceProviderAssetPath,
                defaultSplitCanvasControllerSequenceProviderAssetPath,
                defaultStackCanvasControllerSequenceProviderAssetPath,
                defaultTabBarCanvasControllerSequenceProviderAssetPath
            };

            // Import.
            AssetDatabase.StartAssetEditing();
            AssetDatabase.ImportAsset(settingsAssetPath, ImportAssetOptions.ForceSynchronousImport);
            AssetDatabase.ImportAsset(defaultCanvasControllerSequenceProviderAssetPath, ImportAssetOptions.ForceSynchronousImport);
            foreach (string containerProviderPath in containerProviderPaths)
            {
                AssetDatabase.ImportAsset(containerProviderPath, ImportAssetOptions.ForceSynchronousImport);
            }
            AssetDatabase.StopAssetEditing();

            // Configure.
            var settings = AssetDatabase.LoadAssetAtPath<UIGraphProjectSettings>(settingsAssetPath);
            settings.version = UIGraphPackageVersion.StringValue;
            var defaultCanvasControllerSequenceProvider = AssetDatabase.LoadAssetAtPath<CanvasControllerSequenceProvider>(defaultCanvasControllerSequenceProviderAssetPath);
            settings.createCanvasControllerTemplate.transitionAnimatorProvider = defaultCanvasControllerSequenceProvider;
            var defaultNavigationCanvasControllerSequenceProvider = AssetDatabase.LoadAssetAtPath<ContainerCanvasControllerSequenceProvider>(defaultNavigationCanvasControllerSequenceProviderAssetPath);
            settings.createNavigationCanvasControllerInstanceTemplate.transitionAnimatorProvider = defaultNavigationCanvasControllerSequenceProvider;
            var defaultSplitCanvasControllerSequenceProvider = AssetDatabase.LoadAssetAtPath<ContainerCanvasControllerSequenceProvider>(defaultSplitCanvasControllerSequenceProviderAssetPath);
            settings.createSplitCanvasControllerInstanceTemplate.transitionAnimatorProvider = defaultSplitCanvasControllerSequenceProvider;
            var defaultStackCanvasControllerSequenceProvider = AssetDatabase.LoadAssetAtPath<ContainerCanvasControllerSequenceProvider>(defaultStackCanvasControllerSequenceProviderAssetPath);
            settings.createStackCanvasControllerInstanceTemplate.transitionAnimatorProvider = defaultStackCanvasControllerSequenceProvider;
            var defaultTabBarCanvasControllerSequenceProvider = AssetDatabase.LoadAssetAtPath<ContainerCanvasControllerSequenceProvider>(defaultTabBarCanvasControllerSequenceProviderAssetPath);
            settings.createTabBarCanvasControllerInstanceTemplate.transitionAnimatorProvider = defaultTabBarCanvasControllerSequenceProvider;
            EditorUtility.SetDirty(settings);

            foreach (string containerProviderPath in containerProviderPaths)
            {
                var containerProvider = AssetDatabase.LoadAssetAtPath<ContainerCanvasControllerSequenceProvider>(containerProviderPath);
                containerProvider.fallbackProvider = defaultCanvasControllerSequenceProvider;
                EditorUtility.SetDirty(containerProvider);
            }

            AssetDatabase.SaveAssets();
            Debug.LogFormat("Default UI Graph assets have been generated for your project and can be found in the folder '{0}'.", Path.Combine(rootFolder.Directory, ProjectSettingsLocalFolderPath));

            return settings;
        }

        private static void CreateProjectSettingsDirectoryIfNecessary(UIGraphFolderLocation rootFolder)
        {
            string projectSettingsFullPath = Path.Combine(rootFolder.FullDirectory, ProjectSettingsLocalFolderPath);
            Directory.CreateDirectory(projectSettingsFullPath);
        }

        private static void CreateTransitionSettingsDirectoryIfNecessary(UIGraphFolderLocation rootFolder)
        {
            string transitionSettingsFullPath = Path.Combine(rootFolder.FullDirectory, TransitionSettingsLocalFolderPath);
            Directory.CreateDirectory(transitionSettingsFullPath);
        }

        private static void CreateUICanvasTransitionSettingsDirectoryIfNecessary(UIGraphFolderLocation rootFolder)
        {
            string fullPath = Path.Combine(rootFolder.FullDirectory, UICanvasTransitionSettingsLocalFolderPath);
            Directory.CreateDirectory(fullPath);
        }

        private static void CreateUIElementsTransitionSettingsDirectoryIfNecessary(UIGraphFolderLocation rootFolder)
        {
            string fullPath = Path.Combine(rootFolder.FullDirectory, UIElementsTransitionSettingsLocalFolderPath);
            Directory.CreateDirectory(fullPath);
        }

        private static string WriteSettingsAssetFile(UIGraphFolderLocation rootFolder)
        {
            string settingsAssetPath = Path.Combine(rootFolder.Directory, ProjectSettingsLocalFolderPath, "UI Graph - Project Settings.asset");
            WriteAssetFile(settingsAssetPath, DefaultProjectSettingsTemplateGuid);
            return settingsAssetPath;
        }

        private static string WriteDefaultCanvasControllerSequenceProviderAssetFile(UIGraphFolderLocation rootFolder)
        {
            return WriteUICanvasDefaultSequenceProviderAssetFile(rootFolder, "Default Canvas Controller Sequence Provider.asset", DefaultCanvasControllerSequenceProviderAssetTemplateGuid);
        }

        private static string WriteDefaultNavigationCanvasControllerSequenceProviderAssetFile(UIGraphFolderLocation rootFolder)
        {
            return WriteUICanvasDefaultSequenceProviderAssetFile(rootFolder, "Default Navigation Canvas Controller Sequence Provider.asset", DefaultNavigationCanvasControllerSequenceProviderAssetTemplateGuid);
        }

        private static string WriteDefaultSplitCanvasControllerSequenceProviderAssetFile(UIGraphFolderLocation rootFolder)
        {
            return WriteUICanvasDefaultSequenceProviderAssetFile(rootFolder, "Default Split Canvas Controller Sequence Provider.asset", DefaultSplitCanvasControllerSequenceProviderAssetTemplateGuid);
        }

        private static string WriteDefaultStackCanvasControllerSequenceProviderAssetFile(UIGraphFolderLocation rootFolder)
        {
            return WriteUICanvasDefaultSequenceProviderAssetFile(rootFolder, "Default Stack Canvas Controller Sequence Provider.asset", DefaultStackCanvasControllerSequenceProviderAssetTemplateGuid);
        }

        private static string WriteDefaultTabBarCanvasControllerSequenceProviderAssetFile(UIGraphFolderLocation rootFolder)
        {
            return WriteUICanvasDefaultSequenceProviderAssetFile(rootFolder, "Default Tab Bar Canvas Controller Sequence Provider.asset", DefaultTabBarCanvasControllerSequenceProviderAssetTemplateGuid);
        }

        private static string WriteUICanvasDefaultSequenceProviderAssetFile(UIGraphFolderLocation rootFolder, string fileName, string textAssetTemplateGuid)
        {
            return WriteDefaultSequenceProviderAssetFile(rootFolder, UICanvasTransitionSettingsLocalFolderPath, fileName, textAssetTemplateGuid);
        }

        private static string WriteUIElementsDefaultSequenceProviderAssetFile(UIGraphFolderLocation rootFolder, string fileName, string textAssetTemplateGuid)
        {
            return WriteDefaultSequenceProviderAssetFile(rootFolder, UIElementsTransitionSettingsLocalFolderPath, fileName, textAssetTemplateGuid);
        }

        private static string WriteDefaultSequenceProviderAssetFile(UIGraphFolderLocation rootFolder, string localFolderPath, string fileName, string textAssetTemplateGuid)
        {
            string defaultSequenceProviderAssetPath = Path.Combine(rootFolder.Directory, localFolderPath, fileName);
            WriteAssetFile(defaultSequenceProviderAssetPath, textAssetTemplateGuid);
            return defaultSequenceProviderAssetPath;
        }

        private static void WriteAssetFile(string assetPath, string textAssetTemplateGuid)
        {
            TextAsset assetTemplate = AssetDatabaseExtensions.LoadAssetOfTypeWithGUID<TextAsset>(textAssetTemplateGuid);
            File.WriteAllText(assetPath, assetTemplate.text);
        }
    }
}