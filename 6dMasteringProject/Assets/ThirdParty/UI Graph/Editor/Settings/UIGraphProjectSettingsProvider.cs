﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UIElements;

namespace Pelican7.UIGraph.Editor
{
    public class UIGraphProjectSettingsProvider : SettingsProvider
    {
        private static readonly HashSet<string> ExcludedPropertyPaths = new HashSet<string>()
        {
            "m_Script"
        };

        private SerializedObject settings;

        public UIGraphProjectSettingsProvider(string path, SettingsScope scope = SettingsScope.User) : base(path, scope) { }

        [SettingsProvider]
        public static SettingsProvider Create()
        {
            var provider = new UIGraphProjectSettingsProvider("Project/UI Graph", SettingsScope.Project);
            return provider;
        }

        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            base.OnActivate(searchContext, rootElement);
            settings = UIGraphProjectSettings.SerializedInstance;
            keywords = GetSearchKeywordsFromSerializedObject(settings);
        }

        public override void OnGUI(string searchContext)
        {
            base.OnGUI(searchContext);
            settings.Update();
            DrawAllPropertyFields(ExcludedPropertyPaths);
            settings.ApplyModifiedProperties();
        }

        private void DrawAllPropertyFields(HashSet<string> excludedPropertyPaths)
        {
            SerializedProperty property = settings.GetIterator();
            property.NextVisible(true);
            do
            {
                if (excludedPropertyPaths.Contains(property.propertyPath) == false)
                {
                    EditorGUILayout.PropertyField(property, true);
                }
            }
            while (property.NextVisible(false));
        }
    }
}