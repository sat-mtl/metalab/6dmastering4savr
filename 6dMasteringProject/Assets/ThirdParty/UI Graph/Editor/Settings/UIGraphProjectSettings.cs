﻿// Copyright © 2019 Pelican 7 LTD. All rights reserved.
// This file is part of the UI Graph asset, which is distributed under the Asset Store Terms of Service and EULA - https://unity3d.com/legal/as_terms.

using UnityEditor;
using UnityEngine;

namespace Pelican7.UIGraph.Editor
{
    public class UIGraphProjectSettings : ScriptableObject
    {
        private static UIGraphProjectSettings instance;

        [Header("UI Canvas")]
        public CreateCanvasControllerTemplate createCanvasControllerTemplate;
        public CreateCanvasControllerInstanceTemplate createNavigationCanvasControllerInstanceTemplate;
        public CreateCanvasControllerInstanceTemplate createSplitCanvasControllerInstanceTemplate;
        public CreateCanvasControllerInstanceTemplate createStackCanvasControllerInstanceTemplate;
        public CreateCanvasControllerInstanceTemplate createTabBarCanvasControllerInstanceTemplate;
        public CanvasWindow createCanvasWindowTemplate;

        [HideInInspector]
        public string version;

        public static UIGraphProjectSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = AssetDatabaseExtensions.LoadFirstAssetOfType<UIGraphProjectSettings>();
                    if ((instance != null) && instance.IsPreviousVersion)
                    {
                        instance.MigrateToLatestVersion();
                    }
                }

                if (instance == null)
                {
                    instance = UIGraphProjectSettingsGenerator.GenerateDefaultProjectSettings();
                }

                return instance;
            }
        }

        public static SerializedObject SerializedInstance
        {
            get
            {
                return new SerializedObject(Instance);
            }
        }

        public static bool InstanceExists
        {
            get
            {
                UIGraphProjectSettings instance = AssetDatabaseExtensions.LoadFirstAssetOfType<UIGraphProjectSettings>();
                return (instance != null);
            }
        }

        private bool IsPreviousVersion
        {
            get
            {
                System.Version currentVersion = UIGraphPackageVersion.Value;
                System.Version settingsVersion = new System.Version(version);
                bool settingsVersionIsEarlierThanCurrentVersion = (settingsVersion.CompareTo(currentVersion) < 0);
                return settingsVersionIsEarlierThanCurrentVersion;
            }
        }

        private void MigrateToLatestVersion()
        {
            // [Developer Note] Future version migrations go here.
            version = UIGraphPackageVersion.StringValue;
            EditorUtility.SetDirty(this);
        }
    }
}