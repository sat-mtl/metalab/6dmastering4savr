/*************************************************************************
*
* Experiences Musique6D
* __________________
*
*  [2019] Experiences Musique6D Incorporated
*  All Rights Reserved.
*
* NOTICE:  All Original Software And Resources developed by Experiences Musique6D contained herein is, and remains
* the property of Experiences Musique6D Incorporated.  The intellectual and technical concepts contained
* herein are proprietary to Experiences Musique6D Incorporated
* and may be covered by U.S. and Canadian Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Experiences Musique6D Incorporated.
*/



using UnityEngine;
using UnityEngine.Events;


[RequireComponent(typeof(AudioSource))]

[System.Serializable] public class envFolEvent : UnityEvent<float> { }
[System.Serializable] public class colorChangeEvent : UnityEvent<Color> { }


/// <summary>
/// Music animator.
/// This class supports the zAudioClipSetter class, and provides for music-driven animation of graphics
/// </summary>
public class musicAnimator : MonoBehaviour
{
    public float efolUpdateRate = 0.05f;
    public float clipAmplitude = 0f;
    public envFolEvent changedAmplitude;
    //public envFolEvent onEnvClip;
    //public envFolEvent onEnvZero;


    private float _lastAmplitude = 0f;

    private float currentUpdateTime = 0f;

    private bool _running = false;

    public colorChangeEvent OnColorChange;


    void Start()
    {
        _running = true;
    }

    void Update()
    {
        // Apply audio anvelope to graphic representation
        currentUpdateTime += Time.deltaTime;
        if (currentUpdateTime >= efolUpdateRate)
        {
            if (clipAmplitude != _lastAmplitude)
            {
                _lastAmplitude = clipAmplitude;
                changedAmplitude.Invoke(clipAmplitude);
            }
            currentUpdateTime = 0f;
        }
    }

    // this listens for a colorChanged event and forwards the color to all listeners
    public void setColor(Color color)
    {
        OnColorChange.Invoke(color);
    }


    void OnAudioFilterRead(float[] data, int channels)
    {
        if (!_running)
            return;

        if (changedAmplitude.GetPersistentEventCount() < 1) return; // do not compute when nobody is listening

        float energy = 0f;

        for (int i = 0; i < data.Length; i++)
        {
            energy += Mathf.Abs(data[i]);
        }
        clipAmplitude = energy / data.Length;
    }
}