/*************************************************************************
*
* Experiences Musique6D
* __________________
*
*  [2019] Experiences Musique6D Incorporated
*  All Rights Reserved.
*
* NOTICE:  All Original Software And Resources developed by Experiences Musique6D contained herein is, and remains
* the property of Experiences Musique6D Incorporated.  The intellectual and technical concepts contained
* herein are proprietary to Experiences Musique6D Incorporated
* and may be covered by U.S. and Canadian Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Experiences Musique6D Incorporated.
*/



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wtmFloorMap : MonoBehaviour {

	//private zARensembleGen zARensembleGenCS = null;
	//public Material myMaterial;


	// Use this for initialization
	void Start () {
		
	}
 
	private void Awake()
	{
		MeshRenderer myFloorMap = GetComponent<MeshRenderer>();

		if (myFloorMap == null)
		{
			Debug.LogError(name + ": " + GetType() + ".Awake()  missing Meshrenderer component, aborting");
			Destroy(this);
			return;
		}
	}
    
    // called by parent ensembleGen object
    public float getRadius()
	{
		float ensembleRadius = Mathf.Max(transform.localScale.x, transform.localScale.z); // where the texture material grid size scale is 10 times that of the local transform's
        ensembleRadius = 5f * ensembleRadius;
		return(ensembleRadius);
	}
}
