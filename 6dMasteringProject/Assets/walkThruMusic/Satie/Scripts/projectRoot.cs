﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[ExecuteInEditMode]
public class projectRoot : MonoBehaviour
{

    [Tooltip("name the subdirectory located in /StreamingAssets/sound where music soundfiles are located")]
	public string musicSoundFileDirName = "beethoven7";
	[Tooltip("fixed name of the subdirectory located in /StreamingAssets/sound where other soundfiles are located")]
	public string otherSoundFileDirName = "other";

	// Start is called before the first frame update
	void Start()
    {
		bool dirExists = Directory.Exists(Application.streamingAssetsPath + "/sound/");
        if (!dirExists)
		{
			Debug.LogError($"{name}:{GetType()}.Start() /StreamingAssets/sound should BUT does NOT exist, creating it now");
			Directory.CreateDirectory(Application.streamingAssetsPath + "/sound/");
		}

		dirExists = Directory.Exists(Application.streamingAssetsPath + "/sound/" + musicSoundFileDirName);
		if (!dirExists)
		{
			Debug.LogError($"{name}:{GetType()}.Start() /StreamingAssets/sound/{musicSoundFileDirName} should BUT does NOT exist, creating it now");
			Directory.CreateDirectory(Application.streamingAssetsPath + "/sound/"+musicSoundFileDirName);
		}

		dirExists = Directory.Exists(Application.streamingAssetsPath + "/sound/" + otherSoundFileDirName);
		if (!dirExists)
		{
			Debug.LogError($"{name}:{GetType()}.Start() /StreamingAssets/sound/{otherSoundFileDirName} should BUT does NOT exist, creating it now");
			Directory.CreateDirectory(Application.streamingAssetsPath + "/sound/" + otherSoundFileDirName);
		}

	}

    void OnValidate()
	{
		otherSoundFileDirName = "other";
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
