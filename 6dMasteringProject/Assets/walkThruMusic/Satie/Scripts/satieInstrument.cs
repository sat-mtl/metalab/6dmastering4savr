﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SATIE;

namespace satieDemo
{
    public class satieInstrument : MonoBehaviour
    {
        [SerializeField]
        [Sirenix.OdinInspector.Required]
        List<GameObject> graphicGameObjs;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        SATIEsource audioNode;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        TextMesh instrumentLabel;

        [SerializeField]
        private bool showLabel = true;

        private float initialMaxDist;
        private float initialDoppler;
        private float initialNearFieldRadius;
        private float initiaTrimDB;


        private SATIEsource src;

        private void Awake()
        {
            
        }


        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(afterStartCR());  // make sure all instrument state has loaded before setting up state
        }

        public IEnumerator afterStartCR()
        {
            yield return new WaitForFixedUpdate();

            if (instrumentLabel)
            {
                instrumentLabel.text = audioNode.nodeName;
                instrumentLabel.transform.gameObject.SetActive(showLabel);
            }
            src = GetComponentInChildren<SATIEsource>();
            if (!src)
            {
                Debug.LogError($"{name}.{GetType()}.StartCoroutine(): no SATIEsource found in children, aborting");
                Destroy(this);
            }


            initialMaxDist = src.unityAudioSource.maxDistance;
            initialDoppler = src.unityAudioSource.dopplerLevel;
            initialNearFieldRadius = src.nearFieldRadius;
            initiaTrimDB = src.gainTrimDB;
            yield return null;
        }

        #region hooks

        #region hooks

        public void OffsetMaxDistance(float offset)
        {
            src.unityAudioSource.maxDistance = Mathf.Clamp(offset + initialMaxDist, 0f, 99999f);
            src.evalConnections(true);
        }

        public void OffsetDoppler(float offset)
        {
            src.unityAudioSource.dopplerLevel = Mathf.Clamp(offset + initialDoppler, 0f, 5f);
            src.evalConnections(true);
        }

        public void OffsetNearFieldRadius(float offset)
        {
            src.nearFieldRadius = Mathf.Clamp(offset + initialNearFieldRadius, 0.1f, 6f);
            src.evalConnections(true);
        }

        public void OffsetTrimDB(float offsetDB)
        {
            src.setGainTrimDB(Mathf.Clamp(offsetDB + initiaTrimDB, -42, 24f));
            //src.evalConnections(true);
        }

        public void ShowLabel(bool state)
        {
            instrumentLabel.gameObject.SetActive(state);
        }

        #endregion

        #endregion
    }
}