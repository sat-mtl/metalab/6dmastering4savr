﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;
using UnityEngine.UI;
using TMPro;
using System;

namespace satieDemo
{
    public class satieSettingsUIhelper : MonoBehaviour
    {

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Toggle showLabelsToggle;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Slider playStartOffsetSlider;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private TextMeshProUGUI playOffsetValueTxt;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Button TriggerPlaybackBTN;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Slider dopplerOffsetSlider;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private TextMeshProUGUI dopplerScaleValueTxt;


        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Slider nearFieldRadiusOffsetSlider;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private TextMeshProUGUI nearFieldRadiusOffsetValueTxt;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Slider sourceRangeOffsetSlider;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private TextMeshProUGUI sourceRangeOffsetValueTxt;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Slider acousticsGainDBoffsetSlider;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private TextMeshProUGUI acousticsGainDBoffsetValueTxt;


        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Toggle sourcesMutingToggle;

        [SerializeField]
        [Sirenix.OdinInspector.Required]
        private Toggle acousticsMutingToggle;


        // Start is called before the first frame update
        void Start()
        {
            showLabelsToggle.onValueChanged.AddListener(OnShowInstrumentLabels);
            playStartOffsetSlider.onValueChanged.AddListener(OnPlayStartOffset);
            dopplerOffsetSlider.onValueChanged.AddListener(OnOffsetDopplerEffect);
            sourceRangeOffsetSlider.onValueChanged.AddListener(OnOffsetSourceRange);
            nearFieldRadiusOffsetSlider.onValueChanged.AddListener(OnOffsetNearFieldRadius);
            acousticsGainDBoffsetSlider.onValueChanged.AddListener(OnOffsetAcousticsGainDB);
            sourcesMutingToggle.onValueChanged.AddListener(OnSourcesMutingToggleChanhed);
            acousticsMutingToggle.onValueChanged.AddListener(OnAcousticsMutingToggleChanhed);
            TriggerPlaybackBTN.onClick.AddListener(OnTriggerPlayback);
        }


        private void OnDestroy()
        {
            showLabelsToggle.onValueChanged.RemoveListener(OnShowInstrumentLabels);
            playStartOffsetSlider.onValueChanged.RemoveListener(OnPlayStartOffset);
            dopplerOffsetSlider.onValueChanged.RemoveListener(OnOffsetDopplerEffect);
            sourceRangeOffsetSlider.onValueChanged.RemoveListener(OnOffsetSourceRange);
            acousticsGainDBoffsetSlider.onValueChanged.RemoveListener(OnOffsetAcousticsGainDB);
            sourcesMutingToggle.onValueChanged.RemoveListener(OnSourcesMutingToggleChanhed);
            acousticsMutingToggle.onValueChanged.RemoveListener(OnAcousticsMutingToggleChanhed);
        }



        private void OnEnable()
        {
            refreshUI();
        }

        // Update is called once per frame
        void Update()
        {

        }

        #region UI hooks

        private void OnTriggerPlayback()
        {
            if (!satieSceneControl.Instance) return;
            satieSceneControl.Instance.triggerGroup();
        }

        private void OnSourcesMutingToggleChanhed(bool state)
        {
            if (!satieSceneControl.Instance) return;
            satieSceneControl.Instance.muteSources(state);
        }

        private void OnAcousticsMutingToggleChanhed(bool state)
        {
            if (!satieSceneControl.Instance) return;
            satieSceneControl.Instance.muteAcoustics(state);
        }

        public void OnShowInstrumentLabels(bool state)
        {
            if (!satieSceneControl.Instance) return;
            //StateManager.EventBus.Dispatch(new SatieShowInstrumentLabelsEvent { showInstrumentLabels = state });
            satieSceneControl.Instance.ShowLabels(state);
        }

        // expects a value in seconds
        public void OnPlayStartOffset(float offset)
        {
            if (!satieSceneControl.Instance) return;

            satieSceneControl.Instance.playStartOffset( offset);

            TimeSpan t = TimeSpan.FromSeconds(offset);

            string timeSecs = string.Format("{0:D2}m:{1:D2}s", t.Minutes, t.Seconds);

            playOffsetValueTxt.SetText(timeSecs);
        }

        // expects a value between -3 and +3 
        public void OnOffsetDopplerEffect(float offset)
        {
            if (!satieSceneControl.Instance) return;

            satieSceneControl.Instance.OffsetDoppler(offset);
            dopplerScaleValueTxt.SetText(offset.ToString("F3"));
        }

        // expects values from -100 t0 100 meters
        public void OnOffsetSourceRange(float offset)
        {
            if (!satieSceneControl.Instance) return;
            satieSceneControl.Instance.OffsetSourceRange(offset);
            sourceRangeOffsetValueTxt.SetText(offset.ToString("F2") + "m");
        }
        // expects values from -6 to +6 meters
        public void OnOffsetNearFieldRadius(float offset)
        {
            if (!satieSceneControl.Instance) return;
            satieSceneControl.Instance.OffsetNearFieldRadius(offset);
            nearFieldRadiusOffsetValueTxt.SetText(offset.ToString("F2") + "m");
        }

        // expects values from -6 to +6 meters
        public void OnOffsetAcousticsGainDB(float offset)
        {
            if (!satieSceneControl.Instance) return;
            satieSceneControl.Instance.OffsetAcousticGainDB(offset);
            acousticsGainDBoffsetValueTxt.SetText(offset.ToString("F2") + "db");
        }

        #endregion

        #region functions


        void refreshUI()
        {
            if (!satieSceneControl.Instance) return;
            float lastValue;

            // doppler offsetting
            lastValue = satieSceneControl.Instance.currentDopplerOffset;
            dopplerScaleValueTxt.SetText(lastValue.ToString("F3"));
            dopplerOffsetSlider.SetValueWithoutNotify(lastValue);

            // source range offset
            lastValue = satieSceneControl.Instance.currentSourceOffsetScaler;
            sourceRangeOffsetValueTxt.SetText(lastValue.ToString("F2")+"m");
            sourceRangeOffsetSlider.SetValueWithoutNotify(lastValue);

            // nearField radius offset
            lastValue = satieSceneControl.Instance.currentNearFieldRadiusOffset;
            nearFieldRadiusOffsetValueTxt.SetText(lastValue.ToString("F2") + "m");
            nearFieldRadiusOffsetSlider.SetValueWithoutNotify(lastValue);

            // Acoustics GainDB TRIM 
            lastValue = satieSceneControl.Instance.currentAcousticGainDBoffset;
            acousticsGainDBoffsetValueTxt.SetText(lastValue.ToString("F2") + "m");
            acousticsGainDBoffsetSlider.SetValueWithoutNotify(lastValue);

            bool lastState = satieSceneControl.Instance.currentAcousticMute;
            acousticsMutingToggle.SetIsOnWithoutNotify(lastState);

            lastState = satieSceneControl.Instance.currentSourcesMute;
            sourcesMutingToggle.SetIsOnWithoutNotify(lastState);


            lastValue = satieSceneControl.Instance.currentStartOffsetSecs;
            playStartOffsetSlider.SetValueWithoutNotify(lastValue);
            TimeSpan t = TimeSpan.FromSeconds(lastValue);
            string timeSecs = string.Format("{0:D2}m:{1:D2}s", t.Minutes, t.Seconds);
            playOffsetValueTxt.SetText(timeSecs);

            // source range scaler
            //lastValue = satieSceneControl.Instance.currentSourceRangeScaler;
            //sourceRangeOffsetValueTxt.SetText(lastValue.ToString("F3"));
            //lastValue = exp2lin(lastValue);  // make linear between 0 and 1
            //sourceRangeOffsetSlider.SetValueWithoutNotify(lastValue);


        }

        float lin2exp(float inVal)
        {
            return Mathf.Pow(inVal * 2, 3f);
        }
        float exp2lin(float inVal)
        {
            return 0.5f * Mathf.Pow(inVal, 0.333f);
        }

        #endregion
    }
}