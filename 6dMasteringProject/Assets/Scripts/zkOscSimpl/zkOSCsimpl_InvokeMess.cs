/*
* UniOSC
* Copyright © 2014-2015 Stefan Schlupek
* All rights reserved
* info@monoflow.org
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//using OSCsharp.Data;



// strips off last item in OSC address path, and calls corresponding method by name for all scripts of game object and children
// note,  this object can only handle at most, one datum in the received OSC message


namespace OscSimpl.Examples{

    /// <summary>
    /// Moves a GameObject in normalized coordinates (ScreenToWorldPoint)
    /// </summary>
    // [AddComponentMenu("UniOSC/MoveGameObject")]
    public class zkOSCsimpl_InvokeMess :  MonoBehaviour {

        private string _oscMatchAddr = "";
        private bool _start = false;
        public bool alsoSendToChildren = false;

        private bool wildCardMode = false;

        public bool debug = false;


        public string OSCaddress = "";      // automatically creates an OSC address using the object's name, making:  "/objName/position"

        [Tooltip("make sure that the OscIn objects in the environment have a corresponding tag")]

        public string tagName = "default";

        public OscIn oscIn;
         


        void Start()
        {
            if (tagName.Length == 0)
                tagName = "default";
            
            if (OSCaddress.Length==0)
            {
                Debug.LogWarning(transform.name + " : " + GetType() + " : " + "Start(): no OSC address provided, using nodeName");
                OSCaddress = "/" + name + "/*";
            }

            _oscMatchAddr = OSCaddress = OSCaddress.Replace(" ", String.Empty);  // flush out any white spaces

            if (oscIn != null)   // already chosen manually
                return;

            // look for the OscIn object(s) among children of gameroot using tag


            if (zk_transformTag.zk_transformTagList.Count == 0)
            {
                Debug.LogError(transform.name + " : " + GetType() + " : " + "Start(): can't find any objects with with both a 'zk_transformTag' compenent and an oscIn component in the transform, aborting");
                Destroy(this);
                return;
            }

            //zk_transformTagsCS = ;

            zk_transformTag oscInTagCS = null;

            foreach (zk_transformTag tag in zk_transformTag.zk_transformTagList)
            {
                oscInTagCS = tag;
                if (tag.tagName.Equals(tagName))
                {
                    break;   // all good... object found on transform with corresponding tag                  
                }
                //Debug.Log(transform.name + " : " + GetType() + " : " + "Matching on tag:"+tagName);
            }


            oscIn = oscInTagCS.gameObject.GetComponent<OscIn>();

            if (oscIn == null)
            {
                Debug.LogError(transform.name + " : " + GetType() + " : " + "Start(): can't find locate OscIn object(s) in the transform of" + oscInTagCS.gameObject.name + ", aborting");
                Destroy(this);
                return;
            }

            if (!oscInTagCS.tagName.Equals(tagName))
                Debug.LogWarning(transform.name + " : " + GetType() + " : " + "Start():can't locate oscIn component with zk_transformTag: " + tagName + " in transform.  Using OscIn component associated with tag: " + oscInTagCS.tag + " in transform of object: " + oscInTagCS.gameObject.name);



			checkOSCprependedPath ();
			_start = true;
            OnEnable();   // this can not be called until START has set up the state
        }

       void OnValidate()
        {
            if (!_start)
                return;
			if (OSCaddress != _oscMatchAddr)
                checkOSCprependedPath();
         }


        public void OnEnable()
        {
            if (!_start == true)
                return;

            //Debug.Log(transform.name + " : " + GetType() + " : " + "OnEnable()");
            if (oscIn != null)                   
				oscIn.onAnyMessage.AddListener( OnOSCMessage );  // Subscribe to all OSC messages
        }


        void OnDisable()
        {
            // Unsubscribe from messsages
            if (oscIn != null)                   
                oscIn.onAnyMessage.RemoveListener( OnOSCMessage );   // unsubsccribe
        }

//		void Update()
//        {
//        }
//
		void checkOSCprependedPath()
		{
			if ( !OSCaddress.Contains("/") || !OSCaddress.StartsWith("/")  )               
			{
				Debug.LogError(string.Format("{0}.Awake():  OSC path format error: {1}, aborting", GetType(), OSCaddress), transform);
				_oscMatchAddr = "";
				return;
			}

            _oscMatchAddr = OSCaddress = OSCaddress.Replace(" ", String.Empty);  // flush out any white spaces

            wildCardMode = false;

            if (OSCaddress.EndsWith("/*"))    // all good
            {
                wildCardMode = true;
                _oscMatchAddr = OSCaddress.TrimEnd(new char[] { '/', '*' });
            }
			else if (OSCaddress.EndsWith("/"))
			{
 				_oscMatchAddr = OSCaddress.TrimEnd(new char[] { '/' });
                OSCaddress = _oscMatchAddr;
				//OSCaddress += "*";
			}
			else
			{
				_oscMatchAddr = OSCaddress;
			}
		}

		public void OnOSCMessage(OscMessage msg )
        {
            string address = msg.address;
            int matchLen = _oscMatchAddr.Length;

            string hookName = "nothing";
            string sval;
            float fval;
            int ival;
  
            if (debug)
            {
                Debug.Log(transform.name + ": " + GetType() + "OnOSCMessage()  MESS: " + address + "   arg count: " + msg.args.Count);
            }

             string[] keys = address.Split('/');
//            foreach (string word in keys)
//            {
//                // Debug.Log("ADDRESS ITEM: " + word);
//            }

            if (keys.Length < 2)  // meaning, if it's just  "/" ,  bail
            {
                Debug.LogWarning(transform.name + ": "+ GetType() + ".OnOSCMessage():  ignoring empty address");
                return;
            }

            if (address.Length < matchLen) // message address can not match..  too short
            {
                if (debug)
                {
                    Debug.Log(transform.name + ": " + GetType() + ".OnOSCMessage(): message address not matched, ignoring message: " + address);
                }
                return;
            }

            //Debug.Log("_oscMatchAddr : " + _oscMatchAddr + " matchingAddress: " + address.Substring(0, matchLen));
            //Debug.Log("_oscMatchAddr.Len : " + _oscMatchAddr.Length + " matchingAddress: " + address.Substring(0, matchLen).Length);


            if (_oscMatchAddr == address.Substring(0, matchLen))  // YES,  the message contains at least the match 
            {

                if (debug)
                    Debug.Log(transform.name + ": " + GetType() + ".OnOSCMessage(): :address match: " + address.Substring(0, matchLen));


                if (wildCardMode)  // we are expecting additional keys in the received message
                {

                    if (_oscMatchAddr.Length == address.Length)  // no extra keys in the received address,  no good for wildcard mode
                    {
                        Debug.LogWarning(transform.name + ": " + GetType() + ".OnOSCMessage(): address lacking additional wild card key, ignoring message: " + address);
                        return;
                    }

                    // else chack the rest of the received address for more keys
                    hookName = address.Substring(matchLen + 1);
                    keys = hookName.Split('/');

                    //Debug.Log("KEYS LENGTH= " + keys.Length);
                    foreach (string word in keys)
                    {
                        //Debug.Log("KEY ITEM: " + word);
                    }
                     
                    if (keys.Length == 1 && keys[0].Length != 0)
                    {
                        hookName = keys[0];

                        //Debug.Log("\t\t hookName: " + hookName + "  hookName Length = :" + hookName.Length);
                    }
                    else
                    {
                        Debug.LogWarning(transform.name + ": " + GetType() + ".OnOSCMessage(): wild portion of address must contain ONE key, ignoring message: " + address);
                        return;
                    }
                }
                else  // not looking for wild card, use last key in message
                {
                    if (_oscMatchAddr.Length == address.Length)
                    {
                        keys = address.Split('/');
                        hookName = keys[keys.Length - 1];
                        Debug.Log("\t\t hookName: " + hookName + "  hookName Length = :" + hookName.Length);
                    }
                    else
                    {
                        if (debug)
                        {
                            Debug.Log(transform.name + ": " + GetType() + ".OnOSCMessage(): received address: " + address + " does not match: " + _oscMatchAddr);
                        }

                        return; // message does not correspond to the saught message
                    }

                    
                    //Debug.Log("\t\t hookName: " + hookName);
                }
            }
            else  // message not matched
            {
                if (debug) Debug.Log(transform.name + ": " + GetType() + ".OnOSCMessage(): received address: " + address + " does not match: " + _oscMatchAddr);
                return;
            }


            // address is good but more than one data in message... bail
            if (msg.args.Count > 1)
            {
                Debug.LogWarning(transform.name + ": " + GetType() + ".OnOSCMessage():  can only have maximum of one datum in message, aborting ");
                return;
            }

 
//            if (_oscMatchAddr == address.Substring(0, matchLen))
//            {
//
//                if (debug) Debug.Log("\t\taddress match: " + address.Substring(0, matchLen));
//                hookName = address.Substring(matchLen + 1);
//            }
//            else
//                return;
//            
//            // address matched, using remainder of address as method name

            if (debug)  Debug.Log("\t\tMESSAGE MATCHED: " +      _oscMatchAddr + " for method: "  + hookName);
                   // send message to all scripts on this gameobject, and its children
           // gameObject.BroadcastMessage(hookName, (float)msg.Data[0], SendMessageOptions.DontRequireReceiver);  

  
			// OSC address only, no data to include in message
			if (msg.args.Count == 0)
            {

                gameObject.SendMessage(hookName, null, SendMessageOptions.DontRequireReceiver);  
                if (alsoSendToChildren)
                {
                    Transform[] transforms = gameObject.GetComponentsInChildren<Transform>();
                    foreach (Transform t in transforms)
                    {
                        t.SendMessage(hookName, null, SendMessageOptions.DontRequireReceiver);  
                    }
                }

                return;
            }


			// is the datum is a string
			if( msg.TryGet( 0, out sval ) ) 
				{
					gameObject.SendMessage(hookName, sval, SendMessageOptions.DontRequireReceiver);  

					if (alsoSendToChildren)
					{
						Transform[] transforms = gameObject.GetComponentsInChildren<Transform>();
						foreach (Transform t in transforms)
						{
							t.SendMessage(hookName, sval, SendMessageOptions.DontRequireReceiver);  
						}
					}
					return;
				}


			// is the datum is a float
			if( msg.TryGet( 0, out fval )) // is it a string
			{
				gameObject.SendMessage(hookName, fval, SendMessageOptions.DontRequireReceiver);  

				if (alsoSendToChildren)
				{
					Transform[] transforms = gameObject.GetComponentsInChildren<Transform>();
					foreach (Transform t in transforms)
					{
						t.SendMessage(hookName, fval, SendMessageOptions.DontRequireReceiver);  
					}
				}
				return;
			}


			// is the datum is an int, cast as float
			if( msg.TryGet( 0, out ival )) // is it a string
			{
				gameObject.SendMessage(hookName, (float) ival, SendMessageOptions.DontRequireReceiver);  

				if (alsoSendToChildren)
				{
					Transform[] transforms = gameObject.GetComponentsInChildren<Transform>();
					foreach (Transform t in transforms)
					{
						t.SendMessage(hookName, (float) ival, SendMessageOptions.DontRequireReceiver);  
					}
				}
				return;
			}


		}
    }
}