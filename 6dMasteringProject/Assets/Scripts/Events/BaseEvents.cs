using System;
using Sigtrap.Relays;
using UnityEngine;
using UnityEngine.Events;

namespace Oz.Events
{
    [Serializable]
    public class BaseEvent : Relay
    {
    }

    [Serializable]
    public class BaseEvent<T> : Relay<T>
    {
    }
}