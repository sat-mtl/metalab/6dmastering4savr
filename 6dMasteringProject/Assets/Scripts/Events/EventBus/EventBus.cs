﻿/**
 * Copyright 2017 Ben D'Angelo
 * MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Oz.Common;

namespace Oz.Events
{
    /// <summary>
    /// Base class for all game events. All possible events should derive from this base class in order for the EventBus to be able to process them.
    /// </summary>
    public interface IGameEvent { }

    /// <summary>
    /// 
    /// </summary>
    /// <example>
    /// Usage example:
    /// <code>
    /// class PlayerDamagedEvent : GameEvent
    /// {
    ///     public int DamageValue = 0;
    /// }
    ///
    /// public class PlayerDamageHandler : MonoBehaviour
    /// {
    ///     void OnEnable
    ///     {
    ///         // Will dispatch only once    
    ///         GameManager.Instance.EventBus.AddOnce&lt;PlayerDamagedEvent&gt;(OnPlayerDamaged);
    ///     }
    ///     
    ///     void OnDisable
    ///     {
    ///         GameManager.Instance.EventBus.RemoveListener&lt;PlayerDamagedEvent&gt;(OnPlayerDamaged);
    ///     }
    ///
    ///     void Start()
    ///     {
    ///         // Will execute immediately
    ///         GameManager.Instance.EventBus.Dispatch(new PlayerDamagedEvent { DamageValue = 10 });
    ///
    ///         // Will execute on next frame
    ///         GameManager.Instance.EventBus.DispatchQueued(new PlayerDamagedEvent { DamageValue = 10 });
    ///
    ///         // Check if we have listeners for a given delegate in the event EventBus
    ///         bool hasListeners = GameManage.Instance.EventBus.HasListener&lt;PlayerDamangedEvent&gt;(OnPlayerDamaged);
    ///     }
    ///
    ///     void OnPlayerDamaged(PlayerDamagedEvent e)
    ///     {
    ///         Debug.Log(e.DamageValue);
    ///     }
    /// }
    /// </code>
    /// </example>
    public class EventBus : IDisposable
    {
        /// <summary>
        /// Should the processing of the event queue be offloaded to future frames based on the value in the amount of seconds in `<see cref="QueueProcessTime" />`.
        /// </summary>
        public bool LimitQueueProcesing = false;

        /// <summary>
        /// Maximum amount of time events can be processed in a single frame-time.
        /// This is useful to offload some of the processing to other frames if we have
        /// too many messages going on at once.
        /// 
        /// This needs to be turned on using `<see cref="LimitQueueProcesing" />`.
        /// </summary>
        public float QueueProcessTime = 0.0f;

        private readonly Queue _eventQueue = new Queue();

        public delegate void EventDelegate<T>(T evt) where T : IGameEvent;
        private delegate void EventDelegate(IGameEvent evt);

        private Dictionary<System.Type, EventDelegate> delegates = new Dictionary<System.Type, EventDelegate>();
        private Dictionary<System.Delegate, EventDelegate> delegateLookup = new Dictionary<System.Delegate, EventDelegate>();
        private Dictionary<System.Delegate, System.Delegate> onceLookups = new Dictionary<System.Delegate, System.Delegate>();

        private EventDelegate AddDelegate<T>(EventDelegate<T> del) where T : IGameEvent
        {
            // Early-out if we've already registered this delegate
            if (delegateLookup.ContainsKey(del))
            {
                return null;
            }

            // Create a new non-generic delegate which calls our generic one.
            // This is the delegate we actually invoke.
            EventDelegate internalDelegate = (evt) => del((T)evt);
            delegateLookup[del] = internalDelegate;

            if (delegates.TryGetValue(typeof(T), out EventDelegate tempDel))
            {
                delegates[typeof(T)] = tempDel += internalDelegate;
            }
            else
            {
                delegates[typeof(T)] = internalDelegate;
            }

            return internalDelegate;
        }

        /// <summary>
        /// Add an event listener.
        /// See `<see cref="EventBus" />` for usage example in code.
        /// </summary>
        /// <param name="del">Delegate to call when event is dispatched.</param>
        /// <typeparam name="T">The event object type used as a key to lookup listeners.</typeparam>
        public void AddListener<T>(EventDelegate<T> del) where T : IGameEvent
        {
            AddDelegate<T>(del);
        }

        /// <summary>
        /// Add an event listener once.
        /// See `<see cref="EventBus" />` for usage example in code.
        /// </summary>
        /// <param name="del">Delegate to call when event is dispatched.</param>
        /// <typeparam name="T">The event object type used as a key to lookup listeners.</typeparam>
        public void AddOnce<T>(EventDelegate<T> del) where T : IGameEvent
        {
            EventDelegate result = AddDelegate<T>(del);

            if (result != null)
            {
                // Remember this is only called once
                onceLookups[result] = del;
            }
        }

        /// <summary>
        /// Remove an event listener.
        /// See `<see cref="EventBus" />` for usage example in code.
        /// </summary>
        /// <param name="del">Delegate to call when event is dispatched.</param>
        /// <typeparam name="T">The event object type used as a key to lookup listeners.</typeparam>
        public void RemoveListener<T>(EventDelegate<T> del) where T : IGameEvent
        {
            if (delegateLookup.TryGetValue(del, out EventDelegate internalDelegate))
            {
                if (delegates.TryGetValue(typeof(T), out EventDelegate tempDel))
                {
                    tempDel -= internalDelegate;
                    if (tempDel == null)
                    {
                        delegates.Remove(typeof(T));
                    }
                    else
                    {
                        delegates[typeof(T)] = tempDel;
                    }
                }

                delegateLookup.Remove(del);
            }
        }

        /// <summary>
        /// Remove all event listeners.
        /// </summary>
        public void ClearAllListeners()
        {
            delegates.Clear();
            delegateLookup.Clear();
            onceLookups.Clear();
        }

        /// <summary>
        /// Check if a specific event-delegate pair has at least one listener.
        /// See `<see cref="EventBus" />` for usage example in code.
        /// </summary>
        /// <returns>`<c>true</c>`, if listener was found, `<c>false</c>` otherwise.</returns>
        /// <param name="del">Delegate to look for.</param>
        /// <typeparam name="T">The event object type used as a key to lookup listeners.</typeparam>
        public bool HasListener<T>(EventDelegate<T> del) where T : IGameEvent
        {
            return delegateLookup.ContainsKey(del);
        }

        /// <summary>
        /// Triggers the event immediately (same frame).
        /// See `<see cref="EventBus" />` for usage example in code.
        /// </summary>
        /// <param name="evt">The game event arguments.</param>
        public void Dispatch(IGameEvent evt)
        {
            Type evtType = evt.GetType();
            if (delegates.TryGetValue(evtType, out EventDelegate del))
            {
               // Debug.Log($"[EventBus] Event: <Color=Green><b>{evtType}</b></Color> dispatch called.");
                del.Invoke(evt);

                // Remove listeners which should only be called once
                foreach (EventDelegate k in delegates[evtType].GetInvocationList())
                {
                    if (onceLookups.ContainsKey(k))
                    {
                        delegates[evtType] -= k;

                        if (delegates[evtType] == null)
                        {
                            delegates.Remove(evtType);
                        }

                        delegateLookup.Remove(onceLookups[k]);
                        onceLookups.Remove(k);
                    }
                }
            }
#if UNITY_EDITOR
            else
            {
                Debug.LogWarning($"[EventBus] Event: <Color=Yellow><b>{evtType}</b></Color> has no listeners");
            }
#endif
        }

        /// <summary>
        /// Inserts the event into the current queue (will be processed at the next frame).
        /// See `<see cref="EventBus" />` for usage example in code.
        /// </summary>
        /// <returns><c>true</c>, if event was queued, <c>false</c> otherwise.</returns>
        /// <param name="evt">The game event arguments.</param>
        public bool DispatchQueued(IGameEvent evt)
        {
            if (!delegates.ContainsKey(evt.GetType()))
            {
#if UNITY_EDITOR
                Debug.LogWarning($"[EventBus] DispatchQueued failed due to no listeners for event: <Color=Yellow><b>{evt.GetType()}</b></Color>");
#endif
                return false;
            }

            _eventQueue.Enqueue(evt);
            return true;
        }

        /// <summary>
        /// Every update cycle the queue is processed, if the queue processing is limited,
        /// a maximum processing time per update can be set after which the events will have
        /// to be processed next update loop.
        /// </summary>
        public void Update()
        {
            float timer = 0.0f;
            while (_eventQueue.Count > 0)
            {
                if (LimitQueueProcesing)
                {
                    if (timer > QueueProcessTime)
                    {
                        return;
                    }
                }

                IGameEvent evt = _eventQueue.Dequeue() as IGameEvent;
                Dispatch(evt);

                if (LimitQueueProcesing)
                {
                    timer += Time.deltaTime;
                }
            }
        }

        /// <summary>
        /// Remove all listeners and clear all events in the queue.
        /// </summary>
        public void Dispose()
        {
            ClearAllListeners();
            _eventQueue.Clear();
        }
    }
}