﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;
using SATIE;
using System;

namespace satieDemo
{
    public class satieSceneControl : MonoBehaviour
    {


        public static satieSceneControl Instance;

        [Sirenix.OdinInspector.Required]
        [Tooltip("This is the parent node of the instruments")]
        public GameObject ensembleParent;



        [Sirenix.OdinInspector.Required]
        [Tooltip("This is the parent node of all room acoustics sources")]
        public GameObject roomAcousticsParent;

        [Sirenix.OdinInspector.Required]
        [Tooltip("This is the SATIEgroup component that controls the instruments of the ensemble")]
        public SATIEgroup satieGroup;

        [HideInInspector]
        public float currentSourceOffsetScaler = 0f;

        [HideInInspector]
        public bool currentShowLableState = false;

        [HideInInspector]
        public float currentDopplerOffset = 0f;


        [HideInInspector]
        public float currentNearFieldRadiusOffset = 0f;

        [HideInInspector]
        public float currentAcousticGainDBoffset = 0f;

        [HideInInspector]
        public bool currentSourcesMute = false;

        [HideInInspector]
        public bool currentAcousticMute = false;

        [HideInInspector]
        public float currentStartOffsetSecs = 0;


        private satieInstrument[] instruments;
        private satieInstrument[] acousticSources;



        // Start is called before the first frame update
        void Awake()
        {
            Instance = this;
            currentDopplerOffset = 0f;
            currentStartOffsetSecs = currentSourceOffsetScaler = currentNearFieldRadiusOffset = currentAcousticGainDBoffset = 0f;
            currentSourcesMute = currentAcousticMute = false;

        }

        // Start is called before the first frame update
        void Start()
        {
            instruments = ensembleParent.GetComponentsInChildren<satieInstrument>();
            acousticSources = roomAcousticsParent.GetComponentsInChildren<satieInstrument>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        #region hooks


        public void setGroupProperty(string paramName, float value)
        {
            satieGroup.setProperty(paramName, value);
        }

        public void ShowLabels(bool state)
        {
            currentShowLableState = state;
            foreach (satieInstrument instrument in instruments)
            {
                instrument.ShowLabel(state);
            }
        }



        // expects a value in seconds
        public void playStartOffset(float offset)
        {
            currentStartOffsetSecs = offset;
            satieGroup.setProperty("startPosSecs", offset);
        }

        public void muteSources(bool state)
        {
            currentSourcesMute = state;
            foreach (satieInstrument instrument in instruments)
            {
                SATIEsource src = instrument.GetComponentInChildren<SATIEsource>();
                src.setMute(state);
            }
        }

        public void muteAcoustics(bool state)
        {
            currentAcousticMute = state;
            foreach (satieInstrument instrument in acousticSources)
            {
                SATIEsource src = instrument.GetComponentInChildren<SATIEsource>();
                src.setMute(state);
            }
        }

        public void OffsetDoppler(float offset)
        {
            currentDopplerOffset = offset;
            foreach (satieInstrument instrument in instruments)
            {
                instrument.OffsetDoppler(offset);
            }
        }

        internal void triggerGroup()
        {
            satieGroup.setProperty("t_trig", Time.unscaledTime);
        }

        public void OffsetNearFieldRadius(float offset)
        {
            currentNearFieldRadiusOffset = offset;
            foreach (satieInstrument instrument in instruments)
            {
                instrument.OffsetNearFieldRadius(offset);
            }
        }

        public void OffsetAcousticGainDB(float offset)
        {
            currentAcousticGainDBoffset = offset;
            foreach (satieInstrument instrument in acousticSources)
            {
                instrument.OffsetTrimDB(offset);
            }
        }

        public void OffsetSourceRange(float offset)
        {
            currentSourceOffsetScaler = offset;
            foreach (satieInstrument instrument in instruments)
            {
                instrument.OffsetMaxDistance(offset);
            }
        }

        #endregion
    }
}
