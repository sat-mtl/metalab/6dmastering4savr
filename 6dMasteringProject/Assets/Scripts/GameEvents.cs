using UnityEngine;
using Oz.Events;

using System.Collections.Generic;
using UnityEngine.UI;

namespace GameEvents
{

    public class StateManager : MonoBehaviour
    {
        //----------------------------------------------------------------------
        // Properties
        //----------------------------------------------------------------------

        /// <summary>
        /// Singleton
        /// </summary>
        private static StateManager instance;

        public static StateManager Instance
        {
            get { return instance; }
            private set { instance = value; }
        }


        //----------------------------------------------------------------------
        // Private Properties
        //----------------------------------------------------------------------

#pragma warning disable 0649

        /// <summary>
        /// Event Bus
        /// </summary>
        /// <returns></returns>
        private static readonly EventBus eventBus = new EventBus();

        public static EventBus EventBus
        {
            get { return eventBus; }
        }
    }
        //-------------------------------------------------------------------------------
        // Show Control Events
        //-------------------------------------------------------------------------------

        #region Show Control Events

        /// <summary>
        /// Show control particle force grabber end range received event.
        /// endRange  value from 0 to 1
        /// </summary>
        public class ShowControlParticleForceGrabberEndRangeReceivedEvent : IGameEvent
    {
        public float endRange;
    }

    /// <summary>
    /// Show control time scale changed event.
    /// timeScale value from 0 to 1
    /// </summary>
    public class ShowControlTimeScaleChangedEvent : IGameEvent
    {
        public float timeScale;
    }

    public class PrefabSpawnRequestedEvent : IGameEvent
    {
        public int playerViewId;
        public string spawnGroupName;
    }


    public class GameOverEvent : IGameEvent
    {
        public bool isOver;
    }

    public class ShowControlSkySphereAlphaChangedEvent : IGameEvent
    {
        // 0 to 1 normalized value
        public float alpha;
    }

    #endregion

    //-------------------------------------------------------------------------------
    // WTM UI Events
    //-------------------------------------------------------------------------------

    #region WTM UI Events

    public class WtmUI_HallVisibleEvent : IGameEvent
    {
        public bool hallVisible;
    }

    public class WtmUI_InstrumentsVisibleEvent : IGameEvent
    {
        public bool instrumentsVisible;
    }

    public class WtmUI_NavigationVisibleEvent : IGameEvent
    {
        public bool navigationVisible;
    }

    #endregion


    //-------------------------------------------------------------------------------
    // Satie Events
    //-------------------------------------------------------------------------------


    #region Satie UI Events

    public class SatieShowInstrumentLabelsEvent : IGameEvent
    {
        public bool showInstrumentLabels;
    }

    public class SatieScaleDopplerEvent : IGameEvent
    {
        public float dopplerScaler;
    }

    public class SatieScaleSourceRangeEvent : IGameEvent
    {
        public float sourceRangeScaler;
    }

    public class SatieScaleNearFieldRadiusEvent : IGameEvent
    {
        public float nearFieldRadiusScaler;
    }

    #endregion


    //-------------------------------------------------------------------------------
    // WTM Debugging
    //-------------------------------------------------------------------------------

    #region Debugging Events

    public class WtmDebuggingShowInstrumentLabelEvent : IGameEvent
    {
        public bool instrumentLabelVisible;
    }

    public class WtmDebugStateChangeEvent : IGameEvent
    {
        public bool state;
    }

    #endregion

    //-------------------------------------------------------------------------------
    // UI Events
    //-------------------------------------------------------------------------------

    #region UI Events

    public class PerformanceMonitoringChangedEvent : IGameEvent
    {
        public bool isEnabled;
    }

    public class OpenSettingsMenuEvent : IGameEvent
    {
        public GameObject callerGui;
    }

    public class CloseSettingsMenuEvent : IGameEvent
    {
        public GameObject callerGui;
    }

    public class OpenShowControlsMenuEvent : IGameEvent
    {
    }

    public class DisplayPlayerNameLabelsToggledEvent : IGameEvent
    {
        public bool isEnabled;
    }

    public class ParticlePerSecondIndexChangedEvent : IGameEvent
    {
        public float index;
    }

    public class WtmUItapEvent : IGameEvent
    {
        public Vector2 screenPosition;
    }

    public class WtmUIactiveEvent : IGameEvent
    {
        public bool isActive;
    }

    #endregion

    //-------------------------------------------------------------------------------
    // AR STATE Events
    //-------------------------------------------------------------------------------

    #region Game Events




    #endregion

    //-------------------------------------------------------------------------------
    // Game Events
    //-------------------------------------------------------------------------------

    #region Game Events

    /// <summary>
    /// Sends init message to gameRoot scene
    /// </summary>
    public class GameWasInitializedEvent : IGameEvent
    {
    }

    /// <summary>
    /// Clear all scenes except for persistent scene
    /// </summary>
    public class GameWasResetEvent : IGameEvent
    {
    }

    #endregion

    //-------------------------------------------------------------------------------
    // Scene Events
    //-------------------------------------------------------------------------------

    #region Scene Events

    /// <summary>
    /// Used to reset the start of the scene
    /// </summary>
    public class SceneWasInitializedEvent : IGameEvent
    {
    }

    /// <summary>
    /// Generic modal cue passed along to current scene 
    /// </summary>
    public class SceneWasPausedEvent : IGameEvent
    {
        public bool isPaused;
    }

    /// <summary>
    /// Invoke to change the game state using events when one does not
    /// have access to the SceneStateMachine.
    /// </summary>
    public class SceneWasRecalledEvent : IGameEvent
    {

    }

    /// <summary>
    /// Sometimes called when exiting a scene state, will try to free memory by calling GC.Collect + more
    /// </summary>
    public class SceneWasUnloadedEvent : IGameEvent
    {
    }

    /// <summary>
    /// Called when the scene state starts fading
    /// </summary>
    public class SceneFadeStartedEvent : IGameEvent
    {
    }

    /// <summary>
    /// Called when the scene state fading ends
    /// </summary>
    public class SceneFadeEndedEvent : IGameEvent
    {
    }

    /// <summary>
    /// Called when we enter any scene.
    /// </summary>
    public class SceneIsEnteringEvent : IGameEvent
    {
    }

    /// <summary>
    /// Called when we exit any scene.
    /// </summary>
    public class SceneIsExitingEvent : IGameEvent
    {
    }

    public class SceneCodeConfirmedEvent : IGameEvent
    {
    }

    #endregion

    //-------------------------------------------------------------------------------
    // Intro Events
    //-------------------------------------------------------------------------------

    #region Intro Events

    public class IntroSceneHasStartedEvent : IGameEvent
    {
    }

    public class IntroSceneHasEndedEvent : IGameEvent
    {
    }

    #endregion

    //-------------------------------------------------------------------------------
    // Outro Events
    //-------------------------------------------------------------------------------

    #region Outro Events

    public class OutroSceneHasStartedEvent : IGameEvent
    {
    }

    public class OutroSceneHasEndedEvent : IGameEvent
    {
    }

    #endregion

    //-------------------------------------------------------------------------------
    // Player Events
    //-------------------------------------------------------------------------------

    #region Player Events

    public class LocalPlayerSpawnRequestedEvent : IGameEvent
    {
        public int playerViewId;
        public string spawnGroupName;
    }

    public class LocalPlayerWasSpawnedEvent : IGameEvent
    {
        // TODO(oz): Add same info as when the player spawn was requested here?
    }

    public class LocalPlayerCloneRequestedEvent : IGameEvent
    {
        public int playerViewId;

        // TODO(oz): Make the path values into a single holder object that
        // can be created and returned from the PathManager as a serialized
        // class of its internal state. This will then be available to
        // serialized and distribute the avatar and feature state changes.
        public int pathPrefabIndex;
        public string pathPrefabName;
        public string pathColor;

        public Vector3 spawnPosition;
        public Quaternion spawnRotation;
    }

    public class LocalPlayerWasClonedEvent : IGameEvent
    {
        // TODO(oz): Add same info as when the player clone was requested here?
    }

    public class RemotePlayerWasSpawnedEvent : IGameEvent
    {
        // TODO(oz): Add same info as when the player clone was requested here?
    }

    public class LocalPlayerPathChangedEvent : IGameEvent
    {
        public int playerViewId;
        public int prefabIndex;
        public string prefabName;
    }

    public class LocalPlayerAvatarChangedEvent : IGameEvent
    {
        public int playerViewId;
        public int prefabIndex;
        public string prefabName;
    }

    public class LocalPlayerFeatureChangedEvent : IGameEvent
    {
        public int playerViewId;
        public int prefabIndex;
        public string prefabName;
    }

    /// <summary>
    /// An event that is dispatched when the state of the camera underwater effect is changed.
    /// </summary>
    public class PlayerUnderwaterStateChangedEvent : IGameEvent
    {
        public bool isUnderwater;
    }

    /// <summary>
    /// An event that is dispatched when the state of the camera underwater effect is changed.
    /// </summary>
    public class PlayerInsideMusicSceneChangeEvent : IGameEvent
    {
        public string musicName;
        public bool isInside;
        public Bounds sceneBounds;

    }

    #endregion

    //-------------------------------
    // Network Events
    //-------------------------------

    #region Network Events

    public class ConnectToServerRequestedEvent : IGameEvent
    {
        public string playerName;
    }

    public class ConnectionLoadingCompletedEvent : IGameEvent
    {
    }

    public class NetworkFeedbackTextChangedEvent : IGameEvent
    {
        public string message;
    }

    public class QuitRoomRequestedEvent : IGameEvent
    {
    }

    #endregion

    #region
    // File Download Events



    public class DownloadAbortEvent : IGameEvent
    {
    }

    public class DownLoadingFileStatusEvent : IGameEvent
    {
        public string fileName;
        public long totalBytes;
        public long downloadedBytes;
        public int ItemsInQueue;
        public int totalItemCount;
    }

    public class MusicDownLoadCompleteEvent : IGameEvent
    {
        public string musicName;
    }



    #endregion


    //-------------------------------------------------------------------------------
    // Music and Audio Events
    //-------------------------------------------------------------------------------

    #region Music Events


   
    /// <summary>
    /// Dispatched by a music "scene" (not a unity scene but a prefab) when it has loaded
    /// </summary>
    /// 
    public class WtmSceneLoadedEvent : IGameEvent
    {
        public Transform sceneRoot;
    }

    public class Wtm_audioParamChange_rvbDB : IGameEvent
    {
        public float trimDB;
    }

    //public class WtmEnsembleLoadedEvent : IGameEvent 
    //{
    //    public Transform ensembleRoot;
    //    public bool lockListenerYtoEnsembleY;
    //}

    /// <summary>
    /// Dispatched by a music "scene" (not a unity scene but a prefab) when it has loaded
    /// </summary>
    public class WtmSceneUnLoadedEvent : IGameEvent
    {
    }

    public class WtmSceneEvent : IGameEvent
    {
        public bool recenter;
    }

    public class MusicSelectEvent : IGameEvent
    {
        public string musicName;
    }

    public class MusicClearEvent : IGameEvent
    {
      
    }

    public class MusicChangedEvent : IGameEvent
    {
        public string musicName;
    }

    public class MusicPlayerTransportChangedEvent : IGameEvent
    {
    }

    public class MusicAttenuationChangedEvent : IGameEvent
    {
        public float attenuationScaler; //range:  0 <= n <= 1
    }

    public class MusicAvatarActivateEvent : IGameEvent
    {
        public bool state;
    }

    public class MusicFadeEvent : IGameEvent
    {
        public float attenuationScaler; //range:  0 <= n <= 1
        public float fadeSeconds; //range:  n >= 0 
    }

    public class AudioMasterTrimChangedEvent : IGameEvent
    {
        public float trimDB; //range in decibels:  -12 <= n <= +12  
    }

    // this is a legacy event still used for bdots
    public class MusicListChangedEvent : IGameEvent
    {
        public List<string> titleStrList;
    }

    public class MusicChosenEvent : IGameEvent
    {
        public string musicTitle;
    }

    // used when language change occurs
    public class MusicTitleChangeEvent : IGameEvent
    {
        public string musicTitle;
    }

    public class RefreshRemoteMusicDirsRequestedEvent : IGameEvent
    {
       
    }

    public class RemoteMusicListChangedEvent : IGameEvent
    {
        public List<string> titleStrList;
    }

    public class MusicManagerStateUpdateEvent : IGameEvent
    {

    }

    public class RemoteMusicTitleInfoEvent : IGameEvent
    {
        public string musicName;
        public int stemCount;
        public string musicInfoText;
    }

    /// <summary>
    /// Dispatched by Player to any listeners in loaded MusicScene(s) to display music-specific information
    /// </summary>
    public class MusicInfoRequestedEvent : IGameEvent
    {
    }

    public class WtmSetAudioListenerYclip : IGameEvent
    {
        public bool enableYlockPos;
        public float yLock2Height;
    }

    #endregion
}